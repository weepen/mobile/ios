//
//  UIVisualEffectView+Ambience.swift
//  Ambience
//
//  Created by Louis Cheminant on 07/12/2018.
//

import UIKit

extension UIVisualEffectView {
    
    override open func ambience(_ notification : Notification) {
        
        super.ambience(notification)
        
        guard let currentState = notification.userInfo?["currentState"] as? AmbienceState else { return }
        
        effect = currentState == .invert ? UIBlurEffect(style: .extraLight) : UIBlurEffect(style: .dark)
    }
}
