# Design
You will find all necessary resources to the realization of different views in our application.

## General Theme
**Light**
* #ffffff `#ffffff`  -> Front View
* #efeff5 `#efeff5`  -> Background View
* #eef1f7 `#eef1f7`  -> Background Button
* #c7c7ce `#c7c7ce`  -> Text Unselected
* #95989a `#95989a` -> Text Selected

**Dark**
* #3c3b3c `#3c3b3c`  -> Front View
* #4a4a4a `#4a4a4a`  -> Background View
* #5A5A5A `#eef1f7`  -> Background Button
* #95989a `#95989a`  -> Text Unselected
* #c7c7ce `#c7c7ce` -> Text Selected

**Night**
* #000000 `#000000`  -> Background View
* #141414 `#141414`  -> Front View
* #202020 `#202020`  -> Background Button
* #95989a `#95989a`  -> Text Unselected
* #c7c7ce `#c7c7ce` -> Text Selected

## General Color

**Orange** 

* #fc834f `#fc834f`  -> Main Color
* #fc834f `#fc834f`  -> Top Gradient*
* #e1676f `#e1676f` -> Bottom Gradient*

**Blue**
* #56ccf2 `#56ccf2`  -> Main Color
* #56ccf2 `#56ccf2 `  -> Top Gradient*
* #2f80ed `#2f80ed` -> Bottom Gradient*

**Pink**
* #ec008c `#ec008c`  -> Main Color
* #ec008c `#ec008c`  -> Top Gradient*
* #a848d3 `#a848d3` -> Bottom Gradient*

**Other**
* #4cd964 `#4cd964`  -> Validation Color
* #ff3b30 `#ff3b30`  -> Error Color

*For gradient: startPointX -> 0, startPointY -> 0, endPointX -> 1, endPointY -> 0.5

## Shadow

* Shadow radius -> 10
* Shadow offset -> 0,0
**Light**
* Shadow color -> * #000000 `#000000`  (Opacity 50%)
* * Shadow opacity -> 0.3
**Dark**
* Shadow color -> * #ffffff `#ffffff`  (Opacity 50%)
* * Shadow opacity -> 0.3
**Night**
* Shadow color -> * #ffffff `#ffffff`  (Opacity 100%)
* Shadow opacity -> 0.5

## Corner

* Radius -> 10

## Font
* [Europa Bold](https://drive.google.com/open?id=0B8PE1jzlBIMwRzhhY1VqNThrbWc)  (standard title font)
* [Europa Regular](https://drive.google.com/open?id=0B8PE1jzlBIMwdU9lLUdDaVhnZkE) (standard font)
* [Boucherie](https://drive.google.com/open?id=0B8PE1jzlBIMwVDcxQUtLajFxbzQ) (use for Logo only)

## Images*

* [Banner Filigree](https://drive.google.com/uc?id=0B8PE1jzlBIMwQnVNTHRPY1NIaVk)
* [Google Logo Button](https://drive.google.com/uc?id=0B8PE1jzlBIMwb1JzaTVNVWpFdEU)
* [Facebook Logo Button](https://drive.google.com/uc?id=0B8PE1jzlBIMwMW1lMTBWdlQ1T2s)

*Be careful, images are white.