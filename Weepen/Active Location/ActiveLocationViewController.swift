//
//  ActiveLocationViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 29/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import CoreLocation
import Ambience

class ActiveLocationViewController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var resumeLbl: UILabel!
    var alertController = UIAlertController()

    
    let LocationMgr = LocationService.shared
    @IBOutlet weak var centerViewCst: NSLayoutConstraint!
    @IBOutlet weak var widthPictureCst: NSLayoutConstraint!
    
    override func viewWillAppear(_ animated: Bool) {
        let size = UIScreen.main.bounds.size
        centerViewCst.constant = UIDevice.current.userInterfaceIdiom == .pad && size.width < size.height ? -64 : 0
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        widthPictureCst.isActive = UIDevice.current.userInterfaceIdiom == .phone &&  UIDevice.current.orientation.isLandscape ? true : false
        NotificationCenter.default.addObserver(self, selector: #selector(applicationIsActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        setupAlertController()
    }

    
    func setupAlertController() {
        alertController = UIAlertController(title: titleLbl.text, message: resumeLbl.text, preferredStyle: .actionSheet)
        let settingsAction = UIAlertAction(title: NSLocalizedString("OPEN_PREFERENCES", comment: ""), style: .default) { (action) in
            guard let settingsAppUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            guard let settingsLocationUrl = URL(string: "App-Prefs:root=Privacy&path=LOCATION") else { return}
            if CLLocationManager.locationServicesEnabled() {
                if UIApplication.shared.canOpenURL(settingsAppUrl) {
                    UIApplication.shared.open(settingsAppUrl, completionHandler: { (success) in
                        print("Setting is opened: \(success)")
                    })
                }
            } else {
                if UIApplication.shared.canOpenURL(settingsLocationUrl) {
                    UIApplication.shared.open(settingsLocationUrl, completionHandler: { (success) in
                        print("Setting is opened: \(success)")
                    })
                }
            }
            
        }
        alertController.addAction(settingsAction)
        
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.center.x, y: UIScreen.main.bounds.maxY-32, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        DispatchQueue.main.async {
            self.present(self.alertController, animated: true, completion: nil)
        }
    }
    
    override func willRotate(to toInterfaceOrientation: UIInterfaceOrientation, duration: TimeInterval) {
        switch UIDevice.current.orientation {
        case .portrait:
            centerViewCst.constant = -64
        case .landscapeLeft, .landscapeRight:
            centerViewCst.constant = 0
        default: break
        }
    }
    
    override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
        switch UIDevice.current.orientation {
        case .portrait:
            alertController.removeFromParent()
            setupAlertController()
        case .landscapeLeft, .landscapeRight:
            alertController.removeFromParent()
            setupAlertController()
        default: break
        }
    
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    @objc func applicationIsActive() {
        switch LocationMgr.isAuthorized() {
        case 1:
            dismiss(animated: true, completion: nil)
        default: break
        }
    }



}

