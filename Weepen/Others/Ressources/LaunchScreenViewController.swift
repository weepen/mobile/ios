//
//  LaunchScreenViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 23/07/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import QuartzCore

class LaunchScreenViewController: UIViewController {

    var myView = MyView()
    var gradientLayer: CAGradientLayer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        createGradientLayer()
        myView = MyView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        myView.backgroundColor = .clear
        self.view.addSubview(myView)
    }
    
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [UIColor.hexColor(0xED7A59).cgColor, UIColor.hexColor(0xEA5569).cgColor]
        self.view.layer.addSublayer(gradientLayer)
    }

}

class MyView: UIView, CAAnimationDelegate {
    
    let pathLayer = CAShapeLayer()
    
    override func draw(_ rect: CGRect) {
        
        pathLayer.frame = self.bounds
        
        let textPath = UIBezierPath()
        textPath.move(to: CGPoint(x: 145.78, y: 294.29))
        textPath.addCurve(to: CGPoint(x: 81.71, y: 357.45), controlPoint1: CGPoint(x: 114.32, y: 294.29), controlPoint2: CGPoint(x: 81.71, y: 320.28))
        textPath.addCurve(to: CGPoint(x: 118.19, y: 401.22), controlPoint1: CGPoint(x: 81.71, y: 385.26), controlPoint2: CGPoint(x: 101.32, y: 401.22))
        textPath.addCurve(to: CGPoint(x: 137.57, y: 386.4), controlPoint1: CGPoint(x: 128.45, y: 401.22), controlPoint2: CGPoint(x: 137.57, y: 394.84))
        textPath.addCurve(to: CGPoint(x: 129.14, y: 376.37), controlPoint1: CGPoint(x: 137.57, y: 379.79), controlPoint2: CGPoint(x: 133.93, y: 376.37))
        textPath.addCurve(to: CGPoint(x: 115.69, y: 382.98), controlPoint1: CGPoint(x: 122.75, y: 376.37), controlPoint2: CGPoint(x: 120.25, y: 382.98))
        textPath.addCurve(to: CGPoint(x: 98.81, y: 359.5), controlPoint1: CGPoint(x: 109.53, y: 382.98), controlPoint2: CGPoint(x: 98.81, y: 375.23))
        textPath.addCurve(to: CGPoint(x: 143.5, y: 310.02), controlPoint1: CGPoint(x: 98.81, y: 331.68), controlPoint2: CGPoint(x: 122.53, y: 310.02))
        textPath.addCurve(to: CGPoint(x: 165.39, y: 338.75), controlPoint1: CGPoint(x: 156.95, y: 310.02), controlPoint2: CGPoint(x: 165.39, y: 318.23))
        textPath.addCurve(to: CGPoint(x: 155.81, y: 416.04), controlPoint1: CGPoint(x: 165.39, y: 365.43), controlPoint2: CGPoint(x: 155.81, y: 390.28))
        textPath.addCurve(to: CGPoint(x: 185.23, y: 450.93), controlPoint1: CGPoint(x: 155.81, y: 436.34), controlPoint2: CGPoint(x: 166.07, y: 450.93))
        textPath.addCurve(to: CGPoint(x: 237.67, y: 392.1), controlPoint1: CGPoint(x: 209.17, y: 450.93), controlPoint2: CGPoint(x: 227.86, y: 416.04))
        textPath.addCurve(to: CGPoint(x: 274.15, y: 450.93), controlPoint1: CGPoint(x: 241.54, y: 419.01), controlPoint2: CGPoint(x: 250.21, y: 450.93))
        textPath.addCurve(to: CGPoint(x: 323.85, y: 358.59), controlPoint1: CGPoint(x: 301.73, y: 450.93), controlPoint2: CGPoint(x: 323.85, y: 409.89))
        textPath.addCurve(to: CGPoint(x: 299.68, y: 296.57), controlPoint1: CGPoint(x: 323.85, y: 312.99), controlPoint2: CGPoint(x: 314.5, y: 296.57))
        textPath.addCurve(to: CGPoint(x: 288.28, y: 308.66), controlPoint1: CGPoint(x: 292.84, y: 296.57), controlPoint2: CGPoint(x: 288.28, y: 302.5))
        textPath.addCurve(to: CGPoint(x: 304.24, y: 362.46), controlPoint1: CGPoint(x: 288.28, y: 323.25), controlPoint2: CGPoint(x: 304.24, y: 323.48))
        textPath.addCurve(to: CGPoint(x: 277.34, y: 428.81), controlPoint1: CGPoint(x: 304.24, y: 397.12), controlPoint2: CGPoint(x: 294.21, y: 428.81))
        textPath.addCurve(to: CGPoint(x: 260.01, y: 330.77), controlPoint1: CGPoint(x: 264.34, y: 428.81), controlPoint2: CGPoint(x: 260.01, y: 363.15))
        textPath.addCurve(to: CGPoint(x: 247.01, y: 312.99), controlPoint1: CGPoint(x: 260.01, y: 316.64), controlPoint2: CGPoint(x: 254.99, y: 312.99))
        textPath.addCurve(to: CGPoint(x: 233.33, y: 330.54), controlPoint1: CGPoint(x: 239.26, y: 312.99), controlPoint2: CGPoint(x: 233.33, y: 318.23))
        textPath.addCurve(to: CGPoint(x: 186.37, y: 435.2), controlPoint1: CGPoint(x: 233.33, y: 365.88), controlPoint2: CGPoint(x: 204.61, y: 435.2))
        textPath.addCurve(to: CGPoint(x: 179.07, y: 415.59), controlPoint1: CGPoint(x: 181.35, y: 435.2), controlPoint2: CGPoint(x: 179.07, y: 429.27))
        textPath.addCurve(to: CGPoint(x: 188.65, y: 334.19), controlPoint1: CGPoint(x: 179.07, y: 394.16), controlPoint2: CGPoint(x: 188.65, y: 355.85))
        textPath.addCurve(to: CGPoint(x: 145.78, y: 294.29), controlPoint1: CGPoint(x: 188.65, y: 307.52), controlPoint2: CGPoint(x: 168.35, y: 294.29))
        textPath.close()
        
        pathLayer.path = textPath.cgPath
        pathLayer.lineWidth = 5
        pathLayer.fillColor = UIColor.clear.cgColor
        pathLayer.strokeColor = UIColor.white.cgColor
        pathLayer.strokeStart = 0
        pathLayer.strokeEnd = 1
        
        pathLayer.add(animFontWithDur(dur: 2, delegate: pathLayer), forKey: "strokeEnd")
        self.layer.addSublayer(pathLayer)
        
    }
    
    @objc func animationDidStop(_ animation: CAAnimation, finished: Bool) {
        if animation.duration == 0.5 {
            pathLayer.add(animStrokeEndWithDuration(dur: 1, delegate: pathLayer), forKey: "strokeEnd")
            self.layer.addSublayer(pathLayer)
        } else if animation.duration == 1 {
            pathLayer.add(animFontWithDur(dur: 2, delegate: pathLayer), forKey: "strokeEnd")
            self.layer.addSublayer(pathLayer)
        } else {
            pathLayer.add(animFillColorWithDur(dur: 0.5, startCol: .clear, endColor: .white, delegate: pathLayer), forKey: "strokeEnd")
            self.layer.addSublayer(pathLayer)
        }
        
    }
    
    
    func animFillColorWithDur(dur: CGFloat, startCol start: UIColor, endColor end: UIColor, delegate target: AnyObject) -> CABasicAnimation {
        let animFill = CABasicAnimation(keyPath: "fillColor")
        animFill.duration = CFTimeInterval(dur)
        animFill.fromValue = (start.cgColor as AnyObject)
        animFill.toValue = (end.cgColor as AnyObject)
        animFill.isRemovedOnCompletion = false
        animFill.delegate = self
        animFill.fillMode = kCAFillModeBoth
        return animFill
    }
    
    func animFontWithDur(dur: CGFloat, delegate target: AnyObject) -> CABasicAnimation {
        let easeOut = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        let pulseAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pulseAnimation.fromValue = 0.0
        pulseAnimation.toValue = 1.0
        pulseAnimation.duration = CFTimeInterval(dur)
        pulseAnimation.timingFunction = easeOut
        pulseAnimation.delegate = self
        return pulseAnimation
    }
    
    func animStrokeEndWithDuration(dur: CGFloat, delegate target: AnyObject) -> CABasicAnimation {
        let animLine = CABasicAnimation(keyPath: "fillColor")
        animLine.duration = CFTimeInterval(dur)
        animLine.fromValue = UIColor.white.cgColor
        animLine.toValue = UIColor.white.cgColor
        animLine.isRemovedOnCompletion = false
        animLine.delegate = self
        animLine.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        return animLine
    }
}
