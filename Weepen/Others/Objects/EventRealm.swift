//
//  Event.swift
//  Weepen
//
//  Created by Louis Cheminant on 28/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseFirestore

class EventRealm: Object {
    @objc dynamic var uid: String = UUID().uuidString
    @objc dynamic var title: String? = nil
    @objc dynamic var resume: String? = nil
    
    @objc dynamic var name: String? = nil
    @objc dynamic var thoroughfare: String? = nil
    @objc dynamic var zip: String? = nil
    @objc dynamic var city: String? = nil
    @objc dynamic var country: String? = nil
    
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var distance = RealmOptional<Double>()
    
    @objc dynamic var dateStart: Date? = nil
    @objc dynamic var dateEnd: Date? = nil
    
    @objc dynamic var currency: String? = nil
    var price = RealmOptional<Double>()
    
    
    @objc dynamic var sport: SportRealm? = nil
    @objc dynamic var owner: OwnerRealm? = nil
    @objc dynamic var placeUID: String? = nil
    
    var nbParticipant = RealmOptional<Int>()
    var nbMinParticipant = RealmOptional<Int>()
    var nbMaxParticipant = RealmOptional<Int>()
    
    var isPrivate = RealmOptional<Bool>()
    var isSponsored = RealmOptional<Bool>()
    
    var isParticipate = RealmOptional<Bool>()
    var isDisplay = RealmOptional<Bool>()
    var isCreate = RealmOptional<Bool>()
    
    var participants = List<ParticipantRealm>()
    
    
    public typealias getDocumentsUserParticipateCallback = (_ result: Bool, _ events: [QueryDocumentSnapshot]?)->()
    public typealias getEventUserParticipateCallback = (_ result: Bool, _ events: EventRealm?)->()
    var documentsLoaded : getDocumentsUserParticipateCallback?
    var eventsLoaded : getEventUserParticipateCallback?
    
    override public static func indexedProperties() -> [String] {
        return ["dateStart"]
    }
    
    override public class func primaryKey() -> String {
        return "uid"
    }
    
    convenience init?(dataFromAlgolia data: [String:Any]) {
        self.init()
        
        guard let uid = data["objectID"] as? String,
            let title = data["title"] as? String,
            let resume = data["description"] as? String,
            let sportTitle = data["sportUID"] as? String,
            let sport = CacheService.shared.getDataFromDB(object: SportRealm()).filter("sportUUID = '\(sportTitle)'").first as? SportRealm,
            let start_date = data["startDate"] as? Double,
            Date(timeIntervalSince1970: start_date).timeIntervalSinceNow > 0,
            let end_date = data["endDate"] as? Double,
            let nbMinParticipant = data["minParticipants"] as? Int,
            let nbMaxParticipant = data["maxParticipants"] as? Int,
            let geoloc = data["_geoloc"] as? [String:Any],
            let latitude = geoloc["lat"] as? Double,
            let longitude = geoloc["lng"] as? Double,
            let address = data["address"] as? [String:Any],
            let reservation = data["reservation"] as? [String:Any],
            let price = reservation["price"] as? Double,
            let currency = reservation["currency"] as? String,
            let isPrivate = data["private"] as? Bool,
            let isSponsored = data["sponsored"] as? Bool,
            let ownerUid = data["ownerUID"] as? String else {
                if let oldEvent = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(self.uid)'").first {
                    CacheService.shared.deleteFromDb(object: oldEvent, completion: {})
                }
                return nil
        }
        self.uid = uid
        self.title = title
        self.resume = resume
        if let name = address["name"] as? String { self.name = name }
        if let thoroughfare = address["thoroughfare"] as? String { self.thoroughfare = thoroughfare }
        if let zip = address["zip"] as? String { self.zip = zip }
        if let city = address["city"] as? String { self.city = city }
        if let country = address["country"] as? String { self.country = country }
        self.latitude.value = latitude
        self.longitude.value = longitude
        self.distance.value = LocationService.shared.getDistance(latitude, longitude)
        self.dateStart = Date(timeIntervalSince1970: start_date)
        self.dateEnd = Date(timeIntervalSince1970: end_date)
        self.currency = currency
        self.price.value = price
        self.sport = sport
        if let nbParticipant = data["nbParticipants"] as? Int { self.nbParticipant.value = nbParticipant }
        self.nbMinParticipant.value = nbMinParticipant
        self.nbMaxParticipant.value = nbMaxParticipant
        self.isPrivate.value = isPrivate
        self.isSponsored.value = isSponsored
        self.isDisplay.value = true
        self.isParticipate.value = false
        self.isCreate.value = false
        if let owner = CacheService.shared.getDataFromDB(object: OwnerRealm()).filter("uuid == '\(ownerUid)'").first {
            self.owner = (owner as! OwnerRealm)
        } else {
            self.owner = OwnerRealm.init(uid: ownerUid)
        }
    }
    
    convenience init?(dataFromFirebase data: [String:Any], _ uuid: String? = "", _ isParticipate: Bool, _ isCreate: Bool) {
        self.init()
        
        guard let title = data["title"] as? String,
            let resume = data["description"] as? String,
            let sportRef = data["sport"] as? DocumentReference,
            let sport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(sportRef.path.replacingOccurrences(of: "\(FirebaseService.shared.dbSport.path)/", with: "", options: .literal, range: nil))'").first,
            let start_date = data["startDate"] as? Timestamp,
            start_date.dateValue().timeIntervalSinceNow > 0,
            let end_date = data["endDate"] as? Timestamp,
            let nbMinParticipant = data["minParticipants"] as? Int,
            let nbMaxParticipant = data["maxParticipants"] as? Int,
            let geoloc = data["geoloc"] as? GeoPoint,
            let address = data["address"] as? [String:Any],
            let name = address["name"] as? String,
            let thoroughfare = address["thoroughfare"] as? String,
            let zip = address["zip"] as? String,
            let city = address["city"] as? String,
            let country = address["country"] as? String,
            let reservation = data["reservation"] as? [String:Any],
            let price = reservation["price"] as? Double,
            let currency = reservation["currency"] as? String,
            let isPrivate = data["private"] as? Bool,
            let isSponsored = data["sponsored"] as? Bool,
            let placeRef = data["place"] as? DocumentReference,
            let ownerRef = data["owner"] as? DocumentReference else {
                if let oldEvent = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(self.uid)'").first {
                    CacheService.shared.deleteFromDb(object: oldEvent, completion: {})
                }
                return nil
        }
        if uuid != "" { self.uid = uuid! }
        self.title = title
        self.sport = sport
        self.resume = resume
        self.dateStart = start_date.dateValue()
        self.dateEnd = end_date.dateValue()
        if let nbParticipant = data["nbParticipants"] as? Int { self.nbParticipant.value = nbParticipant }
        self.nbMinParticipant.value = nbMinParticipant
        self.nbMaxParticipant.value = nbMaxParticipant
        self.latitude.value = geoloc.latitude
        self.longitude.value = geoloc.longitude
        self.name = name
        self.thoroughfare = thoroughfare
        self.zip = zip
        self.city = city
        self.country = country
        self.currency = currency
        self.price.value = price
        self.isPrivate.value = isPrivate
        self.isSponsored.value = isSponsored
        self.isDisplay.value = false
        self.isParticipate.value = isParticipate
        self.isCreate.value = isCreate
        self.placeUID = placeRef.path.replacingOccurrences(of: "\(FirebaseService.shared.dbPlace.path)/", with: "", options: .literal, range: nil)
        let ownerUid = ownerRef.path.replacingOccurrences(of: "\(FirebaseService.shared.dbUser.path)/", with: "", options: .literal, range: nil)
        if let owner = CacheService.shared.getDataFromDB(object: OwnerRealm()).filter("uuid == '\(ownerUid)'").first {
            self.owner = (owner as! OwnerRealm)
        } else {
            self.owner = OwnerRealm.init(uid: ownerUid)
        }
    }
    
    
    func orderEventsDisplay(_ newEvents: [EventRealm]) -> ([Object], [Object], [Object]) {
        let removeEvents = Array((Set(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay == true")).subtracting(Set(newEvents))))
        let updateEvents = Array(Set(newEvents).intersection(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay == true")))
        let addEvents = Array(Set(Set(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay == true")).symmetricDifference(Set(newEvents))).intersection(Set(newEvents)))
        return (removeEvents, updateEvents, addEvents)
    }
    
    func orderEventsParticipant(_ newEvents: [EventRealm]) -> ([Object], [Object], [Object]) {
        let removeEvents = Array((Set(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true")).subtracting(Set(newEvents))))
        let updateEvents = Array(Set(newEvents).intersection(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true")))
        let addEvents = Array(Set(Set(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true")).symmetricDifference(Set(newEvents))).intersection(Set(newEvents)))
        return (removeEvents, updateEvents, addEvents)
    }
    
    func orderEventsCreate(_ newEvents: [EventRealm]) -> ([Object], [Object], [Object]) {
        let removeEvents = Array((Set(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate == true")).subtracting(Set(newEvents))))
        let updateEvents = Array(Set(newEvents).intersection(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate == true")))
        let addEvents = Array(Set(Set(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate == true")).symmetricDifference(Set(newEvents))).intersection(Set(newEvents)))
        return (removeEvents, updateEvents, addEvents)
    }
    
    func removeEvents(_ events: [Object], _ key: EventType) {
        for case let event as EventRealm in events {
            switch key {
            case .display:
                if event.isParticipate.value == false && event.isCreate.value == false {
                    if event.owner!.events.count == 0 {
                        CacheService.shared.deleteFromDb(object: event.owner!) {}
                    }
                    CacheService.shared.deleteFromDb(object: event) {}
                }
                else {
                    try! CacheService.shared.getDatabase().write {
                        guard !event.isInvalidated else { return }
                        event.isDisplay.value = false
                    }
                }
            case .participate:
                if event.isDisplay.value == false && event.isCreate.value == false {
                    if event.owner!.events.count == 0 {
                        CacheService.shared.deleteFromDb(object: event.owner!) {}
                    }
                    CacheService.shared.deleteFromDb(object: event) {}
                }
                else {
                    try! CacheService.shared.getDatabase().write {
                        guard !event.isInvalidated else { return }
                        event.isParticipate.value = false
                    }
                }
            case .create:
                if event.isDisplay.value == false && event.isParticipate.value == false {
                    if event.owner!.events.count == 0 {
                        CacheService.shared.deleteFromDb(object: event.owner!) {}
                    }
                    CacheService.shared.deleteFromDb(object: event) {}
                }
                else {
                    try! CacheService.shared.getDatabase().write {
                        guard !event.isInvalidated else { return }
                        event.isCreate.value = false
                    }
                }
            }
        }
    }
    
    func updateEvents(_ events: [Object], _ key: EventType) {
        for case let event as EventRealm in events {
            if let oldEvent = CacheService.shared.getDatabase().objects(EventRealm.self).filter("uid = '\(event.uid)'").first {
                if oldEvent.title != event.title {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.title = event.title }
                    }
                }
                if oldEvent.sport != event.sport {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.sport = event.sport }
                    }
                }
                if oldEvent.resume != event.resume {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.resume = event.resume }
                    }
                }
                if oldEvent.currency != event.currency {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.currency = event.currency }
                    }
                }
                if oldEvent.dateStart != event.dateStart {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.dateStart = event.dateStart }
                    }
                }
                if oldEvent.dateEnd != event.dateEnd {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.dateEnd = event.dateEnd }
                    }
                }
                if oldEvent.nbMinParticipant.value != event.nbMinParticipant.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.nbMinParticipant.value = event.nbMinParticipant.value }
                    }
                }
                if oldEvent.nbMaxParticipant.value != event.nbMaxParticipant.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.nbMaxParticipant.value = event.nbMaxParticipant.value }
                    }
                }
                if oldEvent.latitude.value != event.latitude.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.latitude.value = event.latitude.value }
                    }
                }
                if oldEvent.longitude.value != event.longitude.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.longitude.value = event.longitude.value }
                    }
                }
                if oldEvent.price.value != event.price.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.price.value = event.price.value }
                    }
                }
                if oldEvent.city != event.city {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.city = event.city }
                    }
                }
                if oldEvent.country != event.country {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.country = event.country }
                    }
                }
                if oldEvent.name != event.name {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.name = event.name }
                    }
                }
                if oldEvent.thoroughfare != event.thoroughfare {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.thoroughfare = event.thoroughfare }
                    }
                }
                if oldEvent.zip != event.zip {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.zip = event.zip }
                    }
                }
                if oldEvent.isPrivate.value != event.isPrivate.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.isPrivate.value = event.isPrivate.value }
                    }
                }
                if oldEvent.isSponsored.value != event.isSponsored.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldEvent.isInvalidated { oldEvent.isSponsored.value = event.isSponsored.value }
                    }
                }
                switch key {
                case .display:
                    if oldEvent.isDisplay.value != event.isDisplay.value {
                        try! CacheService.shared.getDatabase().write {
                            if !oldEvent.isInvalidated { oldEvent.isDisplay.value = event.isDisplay.value }
                        }
                    }
                case .participate:
                    if oldEvent.isParticipate.value != event.isParticipate.value {
                        try! CacheService.shared.getDatabase().write {
                            if !oldEvent.isInvalidated { oldEvent.isParticipate.value = event.isParticipate.value }
                        }
                    }
                case .create:
                    if oldEvent.isCreate.value != event.isCreate.value {
                        try! CacheService.shared.getDatabase().write {
                            if !oldEvent.isInvalidated { oldEvent.isCreate.value = event.isCreate.value }
                        }
                    }
                }
            }
        }
    }
    
    func addEvents(_ events: [Object], _ key: EventType) {
        let updateEvents = Array(Set(events).intersection(CacheService.shared.getDataFromDB(object: EventRealm())))
        for case let updateEvent as EventRealm in updateEvents {
            if let event = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(updateEvent.uid)'").first {
                print((event as! EventRealm).uid)
                try! CacheService.shared.getDatabase().write {
                    guard !event.isInvalidated else { return }
                    switch key {
                    case .display:
                        (event as! EventRealm).isDisplay.value = true
                    case .participate:
                        (event as! EventRealm).isParticipate.value = true
                    case .create:
                        (event as! EventRealm).isCreate.value = true
                    }
                }
            }
        }
        let addEvents = Array(Set(Set(CacheService.shared.getDataFromDB(object: EventRealm())).symmetricDifference(Set(events))).intersection(Set(events)))
        CacheService.shared.addData(objects: addEvents)
    }
    
    
}


extension EventRealm {
    func getNBPaticipantFirebaseDisplay(_ uid: String, completion:@escaping (Int) -> ()) {
        let listener = FirebaseService.shared.dbEvent.document(uid).collection("participants").addSnapshotListener({ (queryDocument, error) in
            guard let document = queryDocument, error == nil, UserDefaults.standard.bool(forKey: "isDisplayVC") else {
                return
            }
            completion(document.documents.count)
        })
        if !UserDefaults.standard.bool(forKey: "isDisplayVC") {
            listener.remove()
        }
    }
    
    func getNBPaticipantFirebaseParticipate(_ uid: String, completion:@escaping (Int) -> ()) {
        let listener = FirebaseService.shared.dbEvent.document(uid).collection("participants").addSnapshotListener({ (queryDocument, error) in
            guard let document = queryDocument, error == nil, UserDefaults.standard.bool(forKey: "isParticipateVC") else {
                return
            }
            completion(document.documents.count)
        })
        if !UserDefaults.standard.bool(forKey: "isParticipateVC") {
            listener.remove()
        }
    }
    
    func getNBPaticipantFirebaseCreate(_ uid: String, completion:@escaping (Int) -> ()) {
        let listener = FirebaseService.shared.dbEvent.document(uid).collection("participants").addSnapshotListener({ (queryDocument, error) in
            guard let document = queryDocument, error == nil, UserDefaults.standard.bool(forKey: "isCreateVC") else {
                return
            }
            completion(document.documents.count)
        })
        if !UserDefaults.standard.bool(forKey: "isCreateVC") {
            listener.remove()
        }
    }
    
    func getNBParticipantUpdate(_ type: EventType) {
        switch type {
        case .display:
            for case let event as EventRealm in CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true") {
                EventRealm().getNBPaticipantFirebaseDisplay(event.uid) { (nb) in
                    guard !event.isInvalidated else { return }
                    if event.nbParticipant.value == nb { return }
                    try! CacheService.shared.getDatabase().write {
                        event.nbParticipant.value = nb
                    }
                }
            }
        case .participate:
            for case let event as EventRealm in CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate = true") {
                EventRealm().getNBPaticipantFirebaseParticipate(event.uid) { (nb) in
                    guard !event.isInvalidated else { return }

                    if event.nbParticipant.value == nb { return }
                    try! CacheService.shared.getDatabase().write {
                        event.nbParticipant.value = nb
                    }
                }
            }
        case .create:
            for case let event as EventRealm in CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate = true") {
                EventRealm().getNBPaticipantFirebaseCreate(event.uid) { (nb) in
                    guard !event.isInvalidated else { return }
                    if event.nbParticipant.value == nb { return }
                    try! CacheService.shared.getDatabase().write {
                        event.nbParticipant.value = nb
                    }
                }
            }
        }
    }
    
    
    func getAllDocumentsUserParticipate(_ documentsLoaded: @escaping getDocumentsUserParticipateCallback) {
        self.documentsLoaded = documentsLoaded
        FirebaseService.shared.dbEvent.order(by: "startDate", descending: false).whereField("startDate", isGreaterThanOrEqualTo: Date()).getDocuments(completion: { (queryDocument, error) in
            guard let documents = queryDocument, error == nil else {
                print("***EVENT GET PARTICIPANT REALM ERROR: [\(error!)]***")
                if let _documentsLoaded = self.documentsLoaded {
                    _documentsLoaded(false, nil)
                }
                return
            }
            if let _documentsLoaded = self.documentsLoaded {
                _documentsLoaded(true, documents.documents)
            }
        })
    }
    
    func getDocumentsUserParticipate(uidUser: String, document: QueryDocumentSnapshot, _ eventsLoaded: @escaping getEventUserParticipateCallback) {
        self.eventsLoaded = eventsLoaded
        FirebaseService.shared.dbEvent.document(document.documentID).collection("participants").document(uidUser).getDocument(completion: { (queryDocument, error) in
            guard let documentParticipant = queryDocument, error == nil else {
                if let _eventsLoaded = self.eventsLoaded {
                    _eventsLoaded(false, nil)
                }
                return
            }
            if documentParticipant.exists {
                if let event = EventRealm(dataFromFirebase: document.data(), document.documentID, true, false) {
                    if let _eventsLoaded = self.eventsLoaded {
                        _eventsLoaded(true, event)
                    }
                }
            } else {
                if let _eventsLoaded = self.eventsLoaded {
                    _eventsLoaded(false, nil)
                }
            }
        })
    }
    
    func getEventsUserCreate(uidUser: String, completion:@escaping (Bool, [EventRealm]) -> ()) {
        var events = [EventRealm]()
        FirebaseService.shared.dbEvent.whereField("owner", isEqualTo: FirebaseService.shared.dbUser.document(uidUser)).getDocuments(completion: { (queryDocument, error) in
            guard let documents = queryDocument, error == nil else {
                print("***EVENT GET PARTICIPANT REALM ERROR: [\(error!)]***")
                completion(false, [])
                return
            }
            for document in documents.documents {
                if let event = EventRealm(dataFromFirebase: document.data(), document.documentID, false, true) {
                    events.append(event)
                }
            }
            events = events.sorted(by: { (lhsData, rhsData) -> Bool in
                return Int((lhsData.dateStart?.timeIntervalSince1970)!) < Int((rhsData.dateStart?.timeIntervalSince1970)!)
            })
            completion(true, events)
        })
    }
    
    func getEventUser(uidEvent: String, completion:@escaping (Bool, EventRealm?) -> ()) {
        FirebaseService.shared.dbEvent.document(uidEvent).getDocument(completion: { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***EVENT GET PARTICIPANT REALM ERROR: [\(error!)]***")
                completion(false, nil)
                return
            }
            if let event = EventRealm(dataFromFirebase: document.data()!, document.documentID, false, false) {
                completion(true, event)
            }
        })
    }
    
    
}

extension EventRealm {
    func setEvent(description: String, endDate: Date, latitude: Double, longitude: Double, maxParticipants: Int, minParticipants: Int, owner: String, isPrivate: Bool, price: Int, currency: String, sponsored: Bool, sport: String, startDate: Date, title: String, city: String, country: String, name: String, zip: String, thoroughfare: String, placeUid: String, completion:@escaping (Bool, String?) -> ()) {
        
        let refNewEvent = FirebaseService.shared.dbEvent.document()
        
        var infosNewEvent = [
            "description" : description,
            "endDate": FirebaseService.shared.timestamp(endDate),
            "geoloc" : FirebaseService.shared.location(latitude, longitude),
            "maxParticipants":maxParticipants,
            "minParticipants":minParticipants,
            "owner" : FirebaseService.shared.dbUser.document(owner),
            "private" : isPrivate,
            "archived" : false,
            "reservation": ["currency":currency, "price":price] as [String : Any],
            "sponsored":sponsored,
            "sport":FirebaseService.shared.dbSport.document(sport),
            "startDate": FirebaseService.shared.timestamp(startDate),
            "title":title,
            "address":["city":city, "country":country, "name":name, "zip":zip, "thoroughfare":thoroughfare]
            ] as [String : Any]
        if placeUid != "" {
            infosNewEvent["place"] = FirebaseService.shared.dbPlace.document(placeUid)
        }
        
        refNewEvent.setData(infosNewEvent) { dataError in
            if let error = dataError {
                
                print("***EVENT REALM CREATED ERROR: [\(error)***")
                completion(false, nil)
            } else {
                let _ = EventRealm(dataFromFirebase: infosNewEvent, refNewEvent.documentID, false, true)
                completion(true, refNewEvent.documentID)
            }
        }
    }
    
    func updateEvent(uid: String, description: String, endDate: Date, latitude: Double, longitude: Double, maxParticipants: Int, minParticipants: Int, owner: String, isPrivate: Bool, price: Int, currency: String, sponsored: Bool, sport: String, startDate: Date, title: String, city: String, country: String, name: String, zip: String, thoroughfare: String, placeUid: String, completion:@escaping (Bool) -> ()) {
        let refNewEvent = FirebaseService.shared.dbEvent.document(uid)
        
        var data = [
            "description" : description,
            "endDate": FirebaseService.shared.timestamp(endDate),
            "geoloc" : FirebaseService.shared.location(latitude, longitude),
            "maxParticipants":maxParticipants,
            "minParticipants":minParticipants,
            "owner" : FirebaseService.shared.dbUser.document(owner),
            "private" : isPrivate,
            "reservation": ["currency":currency, "price":price] as [String : Any],
            "sponsored":sponsored,
            "sport":FirebaseService.shared.dbSport.document(sport),
            "startDate": FirebaseService.shared.timestamp(startDate),
            "title":title,
            "address":["city":city, "country":country, "name":name, "zip":zip, "thoroughfare":thoroughfare],
            "archived" : false
            ] as [String : Any]
        if placeUid != "" {
            data["place"] = FirebaseService.shared.dbPlace.document(placeUid)
        }
        
        refNewEvent.updateData(data)
        { err in
            if let error = err {
                print("***EVENT REALM UPDATE ERROR: [\(error)***")
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    func deleteEvent(_ uid: String, completion:@escaping (Bool) -> ()) {
        let refNewEvent = FirebaseService.shared.dbEvent.document(uid)
        refNewEvent.delete(completion: { err in
            if let error = err {
                print("***EVENT REALM UPDATE ERROR: [\(error)***")
                completion(false)
            } else {
                completion(true)
            }
        })
    }
    
    
}


extension EventRealm {
    open override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? EventRealm {
            return self.uid == object.uid
        } else {
            return false
        }
    }
    
    override var hash: Int {
        return uid.hashValue
    }
    
    func getCurrentEvent(_ uid: String) -> EventRealm? {
        if let event = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(uid)'").first as? EventRealm {
            return event
        }
        return nil
    }
    
    
}


enum EventType {
    case participate
    case display
    case create
}
