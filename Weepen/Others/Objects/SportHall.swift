//
//  SportHall.swift
//  Weepen
//
//  Created by Louis Cheminant on 12/11/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import RealmSwift


class SportHallRealm: Object {
    @objc dynamic var uid: String? = nil
    @objc dynamic var resume: String? = nil
    @objc dynamic var currency: String? = nil
    @objc dynamic var name: String? = nil
    @objc dynamic var thoroughfare: String? = nil
    @objc dynamic var zip: String? = nil
    @objc dynamic var city: String? = nil
    @objc dynamic var country: String? = nil
    @objc dynamic var calendarUid: String? = nil
    @objc dynamic var cover: Data? = nil
    @objc dynamic var urlCover: String? = nil
    
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    
    var hourlyRate = RealmOptional<Int>()
    var startDayHour = RealmOptional<Int>()
    var endDayHour = RealmOptional<Int>()
    
    @objc dynamic var sport: SportRealm? = nil
    
    override public class func primaryKey() -> String {
        return "uid"
    }
    
    convenience init?(dataFromAlgolia data: [String:Any]) {
        self.init()
        
        guard let uid = data["objectID"] as? String,
            let resume = data["description"] as? String,
            let reservation = data["reservation"] as? [String:Any],
            let currency = reservation["currency"] as? String,
            let hourlyRate = reservation["hourlyRate"] as? Int,
            let address = data["address"] as? [String:Any],
            let name = address["name"] as? String,
            let thoroughfare = address["thoroughfare"] as? String,
            let zip = address["zip"] as? String,
            let city = address["city"] as? String,
            let country = address["country"] as? String,
//            let calendarUid = data["calendarUid"] as? String,
            let geoloc = data["_geoloc"] as? [String:Any],
            let latitude = geoloc["lat"] as? Double,
            let longitude = geoloc["lng"] as? Double,
            let sportTitle = data["sportUID"] as? String,
            //            let startDayHour = data["startDayHour"] as? Int,
            //            let endDayHour = data["endDayHour"] as? Int,
            let sport = CacheService.shared.getDataFromDB(object: SportRealm()).filter("sportUUID = '\(sportTitle)'").first as? SportRealm else {
                return nil
        }
        self.uid = uid
        self.resume = resume
        self.currency = currency
        self.name = name
        self.thoroughfare = thoroughfare
        self.zip = zip
        self.city = city
        self.country = country
        self.latitude.value = latitude
        self.longitude.value = longitude
        self.hourlyRate.value = hourlyRate
        //        self.dateStart = Date(timeIntervalSince1970: start_date)
        //        self.dateEnd = Date(timeIntervalSince1970: end_date)
        self.sport = sport
    }

    
}


extension SportHallRealm {
    func orderPlaces(_ newPlaces: [SportHallRealm]) -> ([Object], [Object], [Object]) {
        let removePlaces = Array((Set(CacheService.shared.getDataFromDB(object: SportHallRealm())).subtracting(Set(newPlaces))))
        let updatePlaces = Array(Set(newPlaces).intersection(CacheService.shared.getDataFromDB(object: SportHallRealm())))
        let addPlaces = Array(Set(Set(CacheService.shared.getDataFromDB(object: SportHallRealm())).symmetricDifference(Set(newPlaces))).intersection(Set(newPlaces)))
        return (removePlaces, updatePlaces, addPlaces)
    }
    
    func addPlaces(_ places: [Object]) {
        CacheService.shared.addData(objects: places)
    }
    
    func removePlaces(_ places: [Object]) {
        CacheService.shared.deleteFromDb(objects: places) {}
    }
    
    func updatePlaces(_ places: [Object]) {
        for case let place as SportHallRealm in places {
            if let oldPlace = CacheService.shared.getDatabase().objects(SportHallRealm.self).filter("uid = '\(place.uid!)'").first {
                if oldPlace.resume != place.resume {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.resume = place.resume }
                    }
                }
                if oldPlace.currency != place.currency {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.currency = place.currency }
                    }
                }
                if oldPlace.name != place.name {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.name = place.name }
                    }
                }
                if oldPlace.thoroughfare != place.thoroughfare {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.thoroughfare = place.thoroughfare }
                    }
                }
                if oldPlace.zip != place.zip {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.zip = place.zip }
                    }
                }
                if oldPlace.city != place.city {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.city = place.city }
                    }
                }
                if oldPlace.country != place.country {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.country = place.country }
                    }
                }
                if oldPlace.latitude.value != place.latitude.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.latitude.value = place.latitude.value }
                    }
                }
                if oldPlace.longitude.value != place.longitude.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.longitude.value = place.longitude.value }
                    }
                }
                if oldPlace.hourlyRate.value != place.hourlyRate.value {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.hourlyRate.value = place.hourlyRate.value }
                    }
                }
                if oldPlace.sport != place.sport {
                    try! CacheService.shared.getDatabase().write {
                        if !oldPlace.isInvalidated { oldPlace.sport = place.sport }
                    }
                }
            }
        }
    }
    
    func downloadCoverSH(_ place: SportHallRealm) {
        if let strURL = place.urlCover {
            let url = URL(string: strURL)
            StorageService.sharedStorage.getFile("places/\(place.uid!)/cover.jpg", url!) { (data) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        guard !place.isInvalidated else { return }
                        place.cover = data
                    }
                }
            }
        } else {
            StorageService.sharedStorage.getFile("places/\(place.uid!)/cover.jpg", completion: { (data, url) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        guard !place.isInvalidated else { return }
                        place.cover = data
                        place.urlCover = url.absoluteString
                    }
                }
            })
        }
    }
    
    
}


extension SportHallRealm {
    open override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? SportHallRealm {
            return self.uid == object.uid
        } else {
            return false
        }
    }
    
    override var hash: Int {
        return uid.hashValue
    }
    
    
}
