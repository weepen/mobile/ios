//
//  FilterRealm.swift
//  Weepen
//
//  Created by Louis Cheminant on 06/08/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift

class FilterRealm: Object {
    @objc dynamic var ID = -1
    @objc dynamic var isPrivate = false
    @objc dynamic var isSponsored = false
    @objc dynamic var nbFilter = 1


    @objc dynamic var date: Date? = nil
    var sports = List<SportRealm>()
    var radius = RealmOptional<Int>()
    var minPrice = RealmOptional<Int>()
    var maxPrice = RealmOptional<Int>()
    
    override static func primaryKey() -> String? {
        return "ID"
    }
    
    convenience init?(id: Int, radius: Int, isPrivate: Bool, isSponsored: Bool, minPrice: Int, maxPrice: Int, date: Date) {
        self.init()

        self.radius.value = radius
        self.isPrivate = isPrivate
        self.isSponsored = isSponsored
        self.minPrice.value = minPrice
        self.maxPrice.value = maxPrice
        self.date = date
        
        self.ID = id
    }
}


