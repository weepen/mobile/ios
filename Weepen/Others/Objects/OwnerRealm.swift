//
//  OwnerRealm.swift
//  Weepen
//
//  Created by Louis Cheminant on 9/17/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase
import FirebaseFirestore

class OwnerRealm: Object {
    @objc dynamic var uuid: String? = nil
    @objc dynamic var name: String? = nil
    @objc dynamic var picture: Data? = nil
    @objc dynamic var urlPicture: String? = nil

    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var sports = List<SportRealm>()
    let events = LinkingObjects(fromType: EventRealm.self, property: "owner")
    
    
    override static func primaryKey() -> String? {
        return "uuid"
    }
    
    convenience init?(uid: String) {
        self.init()
        self.uuid = uid
    }
    
    func getOwnerInfo(_ uid: String, completion:@escaping (String, Double, Double) -> ()) {
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").getDocument(completion: { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            guard let data = document.data(), let name = data["firstName"] as? String, let place = data["place"] as? GeoPoint else {
                print("***USER REALM ERROR: [Data problems. Check Firebase]***")
                return
            }
            let (latitude, longitude) = FirebaseService.shared.location(place)
            completion(name, latitude, longitude)
        })
    }
    
    func getSportOwnerInfo(_ uid: String, completion:@escaping (List<SportRealm>) -> ()) {
        let sports = List<SportRealm>()
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").collection("sports").getDocuments() { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            for document in document.documents {
                if let ref: DocumentReference = document.data()["ref"] as! DocumentReference? {
                    let uid = ref.path.replacingOccurrences(of: FirebaseService.shared.pathSport, with: "", options: .literal, range: nil)
                    if let sport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(uid)'").first {
                        sports.append(sport)
                        
                    }
                }
            }
            completion(sports)
        }
    }
    
    func getPictureOwner(_ uid: String, _ url: String?, completion:@escaping (Data, String) -> ()) {
        if let strURL = url {
            let url = URL(string: strURL)
            StorageService.sharedStorage.getFile("users/\(uid)/profile.jpg", url!) { (data) in
                completion(data, strURL)
            }
        } else {
            StorageService.sharedStorage.getFile("users/\(uid)/profile.jpg", completion: { (data, url) in
                completion(data, url.absoluteString)
            })
            
        }
    }
    
    
}
