//
//  SportRealm.swift
//  Weepen
//
//  Created by Louis Cheminant on 02/07/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift

class SportRealm: Object {
    @objc dynamic var ID = -1
    
    @objc dynamic var sportUUID: String? = nil
    @objc dynamic var title: String? = nil
    @objc dynamic var urlCover: String? = nil
    @objc dynamic var urlLogo: String? = nil

    @objc dynamic var cover: Data? = nil
    @objc dynamic var logo: Data? = nil
    
    var codeColor = RealmOptional<Int>()
    
    override static func primaryKey() -> String? {
        return "ID"
    }
    
    convenience init?(sportUUID: String, title: String, colorCode: Int) {
        self.init()
        self.sportUUID = sportUUID
        self.title = title.lowercased()
        self.codeColor.value = colorCode
        if let oldSport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(self.sportUUID!)'").first {
            updateSport(oldSport)
            return nil
        }
        self.ID = CacheService.shared.getDataFromDB(object: SportRealm()).count
    }
    
    private func updateSport(_ oldSport: SportRealm) {
        try! CacheService.shared.getDatabase().write {
            oldSport.title = self.title
            oldSport.urlCover = self.urlCover
            oldSport.urlLogo = self.urlLogo
            oldSport.codeColor = self.codeColor
            downloadLogoSport(oldSport)
            downloadCoverSport(oldSport)
        }
    }
    
    func addSnapshotListenerSport() {
        FirebaseService.shared.dbSport.addSnapshotListener { querySnapshot, error in
            guard let querySnapshot = querySnapshot, error == nil else {
                print("***SPORT REALM ERROR: [\(error!)]***")
                return
            }
            for document in querySnapshot.documents {
                guard let title = document.data()["name"] as? String, let color = document.data()["color"] as? String else {
                    print("***SPORT REALM ERROR: [Data problems. Check Firebase]***")
                    return
                }
                if let hexValue = UInt(String(color.suffix(6)), radix: 16) {
                    if let sport = SportRealm(sportUUID: document.documentID, title: title, colorCode: Int(hexValue)) {
                        CacheService.shared.addData(object: sport)
                        self.downloadLogoSport(sport)
                        self.downloadCoverSport(sport)
                    }
                }
            }
        }
    }
    
    func getSport(completion:@escaping (Bool) -> ()) {
        FirebaseService.shared.dbSport.getDocuments(completion: { (querySnapshot, error) in
            guard let querySnapshot = querySnapshot, error == nil else {
                print("***SPORT REALM ERROR: [\(error!)]***")
                return
            }
            for document in querySnapshot.documents {
                guard let title = document.data()["name"] as? String, let color = document.data()["color"] as? String else {
                    print("***SPORT REALM ERROR: [Data problems. Check Firebase]***")
                    return
                }
                if let hexValue = UInt(String(color.suffix(6)), radix: 16) {
                    if let sport = SportRealm(sportUUID: document.documentID, title: title, colorCode: Int(hexValue)) {
                        CacheService.shared.addData(object: sport)
                        self.downloadLogoSport(sport)
                        self.downloadCoverSport(sport)
                        completion(true)
                    }
                }
            }
        })
    }
    
    private func downloadLogoSport(_ sport: SportRealm) {
        if let strURL = sport.urlLogo {
            let url = URL(string: strURL)
            StorageService.sharedStorage.getFile("sports/\(sport.sportUUID!)/logo.png", url!) { (data) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        sport.logo = data
                    }
                }
            }
        } else {
            StorageService.sharedStorage.getFile("sports/\(sport.sportUUID!)/logo.png", completion: { (data, url) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        sport.logo = data
                        sport.urlLogo = url.absoluteString
                    }
                }
            })
        }
    }
    
    private func downloadCoverSport(_ sport: SportRealm) {
        if let strURL = sport.urlCover {
            let url = URL(string: strURL)
            StorageService.sharedStorage.getFile("sports/\(sport.sportUUID!)/cover.png", url!) { (data) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        sport.cover = data
                    }
                }
            }
        } else {
            StorageService.sharedStorage.getFile("sports/\(sport.sportUUID!)/cover.png", completion: { (data, url) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        sport.cover = data
                        sport.urlCover = url.absoluteString
                    }
                }
            })
        }
    }
    
    
}
