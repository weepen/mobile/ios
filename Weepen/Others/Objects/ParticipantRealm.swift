//
//  ParticipantRealm.swift
//  Weepen
//
//  Created by Louis Cheminant on 9/17/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift
import Firebase
import FirebaseFirestore

class ParticipantRealm: Object {
    
    @objc dynamic var uuid: String? = nil
    @objc dynamic var name: String? = nil
    @objc dynamic var picture: Data? = nil
    @objc dynamic var urlPicture: String? = nil
    
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var sports = List<SportRealm>()
    let events = LinkingObjects(fromType: EventRealm.self, property: "participants")

    override static func primaryKey() -> String? {
        return "uuid"
    }
    
    convenience init?(uid: String) {
        self.init()
        self.uuid = uid
    }
 
    func orderParticipants(_ newParticipants: [ParticipantRealm], _ event: EventRealm) -> ([Object], [Object]) {
        let removeParticipants = Array((Set(event.participants).subtracting(Set(newParticipants))))
        let addParticipants = Array(Set(Set(event.participants).symmetricDifference(Set(newParticipants))).intersection(Set(newParticipants)))
        return (removeParticipants, addParticipants)
    }
    
    func removeParticipants(_ participants: [Object], _ event: EventRealm) {
        for case let participant as ParticipantRealm in participants {
            if let existParticipant = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(participant.uuid!)'").first {
                if let index = event.participants.index(of: (existParticipant as! ParticipantRealm)) {
                    try! CacheService.shared.getDatabase().write {
                        guard !event.isInvalidated else { return }
                        event.participants.remove(at: index)
                    }
                }
                if (existParticipant as! ParticipantRealm).events.count == 0 {
                    CacheService.shared.deleteFromDb(object: (existParticipant as! ParticipantRealm)) {}
                }
//                if let index = (existParticipant as! ParticipantRealm).events.index(of: event) {
//                    try! CacheService.shared.getDatabase().write {
//                        guard !existParticipant.isInvalidated else { return }
//                        (existParticipant as! ParticipantRealm).events.remove(at: index)
//                    }
//                    if (existParticipant as! ParticipantRealm).events.count == 0 {  }
//                }
            }
        }
    }
    
    func addParticipants(_ participants: [Object], _ event: EventRealm) {
        for case let participant as ParticipantRealm in participants {
            if let existParticipant = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(participant.uuid!)'").first {
                try! CacheService.shared.getDatabase().write {
                    guard !event.isInvalidated, !existParticipant.isInvalidated else { return }
                    event.participants.append(existParticipant as! ParticipantRealm)
                }
            } else {
                CacheService.shared.addData(object: participant)
                try! CacheService.shared.getDatabase().write {
                    guard !event.isInvalidated else { return }
                    event.participants.append(participant)
                }
            }
        }
    }
    
    func updateParticipants(_ participants: [Object]) {
        for case let participant as ParticipantRealm in participants {
            try! CacheService.shared.getDatabase().write {
                if let oldParticipant = CacheService.shared.getDatabase().objects(ParticipantRealm.self).filter("uuid = '\(participant.uuid!)'").first {
                    oldParticipant.name = participant.name
                    oldParticipant.picture = participant.picture
                    oldParticipant.sports = participant.sports
                    oldParticipant.latitude.value = participant.latitude.value
                    oldParticipant.longitude.value = participant.longitude.value
                }
            }
        }
    }
    
    
}


extension ParticipantRealm {
    func getParticipantInfo(_ uid: String, completion:@escaping (String, Double, Double) -> ()) {
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").getDocument(completion: { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            guard let data = document.data() else { return }
            guard let name = data["firstName"] as? String else {
                print("***USER REALM ERROR: [Data problems. Check Firebase]***")
                return
            }
            guard let place = data["place"] as? GeoPoint else {
                completion(name, 0, 0)
                return
            }
            let (latitude, longitude) = FirebaseService.shared.location(place)
            completion(name, latitude, longitude)
        })
    }
    
    func getSportParticipantInfo(_ uid: String, completion:@escaping (List<SportRealm>) -> ()) {
        let sports = List<SportRealm>()
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").collection("sports").getDocuments() { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            for document in document.documents {
                if let ref: DocumentReference = document.data()["ref"] as! DocumentReference? {
                    let uid = ref.path.replacingOccurrences(of: FirebaseService.shared.pathSport, with: "", options: .literal, range: nil)
                    if let sport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(uid)'").first {
                        sports.append(sport)
                    }
                }
            }
            completion(sports)
        }
    }
    
    func getPictureParticipant(_ uid: String, _ url: String?, completion:@escaping (Data, String) -> ()) {
        if let strURL = url {
            let url = URL(string: strURL)
            StorageService.sharedStorage.getFile("users/\(uid)/profile.jpg", url!) { (data) in
                completion(data, strURL)
            }
        } else {
            StorageService.sharedStorage.getFile("users/\(uid)/profile.jpg", completion: { (data, url) in
                completion(data, url.absoluteString)
            })
        }
    }
    
    
}


extension ParticipantRealm {
    func setParticipant(_ idEvent: String, _ idParticiapnt: String, completion:@escaping (Bool) -> ()) {
        let result = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(idEvent)'")
        if result.count > 0 {
            if let event = result.first as? EventRealm {
                let refEventParticipant = FirebaseService.shared.dbEvent.document(event.uid).collection("participants").document(idParticiapnt)
                let infosParticipant = ["ref" : FirebaseService.shared.dbUser.document(idParticiapnt)] as [String : Any]
                
                refEventParticipant.setData(infosParticipant) { dataError in
                    if let error = dataError {
                        print("***EVENT REALM SET PARTICIPANT ERROR: [\(error)***")
                        completion(false)
                    } else {
                        completion(true)
                    }
                }
            }
        }
    }
    
    func removeParticipant(_ idEvent: String, _ idParticiapnt: String, completion:@escaping (Bool) -> ()) {
        let result = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(idEvent)'")
        if result.count > 0 {
            if let event = result.first as? EventRealm {
                let refEventParticipant = FirebaseService.shared.dbEvent.document(event.uid).collection("participants").document(idParticiapnt)
                refEventParticipant.delete() { dataError in
                    if let error = dataError {
                        print("***EVENT REALM SET PARTICIPANT ERROR: [\(error)***")
                        completion(false)
                    } else {
                        completion(true)
                    }
                }
            }
        }
    }
    
    
}


extension ParticipantRealm {
    open override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? ParticipantRealm {
            return self.uuid == object.uuid
        } else {
            return false
        }
    }
    
    override var hash: Int {
        return uuid.hashValue
    }
    
    
}
