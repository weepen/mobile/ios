//
//  UserRealm.swift
//  Weepen
//
//  Created by Louis Cheminant on 02/07/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift
import FirebaseFirestore

class UserRealm: Object {
    @objc dynamic var ID = -1
    
    @objc dynamic var uuid: String? = nil
    @objc dynamic var name: String? = nil
    @objc dynamic var email: String? = nil
    @objc dynamic var phone: String? = nil
    @objc dynamic var urlPicture: String? = nil
    @objc dynamic var birthday: Date? = nil
    @objc dynamic var picture: Data? = nil
    
    var latitude = RealmOptional<Double>()
    var longitude = RealmOptional<Double>()
    var sports = List<SportRealm>()
    override static func primaryKey() -> String? {
        return "ID"
    }
    
    static var listenerPrivate: ListenerRegistration?
    static var listenerPublic: ListenerRegistration?
    static var listenerSports: ListenerRegistration?
    
    convenience init?(uid: String, name: String, email: String?, phone: String?, birthday: Date, latitude: Double, longitude: Double, sports: List<SportRealm>) {
        self.init()
        self.uuid = uid
        self.name = name
        self.email = email
        self.phone = phone
        self.birthday = birthday
        self.latitude.value = latitude
        self.longitude.value = longitude
        self.sports = sports
        self.ID = CacheService.shared.getDataFromDB(object: UserRealm()).count
    }
    
    
}

// MARK: Listener Functions
extension UserRealm {
    
    func addSnapshotListenerUserPrivate(_ uid: String, completion:@escaping (Date) -> ()) {
        UserRealm.listenerPrivate = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PRIVATE").addSnapshotListener({ (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR UPDATE : [\(error!)]***")
                return
            }
            guard let birthday = document.data()!["dateOfBirth"] as? Timestamp else {
                print("***USER REALM ERROR: [Data problems. Check Firebase]***")
                return
            }
            completion(birthday.dateValue())
        })
    }
    
    func removeSnapshotListenerUserPrivate() {
        if (UserRealm.listenerPrivate != nil) {
            UserRealm.listenerPrivate!.remove()
        }
    }
    
    func addSnapshotListenerUserPublic(_ uid: String, completion:@escaping (String) -> ()) {
        UserRealm.listenerPublic = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").addSnapshotListener({ (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            guard let name = document.data()!["firstName"] as? String else {
                print("***USER REALM ERROR: [Data problems. Check Firebase]***")
                return
            }
            completion(name)
        })
    }
    
    func removeSnapshotListenerUserPublic() {
        if (UserRealm.listenerPublic != nil) {
            UserRealm.listenerPublic!.remove()
        }
    }
    
    func addSnapshotListenerUserSports(_ uid: String, completion:@escaping (List<SportRealm>) -> ()) {
        UserRealm.listenerSports = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").collection("sports").addSnapshotListener({ (queryDocument, error) in
            let sports = List<SportRealm>()
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            for document in document.documents {
                if let ref: DocumentReference = document.data()["ref"] as! DocumentReference? {
                    let uid = ref.path.replacingOccurrences(of: FirebaseService.shared.pathSport, with: "", options: .literal, range: nil)
                    if let sport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(uid)'").first {
                        sports.append(sport)
                        
                    }
                }
            }
            completion(sports)
        })
    }
    
    func removeSnapshotListenerUserSports() {
        if (UserRealm.listenerSports != nil) {
            UserRealm.listenerSports!.remove()
        }
    }
    
    func setTokenRegistrationUser(_ uid: String, _ token: String) {
        let refToken = FirebaseService.shared.dbUser.document(uid)
        let infosToken = ["registrationToken" : token] as [String : Any]
        refToken.setData(infosToken, completion: { (dataError) in
            if let error = dataError {
                print(error)
            }
        })
    }
    
    
    
}

// MARK: Getter Functions
extension UserRealm {
    func getUserPrivateInfo(_ uid: String, completion:@escaping (Date?) -> ()) {
        print(uid)
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PRIVATE").getDocument(completion: { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            guard let birthday = document.data()!["dateOfBirth"] as? Timestamp else {
                print("***USER REALM BIRTHDAY ERROR: [Data problems. Check Firebase]***")
                completion(nil)
                return
            }
            completion(birthday.dateValue())
        })
    }
    
    func getUserPublicInfo(_ uid: String, completion:@escaping (String, Double, Double) -> ()) {
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").getDocument(completion: { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            guard let name = document.data()!["firstName"] as? String, let place = document.data()!["place"] as? GeoPoint else {
                print("***USER REALM ERROR: [Data problems. Check Firebase]***")
                return
            }
            let (latitude, longitude) = FirebaseService.shared.location(place)
            completion(name, latitude, longitude)
        })
    }
    
    func getSportInfo(_ uid: String, completion:@escaping (List<SportRealm>) -> ()) {
        let sports = List<SportRealm>()
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").collection("sports").getDocuments() { (queryDocument, error) in
            guard let document = queryDocument, error == nil else {
                print("***USER REALM ERROR: [\(error!)]***")
                return
            }
            for document in document.documents {
                if let ref: DocumentReference = document.data()["ref"] as! DocumentReference? {
                    let uid = ref.path.replacingOccurrences(of: FirebaseService.shared.pathSport, with: "", options: .literal, range: nil)
                    if let sport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(uid)'").first {
                        sports.append(sport)
                        
                    }
                }
            }
            completion(sports)
        }
    }
    
    func getPictureUser(_ user: UserRealm, completion:@escaping (Data) -> ()) {
        if let strURL = user.urlPicture {
            let url = URL(string: strURL)
            StorageService.sharedStorage.getFile("users/\(user.uuid!)/profile.jpg", url!, completion: { (data) in
                try! CacheService.shared.getDatabase().write(transaction: {
                    user.picture = data
                }, completion: {
                    completion(data)
                })
            })
        } else {
            StorageService.sharedStorage.getFile("users/\(user.uuid!)/profile.jpg", completion: { (data, url) in
                DispatchQueue.main.async  {
                try! CacheService.shared.getDatabase().write(transaction: {
                    guard !user.isInvalidated else { completion(data); return }
                    user.picture = data
                    user.urlPicture = url.absoluteString
                }, completion: {
                    completion(data)
                })
                }
            })
        }
    }
}

// MARK: Setter Functions
extension UserRealm {
    func setUserPrivateInfo(_ uid: String, _ birthday: Date, completion:@escaping (Bool) -> ()) {
        let refPrivate = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PRIVATE")
        let infosPrivate = ["dateOfBirth" : FirebaseService.shared.timestamp(birthday)]
        refPrivate.setData(infosPrivate) { (dataError) in
            if let error = dataError {
                print(error)
            } else {
                completion(true)
            }
        }
    }
    
    func setUserPublicInfo(_ uid: String, _ name: String, _ latitude: Double, _ longitude: Double, completion:@escaping (Bool) -> ()) {
        let refPublic = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC")
        let infosPublic = ["firstName" : name, "place" : FirebaseService.shared.location(latitude, longitude)] as [String : Any]
        refPublic.setData(infosPublic) { dataError in
            if let error = dataError {
                print(error)
                completion(false)
            } else {
                completion(true)
            }
        }
    }
    
    func setSportInfo(_ uid: String, sports: [Any], completion:@escaping (Bool) -> ()) {
        let refPublicSport = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").collection("sports")
        for sport in sports {
            refPublicSport.addDocument(data: ["ref": sport]) { dataError in
                if let error = dataError {
                    print(error)
                } else {
                    completion(true)
                }
            }
        }
    }
    
    func removeSportInfo(_ uid: String, sports: [String], completion:@escaping (Bool) -> ()) {
        let refPublicSport = FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").collection("sports")
        refPublicSport.getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in querySnapshot!.documents {
                    if let ref: DocumentReference = document.data()["ref"] as! DocumentReference? {
                        let uid = ref.path.replacingOccurrences(of: FirebaseService.shared.pathSport, with: "", options: .literal, range: nil)
                        for sport in sports {
                            if sport == uid {
                                refPublicSport.document(document.documentID).delete()
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func setPictureUser(_ uid: String, _ user: UserRealm) {
        StorageService.sharedStorage.setFile("users/\(uid)/profile.jpg", data: user.picture!) { (url) in
            try! CacheService.shared.getDatabase().write { user.urlPicture = url.absoluteString }
        }
    }
    
    func logoutUser(completion:@escaping (Bool, SignInViewController) -> ()) {
        AuthService.sharedAuth.signOut { (result) in
            if result {
                CacheService.shared.deleteAllDatabase()
                SportRealm().getSport(completion: { (result) in })
                let storyboard = UIStoryboard(name: "SignIn", bundle: nil)
                if let previousView = storyboard.instantiateViewController(withIdentifier: "InitialController") as? SignInViewController {
                    completion(true, previousView)
                }
            }
            completion(false, SignInViewController())
        }
    }
}
