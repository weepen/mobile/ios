//
//  LoadActivityIndicatorService.swift
//  Weepen
//
//  Created by Louis Cheminant on 8/23/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class LoadActivityIndicatorService {
    
    static let sharedLAI = LoadActivityIndicatorService()
    
    var bgView = UIView()
    var bgLoader = UIView()
    var progressView = UIProgressView()
    var label = UILabel()
    var loader: NVActivityIndicatorView!
    let sizebgLoader: CGFloat = 70.0
    let marginLoader: CGFloat = 30.0
    
    func addActivityIndicator(_ frame: CGRect, isProgess: Bool? = false) -> UIView {
        bgView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        bgView.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        bgView.alpha = 0
        
        if isProgess! {
            bgLoader = UIView(frame: CGRect(x: 0, y: 0, width: sizebgLoader, height: sizebgLoader))
        } else {
            bgLoader = UIView(frame: CGRect(x: 0, y: 0, width: sizebgLoader, height: sizebgLoader+10))
        }
        bgLoader.center = bgView.center
        bgLoader.backgroundColor = ThemeManager.currentTheme().downView
        bgLoader.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
        
      
    
        loader = NVActivityIndicatorView(frame: CGRect(x: marginLoader/2, y: marginLoader/2, width: sizebgLoader-marginLoader, height: sizebgLoader-marginLoader), type: .ballPulse, color: ThemeManager.currentTheme().selectedFontColor, padding: 0)
        loader.startAnimating()
        
        progressView = UIProgressView(frame: CGRect(x: 8, y: sizebgLoader - 8, width: sizebgLoader-16, height: 3.0))
        progressView.tintColor = ThemeManager.currentTheme().selectedFontColor
        progressView.progress = 0.0
        progressView.layer.cornerRadius = progressView.frame.height/2
        
        label = UILabel(frame: CGRect(x: 8, y: progressView.frame.minY-8, width: sizebgLoader-16, height: 8))
        label.font = UIFont(name: ThemeManager.currentTheme().fontNameMedium, size: 5)
        label.textColor = ThemeManager.currentTheme().selectedFontColor
        label.textAlignment = .center
        
        label.alpha = 0
        progressView.alpha = 0
        
        bgLoader.addSubview(label)
        bgLoader.addSubview(progressView)
        bgLoader.addSubview(loader)
        bgView.addSubview(bgLoader)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        tap.numberOfTapsRequired = 1
        tap.numberOfTouchesRequired = 1
        bgLoader.addGestureRecognizer(tap)
        bgLoader.isUserInteractionEnabled = true


        
        UIView.animate(withDuration: 0.3) {
            self.bgView.alpha = 1
        }
        
        return bgView
    }
    
    func updateProgressView(_ nb: Float) {
        progressView.progress = nb
        label.text = "\(Int(nb*100))%"
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if progressView.alpha == 0 {
            progressView.alpha = 1
            label.alpha = 1
        } else {
            progressView.alpha = 0
            label.alpha = 0
        }
        
    }
    
    
    func removeActivityIndicator() {
        UIView.animate(withDuration: 0.3) {
            self.bgView.alpha = 0
        }
        self.bgView.removeFromSuperview()
    }

    
}
