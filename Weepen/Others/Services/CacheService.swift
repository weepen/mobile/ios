//
//  DBManager.swift
//  Weepen
//
//  Created by Louis Cheminant on 28/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift

class CacheService {
    
    private var database:Realm
    static let shared = CacheService()
    
    private init() {
        database = try! Realm()
    }
    
    func getDataFromDB(object: Object, _ sortedBy: String? = "") -> Results<Object> {
        let results: Results<Object> = database.objects(type(of: object))
    
        if sortedBy != "" {
            return results.sorted(byKeyPath: sortedBy!, ascending: true)
        }
        return results

    }
    
    func addData(object: Object) {
        try! database.write {
            database.add(object, update: true)
            print("Added new object")
        }
    }
    
    func addData(objects: [Object]) {
        try! database.write {
            database.add(objects, update: true)
            print("Added new object")
        }
    }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(objects: Results<Object>, completion:@escaping () -> ()) {
        try! database.write(transaction: {
            database.delete(objects)
        }, completion: {
            completion()
        })
    }
    
    func deleteFromDb(objects: [Object], completion:@escaping () -> ()) {
        try! database.write(transaction: {
            database.delete(objects)
        }, completion: {
            completion()
        })
    }
    
    func deleteFromDb(object: Object, completion:@escaping () -> ()) {
        try! database.write(transaction: {
            database.delete(object)
        }, completion: {
            completion()
        })
    }
    
    func getDatabase() -> Realm {
        return database
    }
    
    func objectExist(id: String, object: Object) -> Bool {
        return database.object(ofType: Object.self, forPrimaryKey: id) != nil
    }
    
}


