//
//  StorageService.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/07/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import FirebaseStorage
import RealmSwift

class StorageService {
    
    static let sharedStorage = StorageService()
    
    static let storage = Storage.storage()
    static let storageRef = storage.reference()
    
    
    func getFile(_ path: String, _ url: URL, completion:@escaping (Data) -> ()) {
        download(url) { (data) in
            completion(data)
        }
    }
    
    func getFile(_ path: String, completion:@escaping (Data, URL) -> ()) {
        let refFile = StorageService.storageRef.child(path)
        refFile.downloadURL { (url, error) in
            guard let downloadURL = url else {
                print("***STORAGE SERVICE ERROR: [\(error!)]***")
                return
            }
            self.download(downloadURL, completion: { (data) in
                completion(data, downloadURL)
            })
        }
    }
    
    func download(_ url: URL, completion:@escaping (Data) -> ()) {
        let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, urlResponse, error) in
            guard let data = data, error == nil else {
                print("***STORAGE SERVICE ERROR: [\(error!)]***")
                return
            }
            completion(data)
        })
        task.resume()
    }
    
    func setFile(_ path: String, data: Data, completion:@escaping (URL) -> ()) {
        let refFile = StorageService.storageRef.child(path)
        refFile.putData(data, metadata: nil) { (metadata, error) in
            guard error == nil else {
                print("***STORAGE SERVICE ERROR: [\(error!)]***")
                return
            }
            refFile.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    print("***STORAGE SERVICE ERROR: [\(error!)]***")
                    return
                }
                completion(downloadURL)
            }
        }
    }
    
    
}
