//
//  LocationService.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import MapKit


protocol LocationUpdateProtocol {
    func locationDidUpdateToLocation(latitude: Double, longitude: Double)
    func locationDidChangeAuthorization(status: Int)
}

class LocationService: NSObject {
    static let shared = LocationService()
    private var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var delegate : LocationUpdateProtocol!
    let distanceFormatter = MKDistanceFormatter()
    var isLocalize = false
    var isJustUpdate = false
    
    private override init () {
        super.init()
        self.locationManager.delegate = self
    }
    
    func askAuthorizationLocation() {
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    func startUpdatingLocation(_ _isJustUpdate: Bool = false) {
        isLocalize = true
        isJustUpdate = _isJustUpdate
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
        self.locationManager.pausesLocationUpdatesAutomatically = true
        self.locationManager.startUpdatingLocation()
//        if currentLocation != nil {
//            DispatchQueue.main.async {
//                self.delegate.locationDidUpdateToLocation(latitude: self.currentLocation!.coordinate.latitude, longitude: self.currentLocation!.coordinate.longitude)
//            }
//        }
    }
    
    func stopUpdatingLocation() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func isAuthorized() -> Int {
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                return -1
            case .restricted, .denied:
                return 0
            case .authorizedAlways, .authorizedWhenInUse:
                return 1
            }
        } else {
            return 0
        }
    }
    
    func getCity(completion:@escaping (String) -> ()) {
        let geoCoder = CLGeocoder()
        geoCoder.reverseGeocodeLocation(currentLocation!, completionHandler: { (placemarks, error) -> Void in
            if let city = placemarks?[0].locality {
                completion(city)
            }
        })
    }
    
    func getDistance(_ latitude: Double, _ longitude: Double) -> Double {
        guard let userLocation = currentLocation else { return 0 }
        let location = CLLocation(latitude: CLLocationDegrees(truncating: NSNumber(value: latitude)), longitude: CLLocationDegrees(truncating: NSNumber(value: longitude)))
        return location.distance(from: userLocation)
    }
    
    func getLocation(_ latitude: Double, _ longitude: Double) -> String {
        guard let userLocation = currentLocation else { return NSLocalizedString("LOADING", comment: "") }
        let location = CLLocation(latitude: CLLocationDegrees(truncating: NSNumber(value: latitude)), longitude: CLLocationDegrees(truncating: NSNumber(value: longitude)))
        distanceFormatter.units = .default
        return distanceFormatter.string(fromDistance: location.distance(from: userLocation))
    }
    
    
}

extension LocationService: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            print("UPDATE")
            currentLocation = location
            print(location.coordinate.latitude)
            print(location.coordinate.longitude)
            if !isJustUpdate {
                DispatchQueue.main.async {
                    self.delegate.locationDidUpdateToLocation(latitude: self.currentLocation!.coordinate.latitude, longitude: self.currentLocation!.coordinate.longitude)
                    self.stopUpdatingLocation()
                }
            } else {
                self.stopUpdatingLocation()
            }
        } else {
            print("error location")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        DispatchQueue.main.async {
            switch status {
            case .notDetermined:
                if let dg = self.delegate {
                    dg.locationDidChangeAuthorization(status: -1)
                }
            case .authorizedAlways, .authorizedWhenInUse:
                if let dg = self.delegate {
                    dg.locationDidChangeAuthorization(status: 1)
                }
            default:
                if let dg = self.delegate {
                    dg.locationDidChangeAuthorization(status: 0)
                }
            }

        }
    }
    
    
}
