//
//  FirebaseService.shared.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/07/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import FirebaseCore
import FirebaseFirestore

class FirebaseService {
    
    var db: Firestore
    let dbUser: CollectionReference
    let dbSport: CollectionReference
    let dbEvent: CollectionReference
    let dbChat: CollectionReference
    let dbPlace: CollectionReference
    let pathUser: String
    let pathSport: String
    let pathEvent: String
    let pathChat: String
    let pathPlace: String

    static let shared = FirebaseService()
    
    init(){
        db = Firestore.firestore()
        let settings = db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        db.settings = settings
        pathUser = "weepen/\(EnvManager.currentEnv())/users/"
        pathSport = "weepen/\(EnvManager.currentEnv())/sports/"
        pathEvent = "weepen/\(EnvManager.currentEnv())/events/"
        pathChat = "weepen/\(EnvManager.currentEnv())/chat/"
        pathPlace = "weepen/\(EnvManager.currentEnv())/places/"
        dbUser = db.collection(pathUser)
        dbSport = db.collection(pathSport)
        dbEvent = db.collection(pathEvent)
        dbChat = db.collection(pathChat)
        dbPlace = db.collection(pathPlace)
    }
    
    func location(_ latitude: Double, _ longitude: Double) -> GeoPoint {
        return GeoPoint(latitude: latitude, longitude: longitude)
    }
    
    func location(_ geoPoint: GeoPoint) -> (Double, Double) {
        return (geoPoint.latitude, geoPoint.longitude)
    }
    
    func timestamp(_ date: Date) -> Timestamp {
        return Timestamp(date: date)
    }
}
