//
//  FCMService.swift
//  Weepen
//
//  Created by Louis Cheminant on 29/10/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import FirebaseMessaging
import Firebase


protocol FCMUProtocol {
    func messagingDidReceiveRemoteMessage(dictionary: NSDictionary)
    func messagingDidReceiveRegistrationToken(token: String)
}

class FCMService: NSObject {
    static let shared = FCMService()
    var delegate: FCMUProtocol!
    
    
    private override init () {
        super.init()
        Messaging.messaging().delegate = self
    }
    
    

}

extension FCMService: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")

        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        delegate.messagingDidReceiveRegistrationToken(token: fcmToken)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        delegate.messagingDidReceiveRemoteMessage(dictionary: remoteMessage.appData as NSDictionary)
        print("Received data message: \(remoteMessage.appData)")
    }
    
    
}
