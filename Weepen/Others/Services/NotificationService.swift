//
//  NotificationSrvice.swift
//  Weepen
//
//  Created by Louis Cheminant on 30/10/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseAuth


protocol NotificationProtocol {
    func userNotificationCenterDidReceiveWithCompletionHandler(completionHandler: @escaping () -> Void)
}

class NotificationService: NSObject {
    
    static let shared = NotificationService()
    var delegate: NotificationProtocol!

    
    private override init () {
        super.init()
        UNUserNotificationCenter.current().delegate = self
    }
    
    func createNotification(_ identifier: String, _ scope: String, _ title: String, _ subtitle: String?, _ body: String, _ sound: UNNotificationSound, _ userInfo: [AnyHashable : Any]) {
        let content = UNMutableNotificationContent()
        content.title = title
        if let _subtitle = subtitle { content.subtitle = _subtitle }
        content.body = body
        content.sound = sound
        content.categoryIdentifier = identifier
        
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest.init(identifier: identifier, content: content, trigger: trigger)
        
        let center = UNUserNotificationCenter.current()
        center.add(request)
    }
}


extension NotificationService: UNUserNotificationCenterDelegate {
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if let messageID = userInfo["gcm.message_id"] {
            print("Message ID: \(messageID)")
        }
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler()
            return
        }
        completionHandler()
    }

    
}
