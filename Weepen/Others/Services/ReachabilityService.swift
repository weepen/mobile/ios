//
//  ReachabilityService.swift
//  Weepen
//
//  Created by Louis Cheminant on 28/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Reachability
import NotificationBannerSwift


class ReachabilityService: NSObject {
    
    static let shared = ReachabilityService()
    
    let reachability = Reachability()!
    let banner = StatusBarNotificationBanner(title: "")
    var reachabilityStatus: Reachability.Connection = .none
    var isAlreadyUnavailable = false
    var isNetworkAvailable : Bool {
        return reachabilityStatus != .none
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged), name: Notification.Name.reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
            debugPrint("Could not start reachability notifier")
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: Notification.Name.reachabilityChanged, object: reachability)
    }
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! Reachability
        switch reachability.connection {
        case .none:
            isAlreadyUnavailable = true
            banner.backgroundColor = ThemeManager.currentTheme().errorColor
            banner.titleLabel?.text = "Erreur réseau"
            banner.autoDismiss = false
            banner.haptic = .heavy
            banner.show(bannerPosition: .top)
        case .wifi, .cellular:
            if isAlreadyUnavailable {
                banner.backgroundColor = ThemeManager.currentTheme().validationColor
                banner.titleLabel?.text = NSLocalizedString("Tu es de nouveau connecté", comment: "Tu es de nouveau connecté")
                banner.show(bannerPosition: .top)
                banner.haptic = .none
                banner.dismiss()
            }
        }
    }
    
    
}
