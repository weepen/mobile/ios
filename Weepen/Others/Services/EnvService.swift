//
//  EnvService.swift
//  Weepen
//
//  Created by Louis Cheminant on 9/24/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

enum Env: Int {
    case dev, prod
    
    var env: String {
        switch self {
        case .prod :
            return "prod"
        case .dev :
            return "dev"
        }
    }
}

let SelectedEnvKey = "SelectedEnv"

struct EnvManager {
    
    static func currentEnv() -> Env {
        if let storedEnv = (UserDefaults.standard.value(forKey: SelectedEnvKey) as AnyObject).integerValue {
            return Env(rawValue: storedEnv)!
        } else {
            return .dev
        }
    }
    
    static func applyEnv(env: Env) {
        UserDefaults.standard.setValue(env.rawValue, forKey: SelectedEnvKey)
        UserDefaults.standard.synchronize()
    }
}
