//
//  AuthService.swift
//  Weepen
//
//  Created by Louis Cheminant on 09/07/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import FirebaseAuth


class AuthService {
    
    static let sharedAuth = AuthService()
    
    var currentUser = Auth.auth().currentUser
    
    private var handle: AuthStateDidChangeListenerHandle?
    
    func signInSocial(_ credential: AuthCredential, completion:@escaping (Bool, String) -> ()) {
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            guard let uid = authResult?.user.uid, error == nil else {
                print("***AUTH SERVICE SIGNIN ERROR: [\(error!)]***")
                completion(false, "")
                return
            }
            completion(true, uid)
        }
    }
    
    func signOut(completion:@escaping (Bool) -> ()) {
        do {
            try Auth.auth().signOut()
            completion(true)
        } catch let error {
            print("***AUTH SERVICE SIGNOUT ERROR: [\(error)]***")
            completion(false)
        }
    }
    
    func deleteAccount(completion:@escaping (Bool) -> ()) {
        Auth.auth().currentUser?.delete(completion: { (error) in
            guard error == nil else {
                print("***AUTH SERVICE DELETE ERROR: [\(error!)]***")
                completion(false)
                return
            }
            completion(true)
        })
    }
    
    func addStateDidChangeListener(completion:@escaping (Auth) -> ()) {
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            guard let currentUserData = auth.currentUser else {
                print("***AUTH SERVICE UPDATE USER ERROR***")
                return
            }
            completion(auth)
            self.currentUser = currentUserData
        }
    }
    
    func removeStateDidChangeListener() {
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    func updatePhoneNumber(_ phone: String, completion:@escaping (Bool) -> ()) {
        Auth.auth().currentUser?.setValue("+33684149099", forKeyPath: "phoneNumber")
        Auth.auth().updateCurrentUser(Auth.auth().currentUser!) { (error) in
            guard error == nil else {
                print("***AUTH SERVICE ERROR: [\(error!)]***")
                completion(false)
                return
            }
            completion(true)
        }
    }
}
