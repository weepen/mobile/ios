//
//  AlgoliaService.swift
//  Weepen
//
//  Created by Louis Cheminant on 9/24/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import InstantSearchClient


class AlgoliaService {
    static let shared = AlgoliaService()
    
    let indexEvents: Index = Client(appID: "H380KHAXFH", apiKey: "55664b14517d22e33b6ca9d5abddc6f2").index(withName: "\(EnvManager.currentEnv())_firebase_events")
    let indexPlaces: Index = Client(appID: "H380KHAXFH", apiKey: "55664b14517d22e33b6ca9d5abddc6f2").index(withName: "\(EnvManager.currentEnv())_firebase_places")

    func getEvent(index: Index, search: String, filters: String, radius: UInt, latitude: Double, longitude: Double, completion:@escaping (Bool, UInt, [[String:Any]]) -> ()) {
        let query = Query(query: "query")

        query.filters = filters
        query.query = search
        query.hitsPerPage = 1000
        query.aroundLatLng = LatLng(lat: latitude, lng: longitude)
        if Locale.current.languageCode == "en" {
            let distanceMiles = Measurement(value: Double(radius), unit: UnitLength.miles)
            let kilometers = distanceMiles.converted(to: UnitLength.kilometers).value
            query.aroundRadius = .explicit(UInt(kilometers*1000))
        } else {
            query.aroundRadius = .explicit(radius*1000)
        }

        query.aroundPrecision = 100
        index.search(query, completionHandler: { (content, error) -> Void in
            guard error == nil, content != nil else {
                return completion(false, 0, [[:]])
            }
            guard let nbPage = content!["nbPages"] as? UInt, let data = content!["hits"] as? [[String:Any]] else {
                return completion(false, 0, [[:]])
            }
            completion(true, nbPage, data)
        })
    }
    
    
}
