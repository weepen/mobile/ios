//
//  DatePickerViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 03/08/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import JTAppleCalendar
import Ambience

@objc protocol DateFilterDelegate {
    func filterSelected(date: String)
}

class DatePickerViewController: UIViewController {
    
    @IBOutlet weak var bgBtn: UIVisualEffectView!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var yearLbl: UILabel!
    @IBOutlet weak var heightTimeCst: NSLayoutConstraint!
    
    let formatter = DateFormatter()
    let currentCalendar = Calendar.current
    let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm
    var delegate: DateFilterDelegate?
    var isSelected = false
    var isTime = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCalendarView()
        bgBtn.layer.cornerRadius = bgBtn.frame.height/2
        if !isTime {
            if let constraint = heightTimeCst {
                constraint.constant = 0
            }
        }
        if let date = filter.date {
            isSelected = true
            calendarView.selectDates([date])
        }
    }
    
    func setupCalendarView() {
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        
        calendarView.visibleDates { (visibleDates) in
            self.setupViewsOfCalendar(from: visibleDates)
        }
    }
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState) {
        guard let validCell = view as? DateCellCustom else { return }
        
        if cellState.isSelected {
            validCell.dateLbl.textColor = Ambience.currentState == .invert ? .white : .white
        } else {
            let components = currentCalendar.dateComponents([.day, .hour], from: Date(), to: cellState.date)
            if components.day == 0 && components.hour! < 0 {
                validCell.dateLbl.textColor = Ambience.currentState == .invert ? .black : .white
            } else {
                if cellState.dateBelongsTo == .thisMonth {
                    validCell.dateLbl.textColor = Ambience.currentState == .invert ? .white : .black
                } else {
                    validCell.dateLbl.textColor = .clear
                }
            }
        }
    }
    
    func handleCellSelected(view: JTAppleCell?, cellState: CellState) {
        guard let validCell = view as? DateCellCustom else { return }
        if cellState.isSelected {
            validCell.selectedView.isHidden = false
        } else {
            validCell.selectedView.isHidden = true
        }
        let components = currentCalendar.dateComponents([.day, .hour], from: Date(), to: cellState.date)
        if components.day == 0 && components.hour! < 0 {
            validCell.dDayView.isHidden = false
        } else {
            validCell.dDayView.isHidden = true
        }
    }
    
    func setupViewsOfCalendar(from visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!.date
        self.formatter.dateFormat = "YYYY"
        self.yearLbl.text = formatter.string(from: date)
        
        self.formatter.dateFormat = "MMMM"
        self.monthLbl.text = self.formatter.string(from: date)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        if !ambience { return .default }
        
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
        
    }
    
    @IBAction func clearTapped(_ sender: UIButton) {
        do {
            try CacheService.shared.getDatabase().write {
                filter.date = Date()
                
                delegate?.filterSelected(date: dateToString(Date()))
                dismiss(animated: true, completion: nil)
            }
        } catch let error {
            print("***DATE FILTER ERROR: [\(error)]***")
        }
    }
    
    
}

extension DatePickerViewController: JTAppleCalendarViewDataSource {
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = currentCalendar.timeZone
        formatter.locale = currentCalendar.locale
        
        let startDate = Date()
        let endDate = formatter.date(from: "2100 08 01")
        
        let parameter = ConfigurationParameters(startDate: startDate, endDate: endDate ?? Date())
        return parameter
    }
}

extension DatePickerViewController: JTAppleCalendarViewDelegate {
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "DateCell", for: indexPath) as! DateCellCustom
        cell.dateLbl.text = cellState.text
        let components = currentCalendar.dateComponents([.day, .hour], from: Date(), to: cellState.date)
        if components.day == 0 && components.hour! < 0 {
            cell.dDayView.isHidden = false
        }
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        if !isSelected {
            do {
                try CacheService.shared.getDatabase().write {
                    filter.date = date
                    delegate?.filterSelected(date: dateToString(date))
                    dismiss(animated: true, completion: nil)
                }
            } catch let error {
                print("***DATE FILTER ERROR: [\(error)]***")
            }
        }
        isSelected = false
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        handleCellSelected(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        setupViewsOfCalendar(from: visibleDates)
    }
    
    func dateToString(_ date: Date) -> String {
        formatter.dateFormat = "MMM dd"
        if formatter.string(from: Date()) == formatter.string(from: date) {
            return NSLocalizedString("TODAY", comment: "").capitalizingFirstLetter()
        }
        return formatter.string(from: date)
    }
    
}

class DateCellCustom: JTAppleCell {
    
    @IBOutlet weak var dDayView: UIView!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var dateLbl: UILabel!
    
}
