//
//  PopRadiusViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/5/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

protocol DataSliderViewControllerDelegate : class {
    
    func slideVCDismissed(_ radius : Float?)
    func slideVCValueChanged(_ radius : Float?)
}

class PopRadiusViewController: UIViewController {

    @IBOutlet weak var sliderRadius: UISlider!
    
    var delegate : DataSliderViewControllerDelegate?
    var currentRadius : Float? {
        didSet {
            updateSliderCurrentRadius()
        }
    }
    
    convenience init() {
        self.init(nibName: "PopRadiusViewController", bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateSliderCurrentRadius()
    }

    override func viewDidDisappear(_ animated: Bool) {
    }
    
    private func updateSliderCurrentRadius() {
        if let currentRadius = self.currentRadius {
            if let _sliderRadius = self.sliderRadius {
                _sliderRadius.value = currentRadius
            }
        }
    }
    
    @IBAction func didTouchUpInside(_ sender: UISlider) {
        self.dismiss(animated: true) {
            let radius = self.sliderRadius.value
            self.delegate?.slideVCDismissed(radius)
        }
    }
    
    @IBAction func didChangeValue(_ sender: UISlider) {
        let radius = self.sliderRadius.value
        self.delegate?.slideVCValueChanged(radius)
    }
    
}

extension PopRadiusViewController {
    
    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: true) {
            let radius = self.sliderRadius.value
            self.delegate?.slideVCDismissed(radius)
        }
    }
    
    
}
