//
//  PopRadius.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/8/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit


class PopRadiusSlider: NSObject {
    
    public typealias PopRadiusSliderCallback = (_ newRadius: Float, _ forCell: FilterRadiusCell, _ refresh: Bool)->()
    
    var radiusSliderVC = PopRadiusViewController()
    var popover : UIPopoverPresentationController?
    var cell : FilterRadiusCell!
    var dataChanged : PopRadiusSliderCallback?
    var presented = false
    var offset : CGFloat = 8.0
    
    public init(with cell: FilterRadiusCell) {
        self.cell = cell
        super.init()
    }
    
    
}

extension PopRadiusSlider: UIPopoverPresentationControllerDelegate {
    
    func pick(_ inViewController: UIViewController, initRadius: Float?, dataChanged: @escaping PopRadiusSliderCallback) {
        if presented { return }
        radiusSliderVC.delegate = self
        radiusSliderVC.modalPresentationStyle = UIModalPresentationStyle.popover
        radiusSliderVC.preferredContentSize = CGSize(width: 250, height: 46)
        radiusSliderVC.popoverPresentationController?.permittedArrowDirections = [.up]
        radiusSliderVC.popoverPresentationController?.backgroundColor = ThemeManager.currentTheme().downView
        radiusSliderVC.currentRadius = initRadius
        
        popover = radiusSliderVC.popoverPresentationController
        if let _popover = popover {
            _popover.sourceView = cell
            _popover.sourceRect = CGRect(x: cell.center.x-8,y: 34,width: 0,height: 0)
            _popover.delegate = self
            self.dataChanged = dataChanged
            inViewController.present(radiusSliderVC, animated: true, completion: nil)
            presented = true
        }
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
}

extension PopRadiusSlider: DataSliderViewControllerDelegate {
    
    func slideVCValueChanged(_ radius: Float?) {
        if let _dataChanged = dataChanged {
            if let _radius = radius {
                _dataChanged(_radius, cell, false)
            }
        }
    }
    
    func slideVCDismissed(_ radius: Float?) {
        if let _dataChanged = dataChanged {
            if let _radius = radius {
                _dataChanged(_radius, cell, true)
            }
        }
        presented = false
    }
    
    
}
