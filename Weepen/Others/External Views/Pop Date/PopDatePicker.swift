//
//  PopDatePicker.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/2/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

class PopDatePicker: NSObject {
    
    public typealias PopDatePickerCallback = (_ newDate: Date, _ forButton: UIButton)->()
    
    var datePickerVC = PopDateViewController()
    var popover : UIPopoverPresentationController?
    var button : UIButton!
    var dataChanged : PopDatePickerCallback?
    var presented = false
    var offset : CGFloat = 8.0
    
    public init(with button: UIButton) {
        self.button = button
        super.init()
    }
    
    
}

extension PopDatePicker: UIPopoverPresentationControllerDelegate {
    
    func pick(_ inViewController: UIViewController, initDate: Date?, dataChanged: @escaping PopDatePickerCallback) {
        if presented { return }
        datePickerVC.delegate = self
        datePickerVC.modalPresentationStyle = UIModalPresentationStyle.popover
        datePickerVC.preferredContentSize = CGSize(width: 500, height: 218)
        datePickerVC.popoverPresentationController?.backgroundColor = ThemeManager.currentTheme().downView
        datePickerVC.currentDate = initDate
        
        popover = datePickerVC.popoverPresentationController
        if let _popover = popover {
            _popover.sourceView = button
            _popover.sourceRect = CGRect(x: button.center.x,y: 0,width: 0,height: 0)
            _popover.delegate = self
            self.dataChanged = dataChanged
            inViewController.present(datePickerVC, animated: true, completion: nil)
            presented = true
        }
    }
    
    
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    
}

extension PopDatePicker: DataPickerViewControllerDelegate {
    
    func datePickerVCDismissed(_ date: Date?) {
        if let _dataChanged = dataChanged {
            if let _date = date {
                _dataChanged(_date, button)
            }
        }
        presented = false
    }
    
    
}

