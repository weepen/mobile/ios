//
//  DatePickerActionSheet.swift
//  iDoctors
//
//  Created by Valerio Ferrucci on 30/09/14.
//  Copyright (c) 2014 Tabasoft. All rights reserved.
//

import UIKit

protocol DataPickerViewControllerDelegate : class {
    
    func datePickerVCDismissed(_ date : Date?)
}

class PopDateViewController : UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var delegate : DataPickerViewControllerDelegate?
    var currentDate : Date? {
        didSet {
            updatePickerCurrentDate()
        }
    }
    
    convenience init() {
        self.init(nibName: "PopDateViewController", bundle: nil)
    }
    
    override func viewDidLoad() {
        updatePickerCurrentDate()
        datePicker.setValue(ThemeManager.currentTheme().selectedFontColor, forKeyPath: "textColor")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.delegate?.datePickerVCDismissed(nil)
    }
    
    private func updatePickerCurrentDate() {
        if let _currentDate = self.currentDate {
            if let _datePicker = self.datePicker {
                _datePicker.date = _currentDate
            }
        }
    }
    
    
    
}

extension PopDateViewController {
    
    @IBAction func okAction(_ sender: AnyObject) {
        self.dismiss(animated: true) {
            let date = self.datePicker.date
            self.delegate?.datePickerVCDismissed(date)
        }
    }
    
    
}

