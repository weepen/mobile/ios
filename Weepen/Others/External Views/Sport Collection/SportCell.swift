//
//  SportCell.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/02/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

class SportCell: UICollectionViewCell {

    @IBOutlet weak var sportIV: UIImageView!
    @IBOutlet weak var sportLbl: UILabel!
    
    var imageNoFilter = UIImage()
    var sportUUID: String!
    var isEnable: Bool = false
    
    override var isSelected: Bool{
        didSet{
            if self.isSelected {
                if let sport = CacheService.shared.getDatabase().objects(SportRealm.self).filter("sportUUID = '\(sportUUID ?? "")'").first {
                    sportLbl.textColor = UIColor.hexColor(UInt((sport.codeColor.value)!))
                    sportIV.image = UIImage(data: sport.logo!)
                }
            }
            else {
                sportLbl.textColor = ThemeManager.currentTheme().unselectedFontColor
                sportIV.addBlackFilter()
            }
        }
    }
    
    func removeBlackFilter(_ originalImage: UIImageView) {
        let noirFilter = Ambience.currentState == .invert ? CIFilter(name: "CIPhotoEffectTonal")! : CIFilter(name: "CIPhotoEffectNoir")!
        let inputImage = noirFilter.value(forKey: kCIInputImageKey)
        originalImage.image = UIImage(cgImage: inputImage as! CGImage)
    }

}
