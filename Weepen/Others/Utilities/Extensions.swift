//
//  Extensions.swift
//  Weepen
//
//  Created by Louis Cheminant on 23/07/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience
import RealmSwift

extension UIColor {
    public class func hexColor(_ rgbValue: UInt) -> UIColor {
        return UIColor.init(displayP3Red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgbValue & 0x0000FF) / 255.0, alpha: CGFloat(1.0))
        
    }
    
    
}

private var kAssociationKeyNextField: UInt8 = 0

extension UITextField {
    func shake() {
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.05
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: self.center.x - 3, y: self.center.y)
        animation.toValue = CGPoint(x: self.center.x + 3, y: self.center.y)
        self.layer.add(animation, forKey: "position")
    }
    
    @IBOutlet var nextField: UITextField? {
        get {
            return objc_getAssociatedObject(self, &kAssociationKeyNextField) as? UITextField
        }
        set(newField) {
            objc_setAssociatedObject(self, &kAssociationKeyNextField, newField, .OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    
}

extension UIViewController {
    func performSegueToReturnBack()  {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func showAlert(_ title: String, _ subtitle: String) {
        let alertController = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("CLOSE", comment: ""), style: .default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(_ title: String, _ subtitle: String, completion:@escaping () -> ()) {
        let alertController = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("CLOSE", comment: ""), style: .default, handler: nil))
        self.present(alertController, animated: true) {
            completion()
        }
    }
    
    
}

extension UIView {
    
    func circle() {
        self.layer.cornerRadius = self.frame.width/2
    }
    
    func loadXib(withName name: String, _ contentView: UIView) {
        Bundle.main.loadNibNamed(name, owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func roundCorners(_ corners: CACornerMask, radius: CGFloat) {
        if #available(iOS 11, *) {
            self.layer.cornerRadius = radius
            self.layer.maskedCorners = corners
        } else {
            var cornerMask = UIRectCorner()
            if(corners.contains(.layerMinXMinYCorner)){
                cornerMask.insert(.topLeft)
            }
            if(corners.contains(.layerMaxXMinYCorner)){
                cornerMask.insert(.topRight)
            }
            if(corners.contains(.layerMinXMaxYCorner)){
                cornerMask.insert(.bottomLeft)
            }
            if(corners.contains(.layerMaxXMaxYCorner)){
                cornerMask.insert(.bottomRight)
            }
            let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            self.layer.mask = mask
        }
    }
    
    
}

extension UIImage {
    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    convenience init(view: UIView) {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.init(cgImage: (image?.cgImage)!)
    }
}

extension UIImageView {
    public func image(fromUrl url: URL, completion:@escaping (Bool) -> ()) {
        let theTask = URLSession.shared.dataTask(with: url) {
            data, response, error in
            guard let response = data, error == nil else {
                print("***DONWLOAD IMAGE ERROR: [\(error!)]***")
                completion(false)
                return
            }
            DispatchQueue.main.async {
                self.image = UIImage(data: response)
                completion(true)
            }
            
        }
        theTask.resume()
    }
    
    public func image(fromUrl urlString: String, completion:@escaping (Bool) -> ()) {
        guard let url = URL(string: urlString) else {
            completion(false)
            return
        }
        let theTask = URLSession.shared.dataTask(with: url) {
            data, response, error in
            if let response = data {
                DispatchQueue.main.async {
                    self.image = UIImage(data: response)
                    completion(true)
                }
            }
        }
        theTask.resume()
    }
}

extension Date {
    func toString(dateFormat format : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
}

extension UIDatePicker {
    public var clampedDate: Date {
        let referenceTimeInterval = self.date.timeIntervalSinceReferenceDate
        let remainingSeconds = referenceTimeInterval.truncatingRemainder(dividingBy: TimeInterval(minuteInterval*60))
        let timeRoundedToInterval = referenceTimeInterval - remainingSeconds
        return Date(timeIntervalSinceReferenceDate: timeRoundedToInterval)
    }
}

extension UIButton {
    func applyGradient(_ colors: [UIColor], _ isShadow: Bool? = true) -> Void {
        removeGraident(self.layer)
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.cornerRadius =  self.frame.size.height / 2
        gradient.colors = colors.map { $0.cgColor }
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.name = "gradient"
        if isShadow! {
            gradient.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
            gradient.shadowRadius = ThemeManager.currentTheme().shadowRadius
            gradient.shadowOffset = ThemeManager.currentTheme().shadowOffset
            gradient.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
        }
        if layer.sublayers?[0].name != "gradient" {
            layer.insertSublayer(gradient, at: 0)
        }
    }
    
    func removeGraident(_ layer: CALayer) {
        if layer.sublayers != nil {
            if layer.sublayers!.count >= 1 && layer.sublayers![0].name == "gradient" {
                layer.sublayers!.remove(at: 0)
            }
        }
    }
    
    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
            self.transform = self.transform.scaledBy(x: 0.9, y: 0.9)
        }, completion: nil)
        super.touchesBegan(touches, with: event)
    }
    
    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.3, delay: 0, options: [], animations: {
            self.transform = .identity
        }, completion: nil)
        super.touchesEnded(touches, with: event)
    }
    
    func changeColor(_ color: UIColor) {
        self.imageView!.image = self.imageView!.image!.withRenderingMode(.alwaysTemplate)
        self.imageView!.tintColor = color
    }
}

private var dateCheck: String = ""

extension String {
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    func isName() -> Bool {
        let nameRegex = "[a-zA-Z]+(([\\'\\,\\.à\\á\\â\\ä\\ã\\å\\ą\\č\\ć\\ę\\è\\é\\ê\\ë\\ė\\į\\ì\\í\\î\\ï\\ł\\ń\\ò\\ó\\ô\\ö\\õ\\ø\\ù\\ú\\û\\ü\\ų\\ū\\ÿ\\ý\\ż\\ź\\ñ\\ç\\č\\š\\ž\\À\\Á\\Ä\\Ã\\Å\\Ą\\Ć\\Č\\Ė\\Ę\\È\\É\\Ê\\Ë\\Ì\\Í\\Î\\Ï\\Į\\Ł\\Ń\\Ò\\Ó\\Ô\\Ö\\Õ\\Ø\\Ù\\Ú\\Û\\Ü\\Ų\\Ū\\Ÿ\\Ý\\Ż\\Ź\\Ñ\\ß\\Ç\\Œ\\Æ\\Č\\Š\\Ž\\∂\\ð\\- ][a-zA-Z ])?[a-zA-Z]*)*"
        return NSPredicate(format: "SELF MATCHES %@", nameRegex).evaluate(with: self)
    }
    
    func isEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self)
    }
    
    func isPhone() -> Bool {
        let phoneRegex = "(\\+\\d{2}\\s*(\\(\\d{2}\\))|(\\(\\d{2}\\)))?\\s*\\d{4,5}\\-?\\d{5}"
        return NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: self)
    }
    
    func isPassword() -> Bool {
        let passwordRegex = "(?=.*[A-Za-z])(?=.*\\d)(?=.*[\\\\'~`•\\]\\[£¥!@€#$%^&*()\\-_+}={|\\;:\"<>,./\\?])[A-Za-z\\d\\\\'~`•\\]\\[£¥!@€#$%^&*()\\-_+}={|\\;:\"<>,./\\?]{8,128}"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: self)
    }
    func isMailFiedValid() -> Bool {
        if self != "" && self[0] == "0" {
            return self.isPhone()
        } else {
            return self.isEmail()
        }
    }
    func isOnlySpecial() -> Bool {
        if (self.trimmingCharacters(in: CharacterSet.whitespaces) == "") { return true }
        return false
    }
    
    func isDate(_ textField: UITextField, _ dateStr: String) -> Bool {
        dateCheck += String(self[0])
        if textField.tag-1 == 3 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "ddMM"
            let date = dateFormatter.date(from: dateStr)
            if date == nil {
                return false
            }
        }
        if textField.tag-1 == 7 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "ddMMyyyy"
            let date = dateFormatter.date(from: dateStr)
            if date == nil {
                dateCheck = String(dateCheck.dropLast())
                return false
            }
            if !dateStr.isMajor() {
                return false
            }
        }
        return true
    }
    
    func isDateUS(_ textField: UITextField, _ dateStr: String) -> Bool {
        dateCheck += String(self[0])
        if textField.tag-1 == 3 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMdd"
            let date = dateFormatter.date(from: dateStr)
            if date == nil {
                return false
            }
        }
        if textField.tag-1 == 7 {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MMddyyyy"
            let date = dateFormatter.date(from: dateStr)
            if date == nil {
                dateCheck = String(dateCheck.dropLast())
                return false
            }
            if !dateStr.isMajorUS() {
                return false
            }
        }
        return true
    }
    
    func isMajor() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMyyyy"
        let date = dateFormatter.date(from: self)
        return -(Calendar.current.dateComponents([.year], from: Date(), to: date!).year ?? 0) >= 16 ? true : false
    }
    
    func isMajorUS() -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMddyyyy"
        let date = dateFormatter.date(from: self)
        return -(Calendar.current.dateComponents([.year], from: Date(), to: date!).year ?? 0) >= 16 ? true : false
    }
    
    func replace(_ myString: String, _ index: Int, _ newChar: Character) -> String {
        var chars = Array(myString)
        chars[index] = newChar
        return String(chars)
    }
    
    func toDate(dateFormat format : String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if let date = dateFormatter.date(from: self) {
            return date
        } else {
            return Date()
            //fatalError("There was an error decoding the string")
        }
    }
    
    var dateFromISO8601: Date? {
        return Formatter.iso8601.date(from: self)   // "Mar 22, 2017, 10:22 AM"
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    func toDatePickerDate() -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = Locale.current.languageCode == "fr" ? "dd MMM yyyy  HH:mm" : "MMM dd, yyyy  h:mm a"
        return  formatter.date(from: self)
    }
    
}

extension UITextView {
    
    @IBInspectable var doneAccessory: Bool{
        get {
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone {
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Terminer", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction() {
        self.resignFirstResponder()
    }
}

extension UIImageView {
    func addBlackFilter() {
        let context = CIContext(options: nil)
        let currentFilter = Ambience.currentState == .invert ? CIFilter(name: "CIPhotoEffectTonal") : CIFilter(name: "CIPhotoEffectNoir")
        currentFilter!.setValue(CIImage(image: self.image!), forKey: kCIInputImageKey)
        let output = currentFilter!.outputImage
        let cgimg = context.createCGImage(output!,from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        self.image = processedImage
    }
}

extension Date {
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfYear], from: date, to: self).weekOfYear ?? 0
    }
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    func offset(from date: Date) -> String {
        if years(from: date)        == 1    { return "\(years(from: date)) \(NSLocalizedString("YEAR", comment: ""))"   }
        else if years(from: date)   > 0     { return "\(years(from: date)) \(NSLocalizedString("YEARS", comment: ""))"  }
        if months(from: date)       == 1    { return "\(months(from: date)) \(NSLocalizedString("MONTH", comment: ""))" }
        if months(from: date)       > 0     { return "\(months(from: date)) \(NSLocalizedString("MONTHS", comment: ""))"}
        if weeks(from: date)        == 1    { return "\(weeks(from: date)) \(NSLocalizedString("WEEK", comment: ""))"   }
        else if weeks(from: date)   > 0     { return "\(weeks(from: date)) \(NSLocalizedString("WEEKS", comment: ""))"  }
        if days(from: date)         == 0    { return "\(NSLocalizedString("TODAY", comment: ""))"                       }
        else if days(from: date)    == 1 { return "\(days(from: date)) \(NSLocalizedString("DAY", comment: ""))"        }
        else if days(from: date)    > 0 { return "\(days(from: date)) \(NSLocalizedString("DAYS", comment: ""))"        }
        if hours(from: date)        > 0 { return "\(hours(from: date))h"                 }
        if minutes(from: date)      > 0 { return "\(minutes(from: date))min"             }
        if seconds(from: date)      > 0 { return "\(seconds(from: date))s"               }
        return ""
    }
    
    func toDatePickerString() -> String? {
        let formatter = DateFormatter()
        formatter.dateFormat = Locale.current.languageCode == "fr" ? "dd MMM yyyy  HH:mm" : "MMM dd, yyyy  h:mm a"
        return formatter.string(from: self)
    }

}


extension Formatter {
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}

extension Realm {
    func write(transaction block: () -> Void, completion: () -> Void) throws {
        try write(block)
        completion()
    }
}

extension UITableView {
    func hideSearchBar() {
        if let bar = self.tableHeaderView as? UISearchBar {
            let height = bar.frame.height
            let offset = self.contentOffset.y
            if offset < height {
                self.contentOffset = CGPoint(x: 0, y: height)
            }
        }
    }
    
    func addMissRow(_ nbRow: Int) {
        var indexes: [Int] = []
        if nbRow-1 >= 0 && self.numberOfRows(inSection: 0) == 0 {
            for i in 0...nbRow-1 {
                indexes.append(i)
            }
            let fromRow = { (row: Int) in return IndexPath(row: row, section: 0) }
            insertRows(at: indexes.map(fromRow), with: .automatic)
        }
    }
    
    func applyChanges<T>(changes: RealmCollectionChange<T>, _ key: EventType) {
        switch changes {
        case .initial:
            reloadData()
        case .update(_, let deletions, let insertions, let updates):
            switch key {
            case .display:
                change(deletions, insertions, updates, "isDisplay = true")
            case .participate:
                change(deletions, insertions, updates, "isParticipate = true")
            case .create:
                change(deletions, insertions, updates, "isCreate = true")
            }
        default: break
        }
    }
    
    func applyPlaceChanges<T>(changes: RealmCollectionChange<T>) {
        switch changes {
        case .initial:
            reloadData()
        case .update(_, let deletions, let insertions, let updates):
            if CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid").count < self.numberOfRows(inSection: 0) {
                let fromRow = { (row: Int) in return IndexPath(row: row, section: 0) }
                
                if deletions.count <= self.numberOfRows(inSection: 0) {
                    deleteRows(at: deletions.map(fromRow), with: .fade)
               }
            }
            for update in updates {
                if CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid").count == self.numberOfRows(inSection: 0) && update < self.numberOfRows(inSection: 0) {
                    reloadRows(at: [IndexPath(row: update, section: 0)], with: .automatic)
                }
            }
            let fromRow = { (row: Int) in return IndexPath(row: row, section: 0) }
            if CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid").count > self.numberOfRows(inSection: 0) {
                insertRows(at: insertions.map(fromRow), with: .automatic)
            }
        default: break
        }
    }
    
    func change(_ deletions: [Int], _ insertions: [Int], _ updates: [Int], _ key: String) {
        for deletion in deletions {
            if CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter(key).count < self.numberOfRows(inSection: 0) {
                deleteRows(at: [IndexPath(row: deletion, section: 0)], with: .fade)
            }
        }
        for update in updates {
            if CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter(key).count == self.numberOfRows(inSection: 0) && update < self.numberOfRows(inSection: 0) {
                reloadRows(at: [IndexPath(row: update, section: 0)], with: .automatic)
            }
        }
        let fromRow = { (row: Int) in return IndexPath(row: row, section: 0) }
        if CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter(key).count > self.numberOfRows(inSection: 0) {
            insertRows(at: insertions.map(fromRow), with: .automatic)
        }
    }
}

extension NSLayoutConstraint {
    
    override open var description: String {
        let id = identifier ?? ""
        return "id: \(id), constant: \(constant)" //you may print whatever you want here
    }
}

