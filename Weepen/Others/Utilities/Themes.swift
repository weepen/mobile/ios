//
//  Themes.swift
//  Weepen
//
//  Created by Louis Cheminant on 23/07/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Ambience
import GrowingTextView
import SwiftRangeSlider


enum Themes: Int {
    case Light, Dark, Night
    
    var selectedFontColor: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //FFFFFF
        default:
            return #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1) //4A4A4A
        }
    }
    
    var reverseSelectedFontColor: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 0.2901960784, green: 0.2901960784, blue: 0.2901960784, alpha: 1) //4A4A4A
        default:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //FFFFFF
        }
    }
    
    var unselectedFontColor: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 0.4745098039, green: 0.5019607843, blue: 0.5764705882, alpha: 1) //798093
        default:
            return #colorLiteral(red: 0.6117647059, green: 0.631372549, blue: 0.6901960784, alpha: 1) //9CA1B0
        }
    }
    
    var unselectedButtonColor: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 0.1019607843, green: 0.1098039216, blue: 0.1607843137, alpha: 1) //1A1C29
        default:
            return #colorLiteral(red: 0.9333333333, green: 0.9450980392, blue: 0.968627451, alpha: 1) //EEF1F7
        }
    }
    
    var placehorderColor: UIColor {
        return #colorLiteral(red: 0.8078431373, green: 0.8078431373, blue: 0.8078431373, alpha: 0.7) //CECECE alpha 65%
    }
    
    var shadowColor: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) //clear
        default:
            return #colorLiteral(red: 0.8078431373, green: 0.8078431373, blue: 0.8078431373, alpha: 0.7) //CECECE alpha 65%
        }
    }
    
    var downView: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 0.05489219725, green: 0.05491021276, blue: 0.08464095742, alpha: 1) //0E0E15
        default:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //FFFFFF
        }
    }
    
    var upperView: UIColor {
        switch Ambience.currentState {
        case .invert :
            return #colorLiteral(red: 0.1019607843, green: 0.1098039216, blue: 0.1607843137, alpha: 1) //1A1C29
        default:
            return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //FFFFFF
        }
    }
    
    var barStyle: UIBarStyle {
        switch Ambience.currentState {
        case .invert :
            return .blackTranslucent
        default:
            return .default
        }
    }
    
    var shadowRadius: CGFloat {
        return 7
    }
    
    var shadowOpacity: Float {
        return 1
    }
    
    var shadowOffset: CGSize {
        return CGSize(width: 0, height: 0)
    }
    
    var cornerRadius: CGFloat {
        return 8
    }
    
    var validationColor: UIColor {
        return #colorLiteral(red: 0.2980392157, green: 0.8509803922, blue: 0.3921568627, alpha: 1) //4CD964
    }
    
    var errorColor: UIColor {
        return #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1) //FF3B30
    }
    
    var warningColor: UIColor {
        return #colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1) //FFCC00
    }
    
    var keyboardColor: UIKeyboardAppearance {
        switch Ambience.currentState {
        case .invert:
            return .dark
        default:
            return .default
        }
    }

    var fontNameRegular : String {
        return "CircularStd-Book"
    }
    
    var fontNameMedium : String {
        return "CircularStd-Medium"
    }
    
    var fontNameBold : String {
        return "CircularStd-Bold"
    }
    
    var fontNameBlack : String {
        return "CircularStd-Black"
    }
    
    
}

enum Color: Int {
    case Orange, Pink, Blue
    
    var mainColor: UIColor {
        switch self {
        case .Orange:
            return #colorLiteral(red: 1, green: 0.4784313725, blue: 0.2431372549, alpha: 1)
        case .Pink:
            return #colorLiteral(red: 0.9254901961, green: 0, blue: 0.5490196078, alpha: 1)
        case .Blue:
            return #colorLiteral(red: 0.337254902, green: 0.8, blue: 0.9490196078, alpha: 1)
        }
    }
    
    var gradientColor: [UIColor] {
        switch self {
        case .Orange:
            return [#colorLiteral(red: 1, green: 0.4795752168, blue: 0.2449897826, alpha: 1), #colorLiteral(red: 0.9490196078, green: 0.3647058824, blue: 0.4235294118, alpha: 1)]
        case .Pink:
            return [#colorLiteral(red: 0.9254901961, green: 0, blue: 0.5490196078, alpha: 1), #colorLiteral(red: 0.6588235294, green: 0.2823529412, blue: 0.8274509804, alpha: 1)]
        case .Blue:
            return [#colorLiteral(red: 0.337254902, green: 0.8, blue: 0.9490196078, alpha: 1), #colorLiteral(red: 0.1843137255, green: 0.5019607843, blue: 0.9294117647, alpha: 1)]
        }
    }
    
    
}

let SelectedThemeKey = "SelectedTheme"
let SelectedColorKey = "SelectedColor"

struct ThemeManager {
        
    static func currentTheme() -> Themes {
        if let storedTheme = (UserDefaults.standard.value(forKey: SelectedThemeKey) as AnyObject).integerValue {
            return Themes(rawValue: storedTheme)!
        } else {
            return .Light
        }
    }
    
    static func currentColor() -> Color {
        if let storedTheme = (UserDefaults.standard.value(forKey: SelectedColorKey) as AnyObject).integerValue {
            return Color(rawValue: storedTheme)!
        } else {
            return .Orange
        }
    }
    
    static func getAppearanceAlert(_ width: CGFloat, _ closeButton: Bool? = false) -> AlertView.Appearance {
        let appearance = AlertView.Appearance(
            kWindowWidth: width * 0.8,
            kTitleFont: UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 20)!,
            kTextFont: UIFont(name: ThemeManager.currentTheme().fontNameRegular, size: 14)!,
            kButtonFont: UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 14)!,
            showCloseButton: closeButton!,
            shouldAutoDismiss : false,
            contentViewCornerRadius: ThemeManager.currentTheme().cornerRadius,
            fieldCornerRadius : ThemeManager.currentTheme().cornerRadius,
            buttonCornerRadius: 20,
            hideWhenBackgroundViewIsTapped : false,
            contentViewColor: ThemeManager.currentTheme().downView,
            contentViewBorderColor: ThemeManager.currentTheme().downView,
            titleColor: ThemeManager.currentTheme().selectedFontColor,
            backgroundCircle: ThemeManager.currentTheme().downView,
            shadowColor: ThemeManager.currentTheme().shadowColor,
            shadowOffset: ThemeManager.currentTheme().shadowOffset,
            shadowRadius: ThemeManager.currentTheme().shadowRadius,
            shadowOpacity: ThemeManager.currentTheme().shadowOpacity
        )
        return appearance
    }
    
    static func applyTheme(theme: Themes, color: Color) {
        UserDefaults.standard.setValue(theme.rawValue, forKey: SelectedThemeKey)
        UserDefaults.standard.setValue(color.rawValue, forKey: SelectedColorKey)
        UserDefaults.standard.synchronize()
        
        UISwitch.appearance().tintColor = color.mainColor
        UISwitch.appearance().onTintColor = color.mainColor

        
        UITextField.appearance().keyboardAppearance = theme.keyboardColor
        
        let sharedApplication = UIApplication.shared
        sharedApplication.delegate?.window??.tintColor = color.mainColor
        
        SkyFloatingLabelTextField.appearance().font = UIFont(name: theme.fontNameBlack, size: 19)
        SkyFloatingLabelTextField.appearance().lineHeight = 1.5
        SkyFloatingLabelTextField.appearance().selectedLineHeight = 1.5

        
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
    }
    
    
}


extension SkyFloatingLabelTextField {
    override open func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.selectedTitleColor = ThemeManager.currentTheme().unselectedFontColor

        self.textColor = ThemeManager.currentTheme().selectedFontColor
        self.placeholderColor = ThemeManager.currentTheme().unselectedFontColor
        self.titleColor = ThemeManager.currentTheme().unselectedFontColor
        self.errorColor = ThemeManager.currentTheme().errorColor
        self.selectedTitleColor = ThemeManager.currentTheme().unselectedFontColor
        self.disabledColor = ThemeManager.currentTheme().selectedFontColor
        self.selectedLineColor = ThemeManager.currentTheme().selectedFontColor
        self.lineColor = ThemeManager.currentTheme().unselectedFontColor
        self.keyboardAppearance = ThemeManager.currentTheme().keyboardColor
    }
    
    
}

// MARK: - Storyboard Extension
class UpperView: UIView {
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        layoutSubviews()
    }
    
    @IBInspectable var isGradient: Bool = false
    @IBInspectable var isShadow: Bool = false
    @IBInspectable var isBorder: Bool = false
    @IBInspectable var isCircle: Bool = false
    @IBInspectable var isRounded: Bool = false
    @IBInspectable var isLazy: Bool = false
    @IBInspectable var isDownview: Bool = false
    @IBInspectable var isUpperview: Bool = false

    
    @IBInspectable var topColor: UIColor = ThemeManager.currentColor().gradientColor[0] {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var bottomColor: UIColor = ThemeManager.currentColor().gradientColor[1] {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var startPointX: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var startPointY: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var endPointX: CGFloat = 1 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var endPointY: CGFloat = 0.5 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = ThemeManager.currentTheme().shadowRadius {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowOpacity: Float = ThemeManager.currentTheme().shadowOpacity {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowOffset: CGSize = ThemeManager.currentTheme().shadowOffset {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowColor: UIColor = ThemeManager.currentTheme().shadowColor {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var borderColor:UIColor = ThemeManager.currentTheme().shadowColor {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = ThemeManager.currentTheme().cornerRadius {
        didSet {
            setNeedsLayout()
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func didAddSubview(_ subview: UIView) {
        super.didAddSubview(subview)
    }
    
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.reinstateAmbience()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isGradient {
            let gradientLayer = layer as! CAGradientLayer
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: startPointX, y: startPointY)
            gradientLayer.endPoint = CGPoint(x: endPointX, y: endPointY)
        } else {
            if isLazy {
                self.backgroundColor = ThemeManager.currentTheme().unselectedFontColor
            }
        }
        if isShadow {
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            layer.shadowRadius = shadowRadius
        }
        if isBorder {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = borderWidth
        }
        if isCircle {
            layer.cornerRadius = self.frame.height/2
        }
        if isRounded {
            layer.cornerRadius = 10
        }
        if isUpperview {
            backgroundColor = ThemeManager.currentTheme().upperView
        }
        if isDownview {
            backgroundColor = ThemeManager.currentTheme().downView
        }
    }
    
    
}

class UpperLabel: UILabel {
    @IBInspectable var isCircle: Bool = false
    @IBInspectable var isLazy: Bool = false
    @IBInspectable var isSelectedReverse: Bool = false
    @IBInspectable var isSelected: Bool = false
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isLazy {
            if isSelected {
                textColor = ThemeManager.currentTheme().selectedFontColor
            } else if isSelectedReverse {
                textColor = ThemeManager.currentTheme().reverseSelectedFontColor
            } else {
                textColor = ThemeManager.currentTheme().unselectedFontColor
            }
        }
        if isCircle {
            layer.cornerRadius = self.frame.height/2
        }
    }
}

class UpperTextField: UITextField {
    @IBInspectable var isLazy: Bool = false
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isLazy {
            textColor = ThemeManager.currentTheme().selectedFontColor
        }
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: ThemeManager.currentTheme().placehorderColor])
    }
}

class UpperRangeSlider: RangeSlider {
    @IBInspectable var isLazy: Bool = false
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isLazy {
            labelColor = ThemeManager.currentTheme().selectedFontColor
            trackTintColor = ThemeManager.currentTheme().unselectedFontColor
        }
    }
}

class UpperDatePicker: UIDatePicker {
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setValue(ThemeManager.currentTheme().selectedFontColor, forKeyPath: "textColor")
    }
}

class UpperTextView: GrowingTextView {
    @IBInspectable var isLazy: Bool = false
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isLazy {
            textColor = ThemeManager.currentTheme().selectedFontColor
        }
        placeholderColor = ThemeManager.currentTheme().placehorderColor
    }
}

class UpperImageView: UIImageView {
    
    @IBInspectable var isShadow: Bool = false
    @IBInspectable var isCircle: Bool = false
    @IBInspectable var isRounded: Bool = false
    @IBInspectable var isSelected: Bool = false
    @IBInspectable var isLazy: Bool = false
    @IBInspectable var isNoImage: Bool = false

    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    @IBInspectable var shadowRadius: CGFloat = ThemeManager.currentTheme().shadowRadius {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowOpacity: Float = ThemeManager.currentTheme().shadowOpacity {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowOffset: CGSize = ThemeManager.currentTheme().shadowOffset {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowColor: UIColor = ThemeManager.currentTheme().shadowColor {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = ThemeManager.currentTheme().cornerRadius {
        didSet {
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isShadow {
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            layer.shadowRadius = shadowRadius
        }
        if isCircle {
            layer.cornerRadius = self.frame.height/2
        } else if isRounded {
            layer.cornerRadius = cornerRadius
        } else {
            layer.cornerRadius = 0
        }
        if isSelected {
            tintColor = ThemeManager.currentTheme().selectedFontColor
        }
        if isLazy {
            tintColor = ThemeManager.currentTheme().unselectedFontColor
        }
        if isNoImage {
            backgroundColor = ThemeManager.currentTheme().unselectedFontColor
        }
    }
}

class UpperProgress: UIProgressView {
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.layoutSubviews()
    }
    
    @IBInspectable var isGradient: Bool = false
    @IBInspectable var isRounded: Bool = false
    
    @IBInspectable var topColor: UIColor = ThemeManager.currentColor().gradientColor[0] {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var bottomColor: UIColor = ThemeManager.currentColor().gradientColor[1] {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var startPointX: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var startPointY: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var endPointX: CGFloat = 1 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var endPointY: CGFloat = 0.5 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = ThemeManager.currentTheme().cornerRadius {
        didSet {
            setNeedsLayout()
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isGradient {
            let gradientLayer = layer as! CAGradientLayer
            gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
            gradientLayer.startPoint = CGPoint(x: startPointX, y: startPointY)
            gradientLayer.endPoint = CGPoint(x: endPointX, y: endPointY)
            gradientLayer.cornerRadius = self.frame.height/2
        }
        if isRounded {
            layer.cornerRadius = self.frame.height/2
        }
    }
    
    
}

class UpperButton: UIButton {
    
    @IBInspectable var isCircle: Bool = false
    @IBInspectable var isRounded: Bool = false
    @IBInspectable var isShadow: Bool = false
    @IBInspectable var isSign: Bool = false
    @IBInspectable var isGradient: Bool = false
    @IBInspectable var isRemoveGradient: Bool = false
    @IBInspectable var isBorder: Bool = false
    @IBInspectable var isLazy: Bool = false
    @IBInspectable var isSelect: Bool = false
    @IBInspectable var isIntro: Bool = false
    @IBInspectable var isButtonItem: Bool = false
    @IBInspectable var isContrast: Bool = false
    @IBInspectable var isProfileSelected: Bool = false
    @IBInspectable var isProfileLazy: Bool = false
    @IBInspectable var isSocial: Bool = false
    @IBInspectable var isNormal: Bool = false
    @IBInspectable var isNormalReverse: Bool = false
    @IBInspectable var isFontSelect: Bool = false

    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        layoutSubviews()
    }
    
    @IBInspectable var topColor: UIColor = ThemeManager.currentColor().gradientColor[0] {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var bottomColor: UIColor = ThemeManager.currentColor().gradientColor[1] {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var startPointX: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var startPointY: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var endPointX: CGFloat = 1 {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var endPointY: CGFloat = 0.5 {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = ThemeManager.currentTheme().shadowRadius {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowOpacity: Float = ThemeManager.currentTheme().shadowOpacity {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowOffset: CGSize = ThemeManager.currentTheme().shadowOffset {
        didSet {
            setNeedsLayout()
        }
    }
    @IBInspectable var shadowColor: UIColor = ThemeManager.currentTheme().shadowColor {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = ThemeManager.currentTheme().cornerRadius {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor:UIColor = ThemeManager.currentColor().mainColor {
        didSet {
            setNeedsLayout()
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        layoutSubviews()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if isSign {
            titleLabel?.font =  UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 25)
            if !self.isEnabled {
                backgroundColor = ThemeManager.currentTheme().unselectedButtonColor
                setTitleColor(ThemeManager.currentTheme().unselectedFontColor, for: .disabled)
                if layer.sublayers != nil {
                    if layer.sublayers!.count >= 1 && layer.sublayers![0].name == "gradient" {
                        layer.sublayers!.remove(at: 0)
                    }
                }
            } else {
                setTitleColor(.white, for: .normal)
                self.applyGradient(ThemeManager.currentColor().gradientColor)
            }
        }
        if isGradient && !isRemoveGradient {
            titleLabel?.font =  UIFont(name: ThemeManager.currentTheme().fontNameBold, size: self.titleLabel?.font.pointSize ?? 12)
            setTitleColor(.white, for: .normal)
            self.applyGradient([topColor, bottomColor])
        }
        if isBorder {
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = borderWidth
        }
        if isShadow {
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            layer.shadowRadius = shadowRadius
        }
        if isCircle {
            layer.cornerRadius = self.frame.height/2
        }
        if isRounded {
            layer.cornerRadius = cornerRadius
        }
        if isLazy {
            backgroundColor = ThemeManager.currentTheme().unselectedFontColor
        }
        if isSelect {
            backgroundColor = ThemeManager.currentTheme().selectedFontColor
        }
        if isFontSelect {
            setTitleColor(ThemeManager.currentTheme().reverseSelectedFontColor, for: .normal)
        }
        if isIntro {
            tintColor = ThemeManager.currentTheme().unselectedFontColor
            layer.borderColor = ThemeManager.currentTheme().unselectedFontColor.cgColor
            layer.shadowColor = ThemeManager.currentTheme().unselectedFontColor.cgColor
        }
        if isButtonItem {
            tintColor = ThemeManager.currentTheme().selectedFontColor
        }
        if isContrast {
            tintColor = ThemeManager.currentTheme().reverseSelectedFontColor
        }
        if isProfileSelected {
            setTitleColor(ThemeManager.currentTheme().selectedFontColor, for: .disabled)
            setTitleColor(ThemeManager.currentColor().mainColor, for: .normal)
        }
        if isProfileLazy {
            setTitleColor(ThemeManager.currentTheme().unselectedFontColor, for: .disabled)
            setTitleColor(ThemeManager.currentColor().mainColor, for: .normal)
        }
        if isSocial {
            applyGradient([topColor, bottomColor], false)
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = shadowColor.cgColor
            layer.shadowRadius = shadowRadius
        }
        if isNormal {
            backgroundColor = ThemeManager.currentTheme().upperView
            setTitleColor(ThemeManager.currentTheme().selectedFontColor, for: .normal)
            titleLabel?.font = UIFont(name: ThemeManager.currentTheme().fontNameMedium, size: 18)
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            layer.shadowRadius = shadowRadius
        }
        if isNormalReverse {
            backgroundColor = ThemeManager.currentTheme().downView
            setTitleColor(ThemeManager.currentTheme().selectedFontColor, for: .normal)
            titleLabel?.font = UIFont(name: ThemeManager.currentTheme().fontNameMedium, size: 18)
            layer.shadowOpacity = shadowOpacity
            layer.shadowOffset = shadowOffset
            layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            layer.shadowRadius = shadowRadius
        }
    }
}

