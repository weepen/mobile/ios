//
//  SUAuthorizationViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseAuth

class AuthorizationViewController: UIViewController {
    
    @IBOutlet weak var btnLocation: UpperButton!
    @IBOutlet weak var btnNotification: UpperButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    @IBOutlet weak var validateBtn: UpperButton!
    
    var isAskNotif = UserDefaults.standard.bool(forKey: "isAskNotif")
    var alert: AlertViewResponder!
    let LocationMgr = LocationService.shared
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateButton(false)
        didNotificationAuthorization()
        LocationMgr.delegate = self
        if LocationMgr.isAuthorized() == 1 {
            LocationMgr.startUpdatingLocation()
            btnGood(btnLocation)
            lblLocation.text = NSLocalizedString("PERFECT", comment: "")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(didNotificationAuthorization), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !lblLocation.text!.contains(NSLocalizedString("PERFECT", comment: "")) {
            btnColor(btnLocation, true)
        }
        if !lblNotification.text!.contains(NSLocalizedString("PERFECT", comment: "")) {
            if lblNotification.text!.contains(NSLocalizedString("NO_NOTIFICATIONS", comment: "")) {
                btnBad(btnNotification)
            } else {
                btnColor(btnNotification, true)
            }
        }
    }
    
    override func viewDidLoad() {
        try! CacheService.shared.getDatabase().write {
            user.latitude.value = 0.0
            user.longitude.value = 0.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didNotificationAuthorization() {
        if isAskNotif {
            didChangeAuthorization()
        }
    }
    
    @IBAction func locationTapped(_ sender: UIButton) {
        LocationMgr.askAuthorizationLocation()
    }
    
    @IBAction func notificationTapped(_ sender: UIButton) {
        didChangeAuthorization()
    }
    
    func btnColor(_ button: UIButton, _ isEnabled: Bool) {
        button.applyGradient(ThemeManager.currentColor().gradientColor)
        button.setTitleColor(.white, for: .disabled)
        button.isEnabled = isEnabled
    }
    
    func btnGood(_ button: UIButton) {
        button.removeGraident(button.layer)
        button.backgroundColor = ThemeManager.currentTheme().validationColor
        button.setTitleColor(.white, for: .disabled)
        button.isEnabled = false
    }
    
    func btnBad(_ button: UIButton) {
        button.removeGraident(button.layer)
        button.backgroundColor = ThemeManager.currentTheme().errorColor
        button.setTitleColor(.white, for: .disabled)
        button.isEnabled = false
    }
    
    @IBAction func validateTappede(_ sender: Any) {
        if validateBtn.isEnabled {
            self.push(sbName: "Main", sbID: "MainTabController")

        }
    }
    
    func updateButton(_ isEnabled: Bool) {
        UIApplication.shared.registerForRemoteNotifications()
        validateBtn.isEnabled = isEnabled
    }
    
    func push(sbName: String, sbID: String) {
        UserDefaults.standard.set(true, forKey: "isFirstTime")
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: sbName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: sbID)
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
    
}


extension AuthorizationViewController: UNUserNotificationCenterDelegate {
    func didChangeAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            if !granted {
                DispatchQueue.main.async {
                    self.btnBad(self.btnNotification)
                    self.lblNotification.text = NSLocalizedString("NO_NOTIFICATIONS", comment: "")
                }
            } else {
                DispatchQueue.main.async {
                    self.btnGood(self.btnNotification)
                    self.lblNotification.text = NSLocalizedString("PERFECT", comment: "")
                }
            }
            DispatchQueue.main.async {
                if self.btnNotification.isEnabled == false && self.btnLocation.isEnabled == false  {
                    self.updateButton(true)
                }
            }
            self.isAskNotif = true
            UserDefaults.standard.set(self.isAskNotif, forKey: "isAskNotif")
        }
    }
    
    
}

// MARK: - LocationUpdateProtocol
extension AuthorizationViewController: LocationUpdateProtocol {
    func locationDidChangeAuthorization(status: Int) {
        switch status {
        case 0:
            self.performSegue(withIdentifier: "noLocation", sender: nil)
            btnBad(btnLocation)
            lblLocation.text = "Si jamais tu changes d'avis: Application \"Réglages\" -> \"Weepen\" -> \"Position\""
            updateButton(false)
        default:
            btnGood(btnLocation)
            lblLocation.text = NSLocalizedString("PERFECT", comment: "")
            LocationMgr.startUpdatingLocation()
        }
        if btnNotification.isEnabled == false && btnLocation.isEnabled == false  {
            updateButton(true)
        }
        
    }
    
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {
        try! CacheService.shared.getDatabase().write {
            user.latitude.value = latitude
            user.longitude.value = longitude
        }
        LocationMgr.stopUpdatingLocation()
    }
}
