//
//  DescriptionPageViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 19/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import CHIPageControl
import GoogleSignIn
import FBSDKLoginKit
import Firebase
import FirebaseUI
import Ambience
import NVActivityIndicatorView
import CoreLocation
import UserNotifications


class SignInViewController: UIViewController {
    
    @IBOutlet weak var descpLbl: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: CHIPageControlAleppo!
    
    var AuthMgr = AuthService.sharedAuth
    let images = [#imageLiteral(resourceName: "Introduction - Page 3"), #imageLiteral(resourceName: "Introduction - Page 2"), #imageLiteral(resourceName: "Introduction - Page 3")]
    let titlesText = [
        NSLocalizedString("INTRODUCTION_MESSAGE_1", comment: ""),
        NSLocalizedString("INTRODUCTION_MESSAGE_2", comment: ""),
        NSLocalizedString("INTRODUCTION_MESSAGE_3", comment: "")
    ]
    var seconds = 10
    var pictureSeconds = 10
    var timer = Timer()
    var isTimerRunning = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupPageController()
        setupFirebaseUI()
        addSwipeGestureRecognizer()
        initGoogle()
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    func setupPageController() {
        pageControl.tintColors = [UIColor.hexColor(0xF0F0F7), UIColor.hexColor(0xF0F0F7), UIColor.hexColor(0xF0F0F7)]
        pageControl.set(progress: 0, animated: true)
        scrollView.contentSize = CGSize(width: 3*(UIScreen.main.bounds.width-30), height: scrollView.bounds.height)
    }
    
    func setupFirebaseUI() {
        FUIAuth.defaultAuthUI()?.delegate = self
        let phoneProvider = FUIPhoneAuth.init(authUI: FUIAuth.defaultAuthUI()!)
        FUIAuth.defaultAuthUI()?.providers = [phoneProvider]
    }
    
    func runTimer(completion: @escaping (Bool)->()) {
        
    }
}




// MARK: Phone sign up
extension SignInViewController: FUIAuthDelegate {
    @IBAction func didPhoneTapped(_ sender: UIButton) {
        let phoneProvider = FUIAuth.defaultAuthUI()?.providers.first as! FUIPhoneAuth
        phoneProvider.signIn(withPresenting: self, phoneNumber: nil)
    }
    
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        guard let uid = authDataResult?.user.uid, error == nil else {
            LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
            print("***USER REALM ERROR: [\(error!)]***")
            return
        }
        self.view.addSubview(LoadActivityIndicatorService.sharedLAI.addActivityIndicator(self.view.frame, isProgess: true))
        LoadActivityIndicatorService.sharedLAI.updateProgressView(0.1)
        self.startConnection(uid)
    }
    
    
}

//// MARK: Facebook sign up
extension SignInViewController {
    @IBAction func didFacebookTapped(_ sender: UIButton) {
        let loginManager = FBSDKLoginManager()
        loginManager.logIn(withReadPermissions: ["email", "public_profile"], from: self) { (loginResult, error) in
            guard let result = loginResult, error == nil, !result.isCancelled else {
                LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
                guard let error = error else { return }
                print("***USER REALM ERROR: [\(error)]***")
                return
            }
            self.view.addSubview(LoadActivityIndicatorService.sharedLAI.addActivityIndicator(self.view.frame, isProgess: true))
            LoadActivityIndicatorService.sharedLAI.updateProgressView(0.1)
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            LoadActivityIndicatorService.sharedLAI.updateProgressView(0.2)
            self.AuthMgr.signInSocial(credential, completion: { (result, uid) in
                if result {
                    LoadActivityIndicatorService.sharedLAI.updateProgressView(0.3)
                    self.startConnection(uid)
                }
            })
        }
    }
    
}


// MARK: Google sign up
extension SignInViewController: GIDSignInUIDelegate, GIDSignInDelegate {
    func initGoogle() {
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    @IBAction func didGoogleTapped(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        self.view.addSubview(LoadActivityIndicatorService.sharedLAI.addActivityIndicator(self.view.frame, isProgess: true))
        LoadActivityIndicatorService.sharedLAI.updateProgressView(0.1)
        guard let authentication = user.authentication, error == nil else {
            LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
            print("***SIGNIN SOCIAL ERROR: [\(error!)]***")
            return
        }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,accessToken: authentication.accessToken)
        LoadActivityIndicatorService.sharedLAI.updateProgressView(0.2)
        self.AuthMgr.signInSocial(credential, completion: { (result, uid) in
            if result {
                LoadActivityIndicatorService.sharedLAI.updateProgressView(0.3)
                self.startConnection(uid)
            }
        })
    }
    
    func showAlertConnection(_ title: String, _ subtitle: String) {
        let alert: AlertView = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
        alert.showError(title, subTitle: subtitle)
        alert.addButton("Fermer") {
            alert.appearance.shouldAutoDismiss = true
            alert.dismiss(animated: true, completion: nil)
        }
    }
    
}

// MARK: Check up before sign up process
extension SignInViewController {
    
    func startConnection(_ uid: String) {
        self.seconds = 0
        self.runTimerConnection(uid, completion: { (isTime, isError, isAlreadyAccount) in
            guard isTime == true else { self.showAlert("Une erreur s'est produite", "Il semble que ta connexion soit lente"); LoadActivityIndicatorService.sharedLAI.removeActivityIndicator(); return }
            guard isError == false else { self.showAlert("Une erreur s'est produite", "Relancer votre processus d'inscription"); LoadActivityIndicatorService.sharedLAI.removeActivityIndicator(); return }
            LoadActivityIndicatorService.sharedLAI.updateProgressView(0.4)
            if CacheService.shared.getDataFromDB(object: SportRealm()).count == 0 {
                SportRealm().getSport { (result) in
                    LoadActivityIndicatorService.sharedLAI.updateProgressView(0.5)
                    if result {
                        if isAlreadyAccount {
                            self.connectUser(uid)
                        } else {
                            self.createNewUser()
                        }
                    }
                }
            } else {
                if isAlreadyAccount {
                    self.connectUser(uid)
                } else {
                    self.createNewUser()
                }
            }
        })
    }
    
    func runTimerConnection(_ uid: String, completion: @escaping (Bool, Bool, Bool)->()) {
        var isDone = false
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                self.seconds -= 1
                if self.seconds == 0 {
                    timer.invalidate()
                    if !isDone {
                        isDone = true
                        completion(false, false, false)
                    }
                } else {
                    self.checkConnection(uid, completion: { (isError, isAlreadyAccount) in
                        timer.invalidate()
                        if !isDone {
                            isDone = true
                            if isError {
                                completion(true, true, isAlreadyAccount)
                            } else {
                                completion(true, false, isAlreadyAccount)
                            }
                        }
                    })
                }
            }
        }
    }
    
    func runTimerPicture(_ user: UserRealm, completion: @escaping (Data?)->()) {
        var isDone = false
        DispatchQueue.main.async {
            Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { timer in
                self.pictureSeconds -= 1
                if self.pictureSeconds == 0 {
                    timer.invalidate()
                    if !isDone {
                        isDone = true
                        completion(nil)
                    }
                } else {
                    UserRealm().getPictureUser(user, completion: { (data) in
                        timer.invalidate()
                        if !isDone {
                            isDone = true
                            completion(data)
                        }
                    })
                }
            }
        }
    }
    
    func checkConnection(_ uid: String, completion: @escaping (Bool, Bool)->()) {
        FirebaseService.shared.dbUser.document(uid).collection("visibility").document("PUBLIC").getDocument(completion: { (queryDocument, error) in
            guard error == nil, let document = queryDocument else {
                LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
                print("***CHECK CONNECTION ERROR: [\(error!)]***")
                completion(true, false); return
            }
            if document.data() != nil {
                completion(false, true)
            } else {
                completion(false, false)
            }
        })
    }
    
    func connectUser(_ uid: String) {
        UserRealm().getUserPublicInfo(uid, completion: { (name, latitude, longitude) in
            LoadActivityIndicatorService.sharedLAI.updateProgressView(0.6)
            UserRealm().getUserPrivateInfo(uid, completion: { (birthday) in
                LoadActivityIndicatorService.sharedLAI.updateProgressView(0.7)
                UserRealm().getSportInfo(uid, completion: { (sports) in
                    LoadActivityIndicatorService.sharedLAI.updateProgressView(0.8)
                    if birthday == nil { self.showAlert("Une erreur s'est produite", "Relancer votre processus d'inscription"); LoadActivityIndicatorService.sharedLAI.removeActivityIndicator(); return }
                    if let user = UserRealm(uid: uid, name: name, email: Auth.auth().currentUser?.email, phone: Auth.auth().currentUser?.phoneNumber, birthday: birthday!, latitude: latitude, longitude: longitude, sports: sports) {
                        LoadActivityIndicatorService.sharedLAI.updateProgressView(0.9)
                        self.runTimerPicture(user, completion: { data in
                            if data != nil { user.picture = data }
                            CacheService.shared.addData(object: user)
                            LoadActivityIndicatorService.sharedLAI.updateProgressView(1)
                            self.isRestrictionEnabled(completion: { (result) in
                                LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
                                if result {
                                    self.push(sbName: "Main", sbID: "MainTabController")
                                } else {
                                    self.performSegue(withIdentifier: "pushToRestriction", sender: nil)
                                }
                            })
                        })
                    }
                })
            })
        })
    }
    
    func isRestrictionEnabled(completion:@escaping (Bool) -> ()) {
        let current = UNUserNotificationCenter.current()
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
                current.getNotificationSettings(completionHandler: { (settings) in
                    switch settings.authorizationStatus {
                    case .notDetermined:
                        completion(false)
                    case .authorized:
                        completion(true)
                    case .provisional:
                        completion(false)
                    case .denied:
                        completion(false)
                    }
                })
            } else {
                completion(false)
            }
        } else {
            completion(false)
        }
    }
    
    func createNewUser() {
        guard let authUser = Auth.auth().currentUser else {
            LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
            print("***USER SIGNIN ERROR: [ERROR]***")
            return
        }
        self.fillInfo(authUser.photoURL?.absoluteString, authUser.displayName, authUser.email, authUser.phoneNumber, completion: { (result, user) in
            if result {
                LoadActivityIndicatorService.sharedLAI.updateProgressView(0.9)
                CacheService.shared.addData(object: user)
                LoadActivityIndicatorService.sharedLAI.updateProgressView(1)
                LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
                self.push(sbName: "SignUp", sbID: "SUMasterViewController")
            }
        })
    }
    
    func fillInfo(_ urlPhoto: String?, _ name: String?, _ email: String?, _ phoneNumber: String?, completion:@escaping (Bool, UserRealm) -> ()) {
        let user = UserRealm()
        LoadActivityIndicatorService.sharedLAI.updateProgressView(0.6)
        if let url = urlPhoto {
            let imageV = UIImageView()
            imageV.image(fromUrl: url, completion: { result -> Void in
                if result {
                    user.picture = imageV.image!.jpegData(compressionQuality: 1)
                } else {
                    user.picture = #imageLiteral(resourceName: "no-photo").jpegData(compressionQuality: 1)
                }
                if let name = name { user.name = name.components(separatedBy: " ")[0] }
                if let email = email { user.email = email }
                LoadActivityIndicatorService.sharedLAI.updateProgressView(0.7)
                completion(true, user)
            })
        }
        if let phone = phoneNumber {
            user.phone = phone
            LoadActivityIndicatorService.sharedLAI.updateProgressView(0.8)
            completion(true, user)
        }
    }
    
    func push(sbName: String, sbID: String) {
        DispatchQueue.main.async {
            let storyboard = UIStoryboard(name: sbName, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: sbID)
            controller.modalTransitionStyle = .crossDissolve
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    
}

// MARK: UIScroll functions
extension SignInViewController {
    @IBAction func pageControlDidPage(sender: AnyObject) {
        moveScrollContent(pageControl.currentPage)
    }
    
    func moveScrollContent(_ page: Int) {
        if page >= 0 && page < self.images.count {
            let xOffset = scrollView.bounds.width * CGFloat(page)
            scrollView.setContentOffset(CGPoint(x: xOffset, y: 0), animated: true)
            animateImage(page)
            animateText(page)
        }
    }
    
    
}

// MARK: UISwipeGestureRecognizer functions
extension SignInViewController {
    func addSwipeGestureRecognizer() {
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    @objc func handleGesture(gesture: AnyObject) {
        if pageControl.currentPage >= 0 && pageControl.currentPage < self.images.count {
            if gesture.direction == UISwipeGestureRecognizer.Direction.right {
                pageControl.set(progress: pageControl.currentPage - 1, animated: true)
                moveScrollContent(pageControl.currentPage - 1)
            } else if gesture.direction == UISwipeGestureRecognizer.Direction.left {
                pageControl.set(progress: pageControl.currentPage + 1, animated: true)
                moveScrollContent(pageControl.currentPage + 1)
            }
        }
    }
    
    
}

// MARK: Annimation functions
extension SignInViewController {
    func animateImage(_ page: Int) {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.imageView.alpha = 0.0
        }, completion: { (finished: Bool) -> Void in
            self.imageView.image = self.images[page]
            UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.imageView.alpha = 1.0
            }, completion: nil)
        })
    }
    
    func animateText(_ page: Int) {
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.descpLbl.alpha = 0.0
        }, completion: { (finished: Bool) -> Void in
            self.descpLbl.text = self.titlesText[page]
            UIView.animate(withDuration: 0.1, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
                self.descpLbl.alpha = 1.0
            }, completion: nil)
        })
    }
    
    
}
