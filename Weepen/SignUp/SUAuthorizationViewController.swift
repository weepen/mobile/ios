//
//  SUAuthorizationViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseAuth

class SUAuthorizationViewController: ChildView {
    
    @IBOutlet weak var btnLocation: UpperButton!
    @IBOutlet weak var btnNotification: UpperButton!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblNotification: UILabel!
    
    var isAskNotif = UserDefaults.standard.bool(forKey: "isAskNotif")
    var alert: AlertViewResponder!
    let LocationMgr = LocationService.shared
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didupdateButton(false)
        didNotificationAuthorization()
        LocationMgr.delegate = self
        if LocationMgr.isAuthorized() == 1 {
            LocationMgr.startUpdatingLocation()
            btnGood(btnLocation)
            lblLocation.text = NSLocalizedString("PERFECT", comment: "")
        }
        NotificationCenter.default.addObserver(self, selector: #selector(didNotificationAuthorization), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if !lblLocation.text!.contains(NSLocalizedString("PERFECT", comment: "")) {
            btnColor(btnLocation, true)
        }
        if !lblNotification.text!.contains(NSLocalizedString("PERFECT", comment: "")) {
            if lblNotification.text!.contains(NSLocalizedString("NO_NOTIFICATIONS", comment: "")) {
                btnBad(btnNotification)
            } else {
                btnColor(btnNotification, true)
            }
        }
    }
    
    override func viewDidLoad() {
        try! CacheService.shared.getDatabase().write {
            user.latitude.value = 0.0
            user.longitude.value = 0.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didNotificationAuthorization() {
        if isAskNotif {
            didChangeAuthorization()
        }
    }
    
    @IBAction func locationTapped(_ sender: UIButton) {
        LocationMgr.askAuthorizationLocation()
    }
    
    @IBAction func notificationTapped(_ sender: UIButton) {
        didChangeAuthorization()
    }
    
    func btnColor(_ button: UIButton, _ isEnabled: Bool) {
        button.applyGradient(ThemeManager.currentColor().gradientColor)
        button.setTitleColor(.white, for: .disabled)
        button.isEnabled = isEnabled
    }
    
    func btnGood(_ button: UIButton) {
        button.removeGraident(button.layer)
        button.backgroundColor = ThemeManager.currentTheme().validationColor
        button.setTitleColor(.white, for: .disabled)
        button.isEnabled = false
    }
    
    func btnBad(_ button: UIButton) {
        button.removeGraident(button.layer)
        button.backgroundColor = ThemeManager.currentTheme().errorColor
        button.setTitleColor(.white, for: .disabled)
        button.isEnabled = false
    }
    
    
}


extension SUAuthorizationViewController: UNUserNotificationCenterDelegate {
    func didChangeAuthorization() {
        UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
            if !granted {
                DispatchQueue.main.async {
                    self.btnBad(self.btnNotification)
                    self.lblNotification.text = NSLocalizedString("NO_NOTIFICATIONS", comment: "")
                }
            } else {
                DispatchQueue.main.async {
                    self.btnGood(self.btnNotification)
                    self.lblNotification.text = NSLocalizedString("PERFECT", comment: "")
                }
            }
            DispatchQueue.main.async {
                if self.btnNotification.isEnabled == false && self.btnLocation.isEnabled == false  {
                    self.didupdateButton(true)
                }
            }
            self.isAskNotif = true
            UserDefaults.standard.set(self.isAskNotif, forKey: "isAskNotif")
        }
    }
    
    
}

// MARK: - LocationUpdateProtocol
extension SUAuthorizationViewController: LocationUpdateProtocol {
    func locationDidChangeAuthorization(status: Int) {
        switch status {
        case 0:
            self.performSegue(withIdentifier: "noLocation", sender: nil)
            btnBad(btnLocation)
            lblLocation.text = "Si jamais tu changes d'avis: Application \"Réglages\" -> \"Weepen\" -> \"Position\""
            didupdateButton(false)
        default:
            btnGood(btnLocation)
            lblLocation.text = NSLocalizedString("PERFECT", comment: "")
            LocationMgr.startUpdatingLocation()
        }
        if btnNotification.isEnabled == false && btnLocation.isEnabled == false  {
            didupdateButton(true)
        }
        
    }
    
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {
        try! CacheService.shared.getDatabase().write {
            user.latitude.value = latitude
            user.longitude.value = longitude
        }
        LocationMgr.stopUpdatingLocation()
    }
}

extension SUAuthorizationViewController: MasterDelegate {
    func nextBtnTpd() {
        UIApplication.shared.registerForRemoteNotifications()
        alert = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width, false)).showWait(NSLocalizedString("CHECKING", comment: ""), subTitle: NSLocalizedString("SAVE_DATA", comment: ""), closeButtonTitle: nil, duration: 0.0, colorStyle: nil, colorTextButton: 0xFFFFFF, circleIconImage: nil, animationStyle: AnimationStyle.topToBottom)
        var sports = [Any]()
        for case let sport in user.sports {
            sports.append(FirebaseService.shared.dbSport.document(sport.sportUUID!))
        }
        if let currentUser = Auth.auth().currentUser {
            UserRealm().setUserPublicInfo(currentUser.uid, user.name!, user.latitude.value!, user.longitude.value!, completion: { (result) in
                if result {
                    UserRealm().setUserPrivateInfo(currentUser.uid, self.user.birthday!, completion: { (result) in
                        if result {
                            UserRealm().setSportInfo(currentUser.uid, sports: sports, completion: { (result) in
                                if result {
                                    UserRealm().setPictureUser(currentUser.uid, self.user)
                                    try! CacheService.shared.getDatabase().write { self.user.uuid = currentUser.uid }
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let controller = storyboard.instantiateViewController(withIdentifier: "MainTabController")
                                    self.present(controller, animated: true, completion: nil)
                                }
                            })
                        }
                    })
                }
            })
        } else {
            print("***ERROR***")
        }
    }
    
    
}
