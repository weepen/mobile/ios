//
//  MasterSignViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 04/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

@objc protocol MasterDelegate {
    func nextBtnTpd()
    @objc optional func optionalBtnTpd()
}

@objc protocol ChildDelegate {
    func updateButton(_ isEnabled: Bool)
    func tfShoundReturn()
    func isButtonEnable() -> Bool
}

class ChildView: UIViewController {
    var delegate: ChildDelegate?
    
    func didupdateButton(_ isEnabled: Bool) {
        delegate?.updateButton(isEnabled)
    }
    
    
    func didtfShoundReturn() {
        delegate?.tfShoundReturn()
    }
    
    func didIsEnable() -> Bool {
        return (delegate?.isButtonEnable())!
    }
    
}

class SUViewSettings {
    var type: ChildView!
    var title: String!
    var optional: Bool!
    
    init(_ type: ChildView, _ title: String, _ optional: Bool) {
        self.type = type
        self.title = title
        self.optional = optional
    }
}

class SUMasterViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UpperLabel!
    @IBOutlet weak var vwContainer: UIView!
    @IBOutlet weak var btnPrevious: UIButton!
    @IBOutlet weak var btnNext: UpperButton!
    @IBOutlet weak var pgSign: UpperProgress!
    @IBOutlet weak var btnOptional: UpperButton!
    @IBOutlet weak var cstBottomStackView: NSLayoutConstraint!
    @IBOutlet weak var btnBgPrevious: UIVisualEffectView!
    
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    var delegate: MasterDelegate?
    var isShowKeyboard = false
    var countView: Int = 0
    var isPortrait = false
    var marginButton = 16
    private var arraySUViews: [SUViewSettings] = {
        let storyboard = UIStoryboard(name: "SignUp", bundle: Bundle.main)
        var array = [
            SUViewSettings(storyboard.instantiateViewController(withIdentifier: "SUNameViewController") as! SUNameViewController, NSLocalizedString("TITLE_FIRST_NAME", comment: ""), false),
            SUViewSettings(storyboard.instantiateViewController(withIdentifier: "SUDateViewController") as! SUDateViewController, NSLocalizedString("TITLE_BIRTH", comment: ""), false),
            SUViewSettings(storyboard.instantiateViewController(withIdentifier: "SUEmailViewController") as! SUEmailViewController, NSLocalizedString("TITLE_EMAIL", comment: ""), false),
            SUViewSettings(storyboard.instantiateViewController(withIdentifier: "SUPictureViewController") as! SUPictureViewController, NSLocalizedString("TITLE_PHOTO", comment: ""), true),
            SUViewSettings(storyboard.instantiateViewController(withIdentifier: "SUSportViewController") as! SUSportViewController, NSLocalizedString("TITLE_SPORT_FAV", comment: ""), false),
            SUViewSettings(storyboard.instantiateViewController(withIdentifier: "SUAuthorizationViewController") as! SUAuthorizationViewController, NSLocalizedString("AUTHORIZATIONS", comment: ""), false)
        ]
        return array
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        determineMyDeviceOrientation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        add(asChildViewController: arraySUViews[0].type, withMargin: false)
        updateView(countView)
        btnOptional.setTitleColor(ThemeManager.currentTheme().unselectedFontColor, for: .normal)
        pgSign.transform = pgSign.transform.scaledBy(x: -1, y: 2)
        pgSign.progressTintColor = ThemeManager.currentTheme().unselectedFontColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btnBgPrevious.layer.cornerRadius = btnBgPrevious.frame.height/2
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            isPortrait = false
        } else {
            isPortrait = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func determineMyDeviceOrientation() {
        switch UIApplication.shared.statusBarOrientation {
        case .portrait, .portraitUpsideDown:
            isPortrait = true
            break
        case .landscapeLeft, .landscapeRight:
            isPortrait = false
            break
        case .unknown:
            break
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
        guard let progress = pgSign else { return }
        progress.progressTintColor = ThemeManager.currentTheme().unselectedFontColor
    }
    
    func updateView(_ count: Int) {
        if UIScreen.main.bounds.height == 568 {
            self.changeProgress(count)
            self.changeFixStyle(count)
        } else {
            UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveLinear], animations: {
                self.changeProgress(count)
            })
            UIView.transition(with: self.view, duration: 1.0, options: .transitionCrossDissolve, animations: {
                self.changeFixStyle(count)
            })
        }
    }
    
    func changeFixStyle(_ count: Int) {
        self.lblTitle.text = self.arraySUViews[count].title
        count+1 == self.arraySUViews.count ? self.btnNext.setTitle(NSLocalizedString("COMPLETE", comment: ""), for: .normal) : self.btnNext.setTitle(NSLocalizedString("CONTINUE", comment: ""), for: .normal)
        self.btnOptional.alpha = self.arraySUViews[count].optional == true ? 1 : 0
        count == 0 ? self.btnPrevious.setImage(#imageLiteral(resourceName: "Cancel"), for: .normal) : self.btnPrevious.setImage(#imageLiteral(resourceName: "Previous"), for: .normal)
    }
    
    func changeProgress(_ count: Int) {
        let percentage: Float = Float(count+1)/Float(arraySUViews.count) * 100
        self.pgSign.progress = Float(100 - percentage) / 100
    }
    
    
}


// MARK: Child ViewController Manager
extension SUMasterViewController {
    private func add(asChildViewController viewController: ChildView, withMargin margin: Bool, isNext: Bool? = false) {
        addChild(viewController)
        viewController.view.backgroundColor = .clear
        viewController.delegate = self
        vwContainer.frame.size.width = UIScreen.main.bounds.width
        viewController.view.frame.origin = vwContainer.bounds.origin
        viewController.view.frame.size.height = vwContainer.bounds.size.height
        viewController.view.frame.size.width = UIScreen.main.bounds.width
        viewController.view.center.x = vwContainer.center.x
        if margin { viewController.view.frame.origin.x = isNext! ? view.frame.width : -view.frame.width }
        vwContainer.addSubview(viewController.view)
        viewController.didMove(toParent: self)
    }
    
    private func remove(asChildViewController viewController: UIViewController) {
        viewController.willMove(toParent: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParent()
    }
    
    
}


// MARK: IBAction
extension SUMasterViewController {
    @IBAction func btnNextTpd(_ sender: UIButton) {
        self.delegate = (arraySUViews[self.countView].type as? MasterDelegate)
        delegate?.nextBtnTpd()
        if countView+1 < arraySUViews.count {
            add(asChildViewController: arraySUViews[countView+1].type, withMargin: true, isNext: true)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveLinear], animations: {
                self.arraySUViews[self.countView].type.view.frame.origin.x += -self.view.frame.width
                self.arraySUViews[self.countView+1].type.view.frame.origin.x += -self.view.frame.width
                self.updateView(self.countView+1)
            }, completion: { _ in
                self.remove(asChildViewController: self.arraySUViews[self.countView].type)
                self.countView += 1
            })
        }
    }
    
    @IBAction func btnPreviousTpd(_ sender: UIButton) {
        if countView > 0 {
            add(asChildViewController: arraySUViews[countView-1].type, withMargin: true, isNext: false)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveLinear], animations: {
                self.arraySUViews[self.countView].type.view.frame.origin.x += self.view.frame.width
                self.arraySUViews[self.countView-1].type.view.frame.origin.x += self.view.frame.width
                self.updateView(self.countView-1)
            }, completion: { _ in
                self.remove(asChildViewController: self.arraySUViews[self.countView].type)
                self.countView -= 1
            })
        }
        if countView == 0 {
            CacheService.shared.deleteFromDb(object: user) {
                if let previousView = UIStoryboard(name: "SignIn", bundle: nil).instantiateViewController(withIdentifier: "InitialController") as? SignInViewController {
                    self.present(previousView, animated: true, completion: nil)
                }
            }    
        }
    }
    
    @IBAction func optionalTpd(_ sender: UIButton) {
        self.delegate = (arraySUViews[self.countView].type as! MasterDelegate)
        delegate?.optionalBtnTpd!()
        if countView+1 < arraySUViews.count {
            add(asChildViewController: arraySUViews[countView+1].type, withMargin: true, isNext: true)
            UIView.animate(withDuration: 0.3, delay: 0.0, options: [.curveLinear], animations: {
                self.arraySUViews[self.countView].type.view.frame.origin.x += -self.view.frame.width
                self.arraySUViews[self.countView+1].type.view.frame.origin.x += -self.view.frame.width
                self.updateView(self.countView+1)
            }, completion: { _ in
                self.remove(asChildViewController: self.arraySUViews[self.countView].type)
                self.countView += 1
            })
        }
    }
    
    
}


// MARK: Keyboard Manager
extension SUMasterViewController {
    @objc func keyboardWillShow(_ notification: Notification) {
        if !isShowKeyboard {
            if isPortrait {
                if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                    let keyboardHeight = keyboardFrame.cgRectValue.height
                    cstBottomStackView.constant = keyboardHeight + CGFloat(marginButton)
                }
            }
            isShowKeyboard = !isShowKeyboard
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if isShowKeyboard {
            if isPortrait {
                if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                    let keyboardHeight = keyboardFrame.cgRectValue.height
                    if lblTitle.text == NSLocalizedString("TITLE_PHOTO", comment: "") {
                        if UIScreen.main.bounds.height == 812 { animBtnNext(withTranslationY: cstBottomStackView.constant-keyboardHeight+35) } else { animBtnNext(withTranslationY: cstBottomStackView.constant-keyboardHeight) }
                    } else {
                        cstBottomStackView.constant = keyboardHeight + CGFloat(marginButton)
                    }
                }
            }
            if self.countView == 2 {
                cstBottomStackView.constant = 35
            }
            isShowKeyboard = !isShowKeyboard
        }
    }
    
    @objc func keyboardWillChangeFrame(_ notification: Notification) {
        if !isPortrait {
            if let keyboardFrameEnd: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
                if keyboardFrameEnd.cgRectValue.minY > keyboardFrameEnd.cgRectValue.width {
                    cstBottomStackView.constant = keyboardFrameEnd.cgRectValue.height + keyboardFrameEnd.cgRectValue.width/3 + CGFloat(marginButton)
                } else {
                    if let keyboardFrameBegin: NSValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue, let keyboardCenterBegin: NSValue = notification.userInfo?["UIKeyboardCenterBeginUserInfoKey"] as? NSValue {
                        if keyboardCenterBegin.cgPointValue.y - keyboardFrameBegin.cgRectValue.minY == keyboardFrameEnd.cgRectValue.height/2 || keyboardCenterBegin.cgPointValue.y - keyboardFrameBegin.cgRectValue.minY == 0 {
                            cstBottomStackView.constant = keyboardFrameEnd.cgRectValue.height + CGFloat(marginButton)
                        } else {
                            cstBottomStackView.constant = (keyboardFrameBegin.cgRectValue.height + keyboardFrameBegin.cgRectValue.width/4) + CGFloat(marginButton)
                        }
                    }
                }
            }
        }
    }
    
    func animBtnNext(withTranslationY translationY: CGFloat) {
        self.view.setNeedsLayout()
        
        if UIScreen.main.bounds.height == 812 { self.cstBottomStackView.constant = translationY + 15 } else {
            self.cstBottomStackView.constant = translationY + 15
        }
        self.view.setNeedsUpdateConstraints()
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [.curveLinear], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
}


extension SUMasterViewController: ChildDelegate {
    func isButtonEnable() -> Bool {
        return btnNext.isEnabled
    }
    
    func tfShoundReturn() {
        btnNext.sendActions(for: .touchUpInside)
    }
    
    func updateButton(_ isEnabled: Bool) {
        btnNext.isEnabled = isEnabled
    }
    
    
}
