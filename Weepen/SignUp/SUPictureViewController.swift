//
//  SUPictureViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit


class SUPictureViewController: ChildView {

    @IBOutlet weak var ivPicture: UIImageView!
    @IBOutlet weak var btnAddPicture: UpperButton!
    @IBOutlet weak var cstHeightbtnAddPicture: NSLayoutConstraint!
    
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnAddPicture.applyGradient(ThemeManager.currentColor().gradientColor, false)
    }

    override func viewWillAppear(_ animated: Bool) {
        if user.picture != nil {
            hideBtnAddPicture()
            ivPicture.image = UIImage.init(data: user.picture!)
        }
        if ivPicture.image == nil {
            didupdateButton(false)
        }
    }

    func hideBtnAddPicture() {
        cstHeightbtnAddPicture.constant = ivPicture.frame.width
        self.view.layoutIfNeeded()
        btnAddPicture.removeGraident(btnAddPicture.layer)
        btnAddPicture.backgroundColor = .clear
        let title = NSAttributedString(string: "", attributes: nil)
        btnAddPicture.setAttributedTitle(title, for: .normal)
        didupdateButton(true)
    }
    
    @IBAction func pictureTapped(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            return
        }
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        
        present(imagePicker, animated: true)
    }
    
}

extension SUPictureViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        defer {
            picker.dismiss(animated: true)
        }
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        ivPicture.image = image
        hideBtnAddPicture()

    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        defer {
            picker.dismiss(animated: true)
        }
    }
    
    
}

extension SUPictureViewController: MasterDelegate {
    func optionalBtnTpd() {
        try! CacheService.shared.getDatabase().write { user.picture = #imageLiteral(resourceName: "no-photo").jpegData(compressionQuality: 1) }
    }
    
    func nextBtnTpd() {
        try! CacheService.shared.getDatabase().write { user.picture = self.ivPicture.image!.jpegData(compressionQuality: 1) }
    }
        
    
    
}
