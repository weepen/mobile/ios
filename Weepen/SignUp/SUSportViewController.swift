//
//  SUSportViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import Magnetic
import SpriteKit
import RealmSwift

class ImageNode: Node {
    override var image: UIImage? {
        didSet {
            sprite.texture = image.map { SKTexture(image: $0) }
        }
    }
    override func selectedAnimation() {}
    override func deselectedAnimation() {}
}

class SUSportViewController: ChildView {
    
    @IBOutlet weak var vwChooseSport: MagneticView! {
        didSet {
            magnetic.magneticDelegate = self
            magnetic.backgroundColor = .clear
        }
    }
    
    var magnetic: Magnetic { return vwChooseSport.magnetic }
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if user.sports.count > 0 {
            didupdateButton(true)
        } else {
            didupdateButton(false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if magnetic.children.count == 1 {
            for case let sport as SportRealm in CacheService.shared.getDataFromDB(object: SportRealm()) {
                var cover: UIImage? = nil
                if let image = sport.cover {
                    cover = UIImage(data: image)!
                }
                let node = Node(text: NSLocalizedString("\(sport.title!.uppercased())", comment: ""), image: cover, color: UIColor.hexColor(UInt((sport.codeColor.value)!)), uuid: sport.sportUUID!, radius: 40)
                magnetic.addChild(node)
                if (CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.filter("sportUUID = '\(sport.sportUUID!)'").count)! > 0 {
                    node.isSelected = true
                    node.fillColor = .clear
                }
            }
        }
    }

    
    
}

extension SUSportViewController: MagneticDelegate {
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        try! CacheService.shared.getDatabase().write {
            let user = CacheService.shared.getDatabase().objects(UserRealm.self).first!
            user.sports.append(CacheService.shared.getDataFromDB(object: SportRealm()).filter("sportUUID = '\(node.uuid!)'").first as! SportRealm)
        }
        didupdateButton(true)
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        if let sport = user.sports.filter("sportUUID = '\(node.uuid!)'").first {
            try! CacheService.shared.getDatabase().write { CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.remove(at: (CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.index(of: sport)!)!) }
        }
        if user.sports.count == 0 {
            didupdateButton(false)
        }
    }
    
    
}
