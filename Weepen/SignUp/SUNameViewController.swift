//
//  SUNameViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 04/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class SUNameViewController: ChildView {

    @IBOutlet weak var tfName: SkyFloatingLabelTextField!
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfName.placeholder = NSLocalizedString("FIRST_NAME", comment: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tfName.becomeFirstResponder()
    
        if user.name != nil {
            tfName.text = user.name
            didupdateButton(true)
        }
    }


}

extension SUNameViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let name = (text as NSString).replacingCharacters(in: range, with: string)
            name.isName() && !name.isOnlySpecial() && name.count >= 2 && name.count <= 64 ? didupdateButton(true) : didupdateButton(false)
        }
        return true
    }
 
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if didIsEnable() {
            didtfShoundReturn()
        }
        return true
    }
    
    
}

extension SUNameViewController: MasterDelegate {
    func nextBtnTpd() {
        do {
            try CacheService.shared.getDatabase().write {
                guard !user.isInvalidated else { return }
                user.name = tfName.text!
            }
        } catch let error {
            print("***SU NAME ERROR: [\(error)]***")
        }
    }
    
    
}

