//
//  SUEmailViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 05/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField


class SUEmailViewController: ChildView {

    @IBOutlet weak var tfMail: SkyFloatingLabelTextField!
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    
    
    override func viewWillAppear(_ animated: Bool) {
        tfMail.becomeFirstResponder()
        if user.email != nil {
            tfMail.text = user.email
            didupdateButton(true)
        } else {
            didupdateButton(false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tfMail.placeholder = NSLocalizedString("EMAIL", comment: "")
    }


}

extension SUEmailViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text {
            let mail = (text as NSString).replacingCharacters(in: range, with: string)
            mail.isEmail() ? didupdateButton(true) : didupdateButton(false)
        }
        return true
    }
    
    
}

extension SUEmailViewController: MasterDelegate {
    func optionalBtnTpd() {
        try! CacheService.shared.getDatabase().write { user.email = "" }
    }
    
    func nextBtnTpd() {
        try! CacheService.shared.getDatabase().write { user.email = tfMail.text! }
    }
    
    
}
