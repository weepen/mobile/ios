//
//  SUDateViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 04/11/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class UpTextField: SkyFloatingLabelTextField {
    
    @IBOutlet var upDelegate: UpTextFieldDelegate?
    
    required override init(frame: CGRect) {
        super.init(frame:frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.textAlignment = .center
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        upDelegate?.backspacePressed(self)
    }
}

@objc protocol UpTextFieldDelegate {
    func backspacePressed(_ textField: UpTextField)
}

class SUDateViewController: ChildView {

    @IBOutlet weak var tf1Day: UpTextField!
    @IBOutlet weak var tf2Day: UpTextField!
    @IBOutlet weak var tf1Month: UpTextField!
    @IBOutlet weak var tf2Month: UpTextField!
    @IBOutlet weak var tf1Year: UpTextField!
    @IBOutlet weak var tf2Year: UpTextField!
    @IBOutlet weak var tf3Year: UpTextField!
    @IBOutlet weak var tf4Year: UpTextField!
    
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    var dateCheck: String = ""
    var canSupp = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let langStr = Locale.current.languageCode
        if langStr == "fr" {
            tf1Day.placeholder = "J"
            tf2Day.placeholder = "J"
            tf1Month.placeholder = "M"
            tf2Month.placeholder = "M"
            tf1Year.placeholder = "A"
            tf2Year.placeholder = "A"
            tf3Year.placeholder = "A"
            tf4Year.placeholder = "A"
        } else {
            tf1Day.placeholder = "M"
            tf2Day.placeholder = "M"
            tf1Month.placeholder = "D"
            tf2Month.placeholder = "D"
            tf1Year.placeholder = "Y"
            tf2Year.placeholder = "Y"
            tf3Year.placeholder = "Y"
            tf4Year.placeholder = "Y"
  
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        if user.birthday != nil {
            fillTextfields()
            didupdateButton(true)
        } else {
            fillEmptyTextfields()
            didupdateButton(false)
        }
    }
    
    @IBAction func changeText(_ textField: SkyFloatingLabelTextField) {
        if textField.text!.count == 1 {
            textField.nextField?.isEnabled = true
            textField.nextField?.becomeFirstResponder()
            if textField.tag != 8 {
                textField.isEnabled = false
            }
        }
    }
    
    func fillTextfields() {
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyy"
        let date = formatter.string(from: user.birthday!)
        tf1Day.text = date[0]
        tf2Day.text = date[1]
        tf1Month.text = date[2]
        tf2Month.text = date[3]
        tf1Year.text = date[4]
        tf2Year.text = date[5]
        tf3Year.text = date[6]
        tf4Year.text = date[7]
        dateCheck = "\(String(date[0]))\(String(date[1]))\(String(date[2]))\(String(date[3]))\(String(date[4]))\(String(date[5]))\(String(date[6]))\(String(date[7]))"
        tf4Year.isEnabled = true
        tf1Day.isEnabled = false
        tf4Year.becomeFirstResponder()
    }
    
    func fillEmptyTextfields() {
        tf1Day.text = ""
        tf2Day.text = ""
        tf1Month.text = ""
        tf2Month.text = ""
        tf1Year.text = ""
        tf2Year.text = ""
        tf3Year.text = ""
        tf4Year.text = ""
        tf1Day.isEnabled = true
        tf2Day.isEnabled = false
        tf1Month.isEnabled = false
        tf2Month.isEnabled = false
        tf1Year.isEnabled = false
        tf2Year.isEnabled = false
        tf3Year.isEnabled = false
        tf4Year.isEnabled = false
        tf1Day.becomeFirstResponder()
    }
}

extension SUDateViewController: MasterDelegate {
    func nextBtnTpd() {
        let dateStg = "\(tf1Day.text!)\(tf2Day.text!)\(tf1Month.text!)\(tf2Month.text!)\(tf1Year.text!)\(tf2Year.text!)\(tf3Year.text!)\(tf4Year.text!)"
        let dateFormatter = DateFormatter()
        let langStr = Locale.current.languageCode
        if langStr == "fr" {
            dateFormatter.dateFormat = "ddMMyyyy"
        } else {
            dateFormatter.dateFormat = "MMddyyyy"
        }
        let date = dateFormatter.date(from: dateStg)
        do {
            try CacheService.shared.getDatabase().write {
                user.birthday = date
            }
        } catch let error {
            print("***SU BIRTH ERROR: [\(error)]***")
        }
    }
    
    
}

extension SUDateViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let charset: CharacterSet?
        let langStr = Locale.current.languageCode
        if langStr == "fr" {
            switch textField {
            case tf1Day:
                charset = CharacterSet(charactersIn: "0123")
            case tf2Day:
                if tf1Day.text! == "3" {
                    charset = CharacterSet(charactersIn: "01")
                } else if tf1Day.text! == "0" {
                    charset = CharacterSet(charactersIn: "123456789")
                } else {
                    charset = CharacterSet(charactersIn: "0123456789")
                }
            case tf1Month:
                charset = CharacterSet(charactersIn: "01")
            case tf1Year:
                charset = CharacterSet(charactersIn: "12")
            case tf2Year:
                if tf1Year.text! == "1" {
                    charset = CharacterSet(charactersIn: "9")
                } else {
                    charset = CharacterSet(charactersIn: "0")
                }
            case tf3Year:
                if tf2Year.text! == "9" {
                    charset = CharacterSet(charactersIn: "456789")
                } else {
                    charset = CharacterSet(charactersIn: "0")
                }
            default:
                charset = CharacterSet(charactersIn: "0123456789")
            }
        } else {
            switch textField {
            case tf1Day:
                charset = CharacterSet(charactersIn: "01")
            case tf2Day:
                if tf1Day.text! == "0" {
                    charset = CharacterSet(charactersIn: "123456789")
                } else {
                    charset = CharacterSet(charactersIn: "012")
                }
            case tf1Month:
                if tf1Day.text! == "0" && tf2Day.text! == "2" {
                    charset = CharacterSet(charactersIn: "012")
                } else {
                    charset = CharacterSet(charactersIn: "0123")
                }
            case tf2Month:
                if tf1Month.text! == "3" {
                    charset = CharacterSet(charactersIn: "01")
                } else if tf1Month.text! == "0" {
                    charset = CharacterSet(charactersIn: "123456789")
                } else {
                    charset = CharacterSet(charactersIn: "0123456789")
                }
            case tf1Year:
                charset = CharacterSet(charactersIn: "12")
            case tf2Year:
                if tf1Year.text! == "1" {
                    charset = CharacterSet(charactersIn: "9")
                } else {
                    charset = CharacterSet(charactersIn: "0")
                }
            case tf3Year:
                if tf2Year.text! == "9" {
                    charset = CharacterSet(charactersIn: "456789")
                } else {
                    charset = CharacterSet(charactersIn: "0")
                }
            default:
                charset = CharacterSet(charactersIn: "0123456789")
            }
        }
        
        dateCheck += string
        if langStr == "fr" {
            if string.rangeOfCharacter(from: charset!) != nil && string.isDate(textField, dateCheck) {
                if textField.tag == 8 {
                    didupdateButton(true)
                }
                return true
            }
        } else {
            if string.rangeOfCharacter(from: charset!) != nil && string.isDateUS(textField, dateCheck) {
                if textField.tag == 8 {
                    didupdateButton(true)
                }
                return true
            }
        }
        if string == "" {
            dateCheck = String(dateCheck.dropLast())
            textField.text! = ""
            didupdateButton(false)
            return false
        }
        dateCheck = String(dateCheck.dropLast())
        canSupp = true
        if textField == tf4Year && !(tf4Year.text?.isEmpty)! {
            return false
        }
        didupdateButton(false)
        textField.shake()
        return false
    }
    
    func isDay(_ textField1: UITextField, _ textField2: UITextField) -> Bool {
        let day = "\(textField1.text!)\(textField2.text!)"
        let passwordRegex = "(0[1-9]|[12]\\d|3[01])"
        return NSPredicate(format: "SELF MATCHES %@", passwordRegex).evaluate(with: day)
    }
    
    
}

extension SUDateViewController: UpTextFieldDelegate {
    func backspacePressed(_ textField: UpTextField) {
        if canSupp {
            if let previousField = textField.superview?.viewWithTag(textField.tag - 1) as? UpTextField {
                dateCheck = String(dateCheck.dropLast())
                previousField.isEnabled = true
                previousField.text! = ""
                previousField.becomeFirstResponder()
                textField.isEnabled = false
            }
        } else {
            canSupp = true
        }
    }
    
    
}
