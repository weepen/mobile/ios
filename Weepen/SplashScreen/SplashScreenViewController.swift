//
//  ViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 19/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RevealingSplashView
import Hero
import RealmSwift



class SplashScreenViewController: UIViewController {
    
    var gradientLayer: CAGradientLayer!
    let LocationMgr = LocationService.shared
    let CacheMgr = CacheService.shared
    var status = 0
    var waitingLogo = UIImageView()
    let widthScreen = UIScreen.main.bounds.width/3
    let heightScreen = UIScreen.main.bounds.height/3

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        LocationMgr.delegate = self
        createGradientLayer()
        addLogo()
        self.waitingLogo.removeFromSuperview()
        self.createSplashView()
    }
    
    func addLogo() {
        waitingLogo = UIImageView(frame: CGRect(x: 0, y: 0, width: widthScreen, height: heightScreen))
        waitingLogo.center = self.view.center
        waitingLogo.contentMode = .scaleAspectFit
        waitingLogo.image = #imageLiteral(resourceName: "Launchscreen - Logo")
        self.view.addSubview(waitingLogo)
    }
    
    // TODO: Supprimer cette fonction doublon
    func createGradientLayer() {
        let layer = CAGradientLayer()
        layer.frame =  self.view.bounds
        layer.colors = [ThemeManager.currentColor().gradientColor[0].cgColor, ThemeManager.currentColor().gradientColor[1].cgColor]
        view.layer.addSublayer(layer)
    }
    //
    
    func createSplashView() {
        let revealingSplashView = RevealingSplashView(iconImage: #imageLiteral(resourceName: "Launchscreen - Logo"),iconInitialSize: CGSize(width: widthScreen, height: heightScreen), backgroundColor: UIColor(red:0.11, green:0.56, blue:0.95, alpha:0.0))
        revealingSplashView.animationType = SplashAnimationType.swingAndZoomOut
        revealingSplashView.delay = 1
        revealingSplashView.duration = 1
        self.view.addSubview(revealingSplashView)
        revealingSplashView.startAnimation(){
            switch self.status {
            case 0:
                self.performSegue(withIdentifier: "noLocation", sender: nil)
            default:
                self.prepareToPush()
            }
        }
    }
    
    func prepareToPush() {
        if let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm {
            if user.uuid == nil {
                push("SignUp", "SUMasterViewController")
            } else {
                if self.LocationMgr.isAuthorized() == -1 {
                    self.LocationMgr.askAuthorizationLocation()
                } else {
                    push("Main", "MainTabController")
                }
            }
        } else {
            push("SignIn", "InitialController")
        }
    }
    
    func push(_ sbName: String, _ vcName: String) {
        let storyboard = UIStoryboard(name: sbName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: vcName)
        self.present(controller, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
}


extension SplashScreenViewController: LocationUpdateProtocol {
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {}
    
    func locationDidChangeAuthorization(status: Int) {
        self.status = status
    }
    
    
}






