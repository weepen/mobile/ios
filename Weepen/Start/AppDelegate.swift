//
//  AppDelegate.swift
//  Weepen
//
//  Created by Louis Cheminant on 26/07/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKCoreKit
import FirebaseUI
import UserNotifications
import Ambience


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    
    internal func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        ThemeManager.applyTheme(theme: ThemeManager.currentTheme(), color: Color.Orange)
        EnvManager.applyEnv(env: EnvManager.currentEnv())
        SportRealm().addSnapshotListenerSport()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        UNUserNotificationCenter.current().delegate = self
        let _ = Ambience.shared
        Ambience.forcedState = .regular
        Ambience.currentState = .regular
        FCMService.shared.delegate = self
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        ReachabilityService.shared.startMonitoring()
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) { 
        ReachabilityService.shared.stopMonitoring()
    }
    
    internal func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        if let handled = FBSDKApplicationDelegate.sharedInstance()?.application(application, open: url, options: options) {
            return handled
        }
        if Auth.auth().canHandle(url) {
            return true
        }
        return false
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Auth.auth().setAPNSToken(deviceToken, type: AuthAPNSTokenType.prod)
        Messaging.messaging().apnsToken = deviceToken
        print("APNs device token: \(deviceTokenString)")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler(UIBackgroundFetchResult.noData)
            return
        }
        guard userInfo["gcm.message_id"] != nil, let scope = userInfo["scope"] as? String else { completionHandler(UIBackgroundFetchResult.noData); return }
        switch scope {
        case "event":
            guard let action = userInfo["action"] as? String else { completionHandler(UIBackgroundFetchResult.noData); return }
            switch action {
                case "update":
                    configNotificationEventUpdate(userInfo)
                    completionHandler(UIBackgroundFetchResult.noData)
                case "delete":
                    configNotificationEventDelete(userInfo)
            default:
                completionHandler(UIBackgroundFetchResult.noData)
            }
        case "chat":
            configNotificationChat(userInfo)
            switch application.applicationState {
            case .active:
                print("do stuff in case App is active")
            case .background:
                print("do stuff in case App is in background")
            case .inactive:
                print("do stuff in case App is inactive")
            }
            completionHandler(UIBackgroundFetchResult.noData)
        default:
            completionHandler(UIBackgroundFetchResult.noData)
        }
    }
    
    func configNotificationEventUpdate(_ userInfo: [AnyHashable : Any]) {
//        if let _event = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(eventId)'").first {
//        }
    }
    
    func configNotificationEventDelete(_ userInfo: [AnyHashable : Any]) {

    }
    
    func configNotificationChat(_ userInfo: [AnyHashable : Any]) {
        guard let body = userInfo["body"] as? String, let senderId = userInfo["senderId"] as? String, let eventId = userInfo["eventId"] as? String else { return }
        let title = NSLocalizedString("UPDATE_CHAT_NOTIFICATION_TITLE", comment: "")
        var subtitles = ""
        if let _event = CacheService.shared.getDataFromDB(object: EventRealm()).filter("uid = '\(eventId)'").first {
            if let _owner = CacheService.shared.getDataFromDB(object: OwnerRealm()).filter("uuid = '\(senderId)'").first {
                subtitles = "From \((_owner as! OwnerRealm).name ?? "") in \((_event as! EventRealm).title!)"
                NotificationService.shared.createNotification(UUID().uuidString, "chat", title, subtitles, body, UNNotificationSound.default, userInfo)
            } else if let _participant = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(senderId)'").first {
                print((_participant as! ParticipantRealm).name!)
                print((_event as! EventRealm).title!)
                subtitles = "From \((_participant as! ParticipantRealm).name!) in \((_event as! EventRealm).title!)"
                NotificationService.shared.createNotification(UUID().uuidString, "chat", title, subtitles, body, UNNotificationSound.default, userInfo)
            } else {
                ParticipantRealm().getParticipantInfo(senderId) { (name, latitude, longitude) in
                    subtitles = "From \(name) in \((_event as! EventRealm).title!)"
                    NotificationService.shared.createNotification(UUID().uuidString, "chat", title, subtitles, body, UNNotificationSound.default, userInfo)
                }
            }
        } else {
            NotificationService.shared.createNotification(UUID().uuidString, "chat", title, "Une notif", body, UNNotificationSound.default, userInfo)
        }
    }
    
    
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    internal func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        if Auth.auth().canHandleNotification(userInfo) {
            completionHandler()
            return
        }
        completionHandler()
    }
    
    
}


extension AppDelegate: FCMUProtocol {
    func messagingDidReceiveRemoteMessage(dictionary: NSDictionary) {
        print(dictionary)
    }
    
    func messagingDidReceiveRegistrationToken(token: String) {
        UserDefaults.standard.set(token, forKey: "registrationToken")
    }
    
    
}
