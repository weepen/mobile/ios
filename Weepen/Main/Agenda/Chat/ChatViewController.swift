/// Copyright (c) 2018 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import Photos
import Firebase
import MessageKit
import FirebaseFirestore
import Ambience

final class ChatViewController: MessagesViewController {
    
    private var isSendingPhoto = false {
        didSet {
            DispatchQueue.main.async {
                self.messageInputBar.leftStackViewItems.forEach { item in
                    item.isEnabled = !self.isSendingPhoto
                }
            }
        }
    }
    
    private let db = Firestore.firestore()
    private var reference: CollectionReference?
    
    private var messages: [Message] = []
    private var messageListener: ListenerRegistration?
    
    private var user: UserRealm = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    private let channel: Channel
    
    var lastDate: Date? = nil
    
    lazy var formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        return formatter
    }()
    
    deinit {
        messageListener?.remove()
    }
    
    init(user: UserRealm, channel: Channel) {
        self.user = user
        self.channel = channel
        super.init(nibName: nil, bundle: nil)
        self.iMessage()
        messagesCollectionView.backgroundColor = ThemeManager.currentTheme().downView
        title = channel.name
    }
    
    func iMessage() {
        defaultStyle()
        messageInputBar.isTranslucent = false
        messageInputBar.backgroundView.backgroundColor = ThemeManager.currentTheme().downView
        messageInputBar.separatorLine.isHidden = true
        messageInputBar.inputTextView.backgroundColor = Ambience.currentState == .invert ? ThemeManager.currentTheme().upperView : UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        messageInputBar.inputTextView.placeholderTextColor = ThemeManager.currentTheme().unselectedFontColor
        messageInputBar.inputTextView.textContainerInset = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 36)
        messageInputBar.inputTextView.placeholderLabelInsets = UIEdgeInsets(top: 8, left: 20, bottom: 8, right: 36)
        messageInputBar.inputTextView.layer.borderColor = ThemeManager.currentTheme().unselectedFontColor.cgColor
        messageInputBar.inputTextView.layer.borderWidth = 1.0
        messageInputBar.inputTextView.layer.cornerRadius = 16.0
        messageInputBar.inputTextView.layer.masksToBounds = true
        messageInputBar.inputTextView.scrollIndicatorInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        messageInputBar.setRightStackViewWidthConstant(to: 36, animated: true)
        messageInputBar.setStackViewItems([messageInputBar.sendButton], forStack: .right, animated: true)
        messageInputBar.sendButton.imageView?.backgroundColor = ThemeManager.currentColor().mainColor
        messageInputBar.sendButton.contentEdgeInsets = UIEdgeInsets(top: 3, left: 3, bottom: 3.5, right: 3)
        messageInputBar.sendButton.setSize(CGSize(width: 36, height: 36), animated: true)
        messageInputBar.sendButton.image = #imageLiteral(resourceName: "Arrow-Send")
        messageInputBar.inputTextView.placeholder = NSLocalizedString("NEW_MESSAGE", comment: "")
        messageInputBar.sendButton.title = nil
        messageInputBar.sendButton.imageView?.layer.cornerRadius = 16
        messageInputBar.sendButton.backgroundColor = .clear
        messageInputBar.textViewPadding.right = -38
    }
    
    func defaultStyle() {
        let newMessageInputBar = MessageInputBar()
        newMessageInputBar.sendButton.tintColor = UIColor(red: 69/255, green: 193/255, blue: 89/255, alpha: 1)
        newMessageInputBar.delegate = self
        messageInputBar = newMessageInputBar
        reloadInputViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.barStyle = ThemeManager.currentTheme().barStyle
        guard let id = channel.id else {
            navigationController?.popViewController(animated: true)
            return
        }
        
        reference = FirebaseService.shared.dbChat.document(id).collection("messages")
        messageListener = reference?.addSnapshotListener { querySnapshot, error in
            guard let snapshot = querySnapshot else {
                print("Error listening for channel updates: \(error?.localizedDescription ?? "No error")")
                return
            }
            
            snapshot.documentChanges.forEach { change in
                self.handleDocumentChange(change)
            }
        }
        
        navigationItem.largeTitleDisplayMode = .never
        
        maintainPositionOnKeyboardFrameChanged = true
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
    }
    
    // MARK: - Helpers
    
    private func save(_ message: Message) {
        reference?.addDocument(data: message.representation) { error in
            if let e = error {
                print("Error sending message: \(e.localizedDescription)")
                return
            }
            
            self.messagesCollectionView.scrollToBottom()
        }
    }
    
    private func insertNewMessage(_ message: Message) {
        guard !messages.contains(message) else {
            return
        }
        
        messages.append(message)
        messages.sort()
        
        let isLatestMessage = messages.index(of: message) == (messages.count - 1)
        let shouldScrollToBottom = isLatestMessage // messagesCollectionView.isAtBottom && isLatestMessage
        
        messagesCollectionView.reloadData()
        
        if shouldScrollToBottom {
            DispatchQueue.main.async {
                self.messagesCollectionView.scrollToBottom(animated: true)
            }
        }
    }

    private func handleDocumentChange(_ change: DocumentChange) {
        guard let message = Message(document: change.document, channel: self.channel) else {
            return
        }
        insertNewMessage(message)
    }
    
    public var senderAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 14)!,
            NSAttributedString.Key.underlineColor: UIColor.white
        ]
    }()
    
    public var receiverAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.foregroundColor: ThemeManager.currentColor().mainColor,
            NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 14)!,
            NSAttributedString.Key.underlineColor: ThemeManager.currentColor().mainColor
        ]
    }()
    
    public var timeAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.foregroundColor: ThemeManager.currentTheme().unselectedFontColor,
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameRegular, size: 10)!,
        ]
    }()
    
    public var nameAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.foregroundColor: ThemeManager.currentTheme().unselectedFontColor,
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 12)!,
            ]
    }()
    
    public var dateAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.foregroundColor: ThemeManager.currentTheme().unselectedFontColor,
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameMedium, size: 11)!,
            ]
    }()
    
    public var contentAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameBlack, size: 15)!,
            ]
    }()


    
}

// MARK: - MessagesDisplayDelegate

extension ChatViewController: MessagesDisplayDelegate {
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? .white : .white
    }
    
    internal func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
        return isFromCurrentSender(message: message) ? self.senderAttributes : self.receiverAttributes
    }
    
    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        return [.url, .address, .phoneNumber, .date, .transitInformation]
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? ThemeManager.currentColor().mainColor : ThemeManager.currentTheme().unselectedFontColor
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
        return false
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        guard isFromCurrentSender(message: message), let picture = (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).picture else {
            getAvatarFor(sender: message.sender) { (avatar) in
                DispatchQueue.main.async  {
                    avatarView.set(avatar: avatar)
                }
            }
            return
        }
        avatarView.set(avatar: Avatar(image: UIImage(data: picture), initials: user.name![0]))
    }
    

}

// MARK: - MessagesLayoutDelegate

extension ChatViewController: MessagesLayoutDelegate {
    
    func cellTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        if indexPath.section % 10 == 0 {
            return 10
        }
        return 0
    }
    
    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return .zero
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 0, height: 8)
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }
    
    func messageTopLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
    func messageBottomLabelHeight(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 16
    }
    
}

// MARK: - MessagesDataSource

extension ChatViewController: MessagesDataSource {
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }

    func currentSender() -> Sender {
        return Sender(id:  (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!, displayName:  (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).name!)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
        //return NSAttributedString(string: messages[indexPath.section], attributes: contentAttributes)
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        if indexPath.section == 0 {
            lastDate = message.sentDate
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: dateAttributes)
        }
        
        let diifDate = message.sentDate.hours(from: (lastDate)!)
        
        if diifDate >= 2 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: dateAttributes)
            
        }
        if (message.sentDate.days(from: lastDate!)) > 1 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: dateAttributes)
            
        }
        if indexPath.section % 10 == 0 {
            return NSAttributedString(string: MessageKitDateFormatter.shared.string(from: message.sentDate), attributes: dateAttributes)
        }
        return nil
    }
    
    func messageTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(string: name, attributes: nameAttributes)
    }
    
    func messageBottomLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let dateString = formatter.string(from: message.sentDate)
        return NSAttributedString(string: dateString, attributes: timeAttributes)
    }
    
    func getAvatarFor(sender: Sender, completion: @escaping (Avatar) -> Void) {
        if let participantObj = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(sender.id)'").first {
            let participant = participantObj as! ParticipantRealm
            if let data = participant.picture {
                  completion(Avatar(image: UIImage.init(data: data)))
            } else {
                ParticipantRealm().getPictureParticipant(sender.id, participant.urlPicture) { (data, url) in
                    DispatchQueue.main.async {
                        try! CacheService.shared.getDatabase().write {
                            guard !participant.isInvalidated else { return }
                            participant.picture = data
                            participant.urlPicture = url
                        }
                        completion(Avatar(image: UIImage.init(data: data)))
                    }
        
                }
            }
        } else {
            ParticipantRealm().getPictureParticipant(sender.id, nil) { (data, url) in
                completion(Avatar(image: UIImage.init(data: data)))
            }
        }
    }

    
}

// MARK: - MessageInputBarDelegate

extension ChatViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        let message = Message(user: user, content: text, channel: channel)
        
        save(message)
        inputBar.inputTextView.text = ""
    }
    
}

struct Channel {
    
    let id: String?
    let name: String
    let event: EventRealm?
    
    init(name: String, id: String, event: EventRealm?) {
        self.id = id
        self.name = name
        self.event = event
    }
    
    
}
