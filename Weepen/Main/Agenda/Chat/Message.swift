/// Copyright (c) 2018 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Firebase
import MessageKit
import FirebaseFirestore

struct Message: MessageType {
    var kind: MessageKind
    
    
    let id: String?
    let content: String
    let sentDate: Date
    let sender: Sender
    var channel: Channel
    
    var messageId: String {
        return id ?? UUID().uuidString
    }
    
    var image: UIImage? = nil
    var downloadURL: URL? = nil
    
    init(user: UserRealm, content: String, channel: Channel) {
        self.sender = Sender(id: user.uuid!, displayName: user.name!)
        self.content = content
        self.sentDate = Date()
        self.id = nil
        self.channel = channel
        self.kind = .attributedText(NSAttributedString(string: content, attributes: contentAttributes))
    }
    
    init?(document: QueryDocumentSnapshot, channel: Channel) {
        let data = document.data()
        
        guard let sentDate = data["date"] as? Timestamp else {
            return nil
        }
        guard let senderRef = data["sender"] as? DocumentReference else {
            return nil
        }
        guard let senderName = data["senderName"] as? String else {
            return nil
        }
        self.id = document.documentID
        if let content = data["message"] as? String {
            self.content = content
            downloadURL = nil
        } else {
            return nil
        }
        self.sentDate = sentDate.dateValue()
        let senderID = senderRef.path.replacingOccurrences(of: "\(FirebaseService.shared.dbUser.path)/", with: "", options: .literal, range: nil)
        self.sender = Sender(id: senderID, displayName: senderName)
        self.channel = channel
        self.kind = .attributedText(NSAttributedString(string: content, attributes: contentAttributes))

        self.setParticipants(senderID, senderName, event: channel.event!)
    }
    
    public var contentAttributes: [NSAttributedString.Key: Any] = {
        return [
            NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameBlack, size: 15)!,
            NSAttributedString.Key.foregroundColor: UIColor.white,
            ]
    }()

    func setParticipants(_ participantUid: String, _ participantName: String, event: EventRealm) {
        let userUid = (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!
        guard participantUid != userUid, participantUid != event.uid else { return }
        if CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(participantUid)'").count == 0 {
            if let participant = ParticipantRealm(uid: participantUid) {
                participant.name = participantName
                CacheService.shared.addData(object: participant)
                try! CacheService.shared.getDatabase().write {
                    guard !event.isInvalidated else { return }
                    event.participants.append(participant)
                }
            }
        } else {
            if let participantObj = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(participantUid)'").first {
                let participant = participantObj as! ParticipantRealm
                if participant.events.filter("uid = '\(event.uid)'").count == 0 {
                    try! CacheService.shared.getDatabase().write {
                        guard !participant.isInvalidated, !event.isInvalidated else { return }
                        event.participants.append(participant)
                    }
                }
            }
        }
    }
    
}

extension Message: DatabaseRepresentation {
    
    var representation: [String : Any] {
        var rep: [String : Any] = [
            "date": sentDate,
            "sender": FirebaseService.shared.dbUser.document(sender.id),
            "senderName": sender.displayName
        ]
        rep["message"] = content
        
        return rep
    }
    
}

extension Message: Comparable {
    
    static func == (lhs: Message, rhs: Message) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func < (lhs: Message, rhs: Message) -> Bool {
        return lhs.sentDate < rhs.sentDate
    }
    
}
