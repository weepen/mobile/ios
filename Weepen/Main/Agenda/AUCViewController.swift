//
//  AgendaUserCreateViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 15/10/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import ListPlaceholder
import Firebase
import RealmSwift
import GradientLoadingBar
import ListPlaceholder
import MapKit
import SkyFloatingLabelTextField


protocol AUCViewControllerDelegate {
    func chatTappedC(_ indexPath: IndexPath)
    func emptyTappedC()
}

class AUCViewController: UIViewController {

    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var gradientLoadingBar: GradientLoadingBar?
    var nbEmptyCell = 0
    var userLocation: CLLocation?
    var delegate: AUCViewControllerDelegate?
    var token: NotificationToken?
    var isLoaded = false

    let distanceFormatter = MKDistanceFormatter()
    let refreshControl = UIRefreshControl()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(true, forKey: "isCreateVC")
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        EventRealm().getNBParticipantUpdate(.create)
        self.token = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate = true").observe({ (result) in
            self.tableView.applyChanges(changes: result, .create)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate = true").count == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupLocationDelegate()
        setupGradientLoader()
        setupRefreshControl()
        if CacheService.shared.getDatabase().objects(EventRealm.self).filter("isCreate == true").count == 0 { startLoadingEvent() }
        getEventsCreate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(false, forKey: "isCreateVC")
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        LocationService.shared.stopUpdatingLocation()
        self.token?.invalidate()
    }
    
    func setupLocationDelegate() {
        LocationService.shared.delegate = self
        LocationService.shared.startUpdatingLocation()
    }
    
    func setupTableView() {
        tableView.estimatedRowHeight = UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136 ? 140 : 210
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        tableView.separatorColor = .clear
    }
    
    func setupGradientLoader() {
        gradientLoadingBar = GradientLoadingBar(height: 3.5, gradientColorList: [UIColor.hexColor(0xA848D3), UIColor.hexColor(0xF12711), UIColor.hexColor(0xEC008C), UIColor.hexColor(0xF25D6C), UIColor.hexColor(0xFC834F), UIColor.hexColor(0xF5AF19)], onView: self.view)
        gradientLoadingBar?.gradientView.alpha = 0
    }
    
    func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        refreshControl.tintColor = ThemeManager.currentColor().mainColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("DOWNLOAD_EVENT", comment: ""), attributes: nil)
    }
    
    @objc func appMovedToBackground() {
        token?.invalidate()
    }
    
    @objc func appMovedToForeground() {
        refreshData(nil)
        self.token = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate = true").observe({ (result) in
            self.tableView.applyChanges(changes: result, .create)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate = true").count == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }
    
    @objc private func refreshData(_ sender: Any?) {
        getEventsCreate()
    }

    @IBAction func didEmptyTapped(_ sender: UIButton) {
        delegate?.emptyTappedC()
    }
    
    func getMainDate(_ item: EventRealm) -> (String, String, String, String) {
        let date: String = item.dateStart!.toString(dateFormat: "d MMM HH mm")
        let dateArray = date.components(separatedBy: " ")
        return (dateArray[0], dateArray[1], dateArray[2], dateArray[3])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "pushToEdit":
            guard let navController = segue.destination as? UINavigationController, let vc = navController.topViewController as? CreateViewController else { return }
            guard let eventIndex = (sender as? IndexPath)?.row else { return }
            if let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[eventIndex] as? EventRealm {
                vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                vc.navigationItem.leftItemsSupplementBackButton = true
                vc.event = item
                vc.isModify = true
            }
        case "pushToDescpC":
            guard let navController = segue.destination as? NavigationParticipantViewController, let vc = navController.topViewController as? DescriptionViewController else { return }
            guard let eventIndex = (sender as? IndexPath)?.row else { return }
            if let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[eventIndex] as? EventRealm {
                vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                vc.navigationItem.leftItemsSupplementBackButton = true
                vc.event = item
                vc.eventType = .participate
                vc.userLocation = LocationService.shared.currentLocation
            }
        default:
            fatalError("PB")
        }
    }
    
    
}

extension AUCViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nbEmptyCell != 0 {
            return nbEmptyCell
        } else if nbEmptyCell == 0 && CacheService.shared.getDatabase().objects(EventRealm.self).filter("isCreate = true").count == 0 && !isLoaded {
            revealEmptyView()
            return 0
        } else {
            revealTableView()
            return CacheService.shared.getDatabase().objects(EventRealm.self).filter("isCreate = true").count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if nbEmptyCell != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventCreateCell", for: indexPath) as! EventCreateCell
            cell.delegate = self
            let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[indexPath.row] as! EventRealm
            let (nbDate, month, hour, min) = getMainDate(item)
            cell.nbDateLbl.text = nbDate
            cell.monthLbl.text = month
            cell.hourLbl.text = "\(hour):\(min)"
            cell.titleLbl.text = item.title?.capitalized
            if let sport = item.sport {
                cell.sportIV.image = UIImage(data: sport.logo!)
                cell.sportLbl.textColor = UIColor.hexColor(UInt(sport.codeColor.value!))
                cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "").uppercased()
            }
            cell.priceLbl.text = item.price.value == 0 ? NSLocalizedString("FREE", comment: "") : "\(item.price.value!) \(item.currency!)"
            if item.nbParticipant.value == nil { cell.nbPlaceLbl.text = "•••" }
            else { cell.nbPlaceLbl.text = "\(item.nbMaxParticipant.value!-item.nbParticipant.value!)" }
            cell.distanceLbl.text = "\(LocationService.shared.getLocation(item.latitude.value!, item.longitude.value!)) \(NSLocalizedString("ACTUAL_POSITION", comment: ""))"
            cell.indexPath = indexPath
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136 ? 140 : 210
    }
    
    
}

extension AUCViewController {
    
    func startLoadingEvent() {
        nbEmptyCell = 4
        gradientLoadingBar?.show()
        isLoaded = true
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        tableView.reloadData()
        tableView.showLoader(ThemeManager.currentTheme().downView)
    }
    
    func endLoadingEvent() {
        tableView.hideLoader()
    }
    
    func getEventsCreate() {
        gradientLoadingBar?.show()
        isLoaded = true
        refreshControl.endRefreshing()
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        EventRealm().getEventsUserCreate(uidUser: (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!) { (result, events) in
            if result {
                let (removeEvents, updateEvents, addEvents) = EventRealm().orderEventsCreate(events)
                EventRealm().removeEvents(removeEvents, .create)
                self.removeEmptyCell()
                EventRealm().updateEvents(updateEvents, .create)
                EventRealm().addEvents(addEvents, .create)
                EventRealm().getNBParticipantUpdate(.create)
                if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate = true").count == 0 { self.revealEmptyView() }
                else { self.revealTableView() }
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.gradientLoadingBar?.gradientView.alpha = 0
                }, completion: { (result) in
                    if result { self.gradientLoadingBar?.hide(); self.isLoaded = false }
                })
                self.endLoadingEvent()
            }
        }
    }
    
    func revealEmptyView() {
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate == true").count == 0 {
            UIView.animate(withDuration: 0.3) {
                self.emptyView.alpha = 1
                self.tableView.alpha = 0
            }
        }
    }
    
    func revealTableView() {
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isCreate == true").count >= 0 {
            UIView.animate(withDuration: 0.1) {
                self.emptyView.alpha = 0
                self.tableView.alpha = 1
            }
        }
    }
    
    func removeEmptyCell() {
        if nbEmptyCell != 0 {
            var indexes = [IndexPath]()
            for index in 0...nbEmptyCell-1 {
                indexes.append(IndexPath(row: index, section: 0))
            }
            nbEmptyCell = 0
            tableView.deleteRows(at: indexes, with: .fade)
        }
    }
    
    
}

extension AUCViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    
}

extension AUCViewController: EventUserCreateCellDelegate {
    
    func dscpTapped(_ indexPath: IndexPath) {
        performSegue(withIdentifier: "pushToDescpC", sender: indexPath)
    }
    
    
    func chatTapped(_ indexPath: IndexPath) {
        delegate?.chatTappedC(indexPath)
    }
    
    func modifyTapped(_ indexPath: IndexPath) {
        performSegue(withIdentifier: "pushToEdit", sender: indexPath)

//        let storyboard = UIStoryboard(name: "Agenda", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "EditEvent") as! CreateViewController
//        vc.isModify = true
//        let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[indexPath.row] as! EventRealm
//        vc.event = item
//        self.present(vc, animated: true, completion: nil)
    }
    
    func deleteTapped(_ indexPath: IndexPath) {
        let alert = AlertView(appearance: ThemeManager.getAppearanceAlert(UIScreen.main.bounds.width))
        let subview = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width * 0.8, height: 120))
        subview.frame.origin.x = UIScreen.main.bounds.minX
        let label = UILabel(frame: CGRect(x: subview.frame.width/2-(UIScreen.main.bounds.width * 0.6)/2-10, y: 10, width: self.view.frame.width * 0.6, height: 50))
        label.text = NSLocalizedString("DELETE_EVENT_MESSAGE", comment: "")
        label.numberOfLines = 0
        label.font = UIFont.init(name: ThemeManager.currentTheme().fontNameBold, size: 17)
        label.textColor = ThemeManager.currentTheme().selectedFontColor
        label.adjustsFontSizeToFitWidth = true
        subview.addSubview(label)
        let textfield = SkyFloatingLabelTextField(frame: CGRect(x: subview.frame.width/2-(UIScreen.main.bounds.width * 0.6)/2-10, y: 60, width: UIScreen.main.bounds.width * 0.6, height: 50))
        textfield.placeholder = NSLocalizedString("DELETE", comment: "")
        textfield.autocapitalizationType = .words
        textfield.keyboardType = .default
        textfield.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
        subview.addSubview(textfield)
        alert.customSubview = subview
        alert.addButton(NSLocalizedString("DELETE", comment: ""), textColor: .white, showDurationStatus: true) {
            if textfield.text! == NSLocalizedString("DELETE", comment: "") {
                let event = (CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[indexPath.row] as! EventRealm)
                EventRealm().deleteEvent(event.uid, completion: { (result) in
                    if result {
                        DispatchQueue.main.async {
                            alert.dismiss(animated: true, completion: nil)
                        }
                        EventRealm().removeEvents([event as Object], .create)
                    } else {
                        textfield.errorMessage = NSLocalizedString("CAUTION", comment: "")
                        textfield.shake()
                    }
                })
            } else {
                textfield.errorMessage = NSLocalizedString("CAUTION", comment: "")
                textfield.shake()
            }
        }
        alert.addButton(NSLocalizedString("CANCEL", comment: ""), textColor: .white, showDurationStatus: true) {
            alert.appearance.shouldAutoDismiss = true
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit(NSLocalizedString("DELETE_EVENT", comment: ""), subTitle: "")
    }
    
    
}


extension AUCViewController: LocationUpdateProtocol {
    
    func locationDidChangeAuthorization(status: Int) {}
    
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {
        if userLocation == nil {
            LocationService.shared.stopUpdatingLocation()
            userLocation = CLLocation(latitude: latitude, longitude: longitude)
        }
    }

    
}


protocol EventUserCreateCellDelegate {
    func chatTapped(_ indexPath: IndexPath)
    func modifyTapped(_ indexPath: IndexPath)
    func deleteTapped(_ indexPath: IndexPath)
    func dscpTapped(_ indexPath: IndexPath)
}

class EventCreateCell: UITableViewCell {
    
    var indexPath: IndexPath?
    
    @IBOutlet weak var sportIV: UIImageView!
    @IBOutlet weak var sportLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var nbDateLbl: UILabel!
    @IBOutlet weak var nbPlaceLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
            borderView.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            borderView.layer.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
            borderView.layer.shadowOffset = ThemeManager.currentTheme().shadowOffset
            borderView.layer.shadowRadius = ThemeManager.currentTheme().shadowRadius
        }
    }
    
    var delegate: EventUserCreateCellDelegate?
    
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        if let view = borderView {
            view.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
        }
    }
    
    @IBAction func didChatTapped(_ sender: Any) {
        delegate?.chatTapped(indexPath!)
    }
    
    @IBAction func didEditTapped(_ sender: Any) {
        delegate?.modifyTapped(indexPath!)
    }
    
    @IBAction func didDeleteTapped(_ sender: Any) {
        delegate?.deleteTapped(indexPath!)
    }
    
    @IBAction func didSelectCell(_ sender: Any) {
        delegate?.dscpTapped(indexPath!)
    }
    
}
