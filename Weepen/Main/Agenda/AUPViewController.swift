//
//  AgendaUserParticipateViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 15/10/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import ListPlaceholder
import Firebase
import RealmSwift
import GradientLoadingBar
import ListPlaceholder
import MapKit
import FirebaseFirestore
import SkyFloatingLabelTextField


protocol AUPViewControllerDelegate {
    func chatTappedP(_ indexPath: IndexPath)
    func emptyTappedP()
}

class AUPViewController: UIViewController {
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topCst: NSLayoutConstraint!
    
    private var gradientLoadingBar: GradientLoadingBar?
    var nbEmptyCell = 0
    var userLocation: CLLocation?
    var delegate: AUPViewControllerDelegate?
    var count = 0
    var token: NotificationToken?
    
    let distanceFormatter = MKDistanceFormatter()
    let refreshControl = UIRefreshControl()

    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set(true, forKey: "isParticipateVC")
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        EventRealm().getNBParticipantUpdate(.participate)
        self.token = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isParticipate = true").observe({ (result) in
            self.tableView.applyChanges(changes: result, .participate)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate = true").count == 0 {
                self.revealEmptyView()
            }
            else {
                self.tableView.hideLoader()
                self.revealTableView()
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupLocationDelegate()
        setupGradientLoader()
        setupRefreshControl()
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true").count == 0 { startLoadingEvent() }
        getEventsParticipate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(false, forKey: "isParticipateVC")
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        LocationService.shared.stopUpdatingLocation()
        self.token?.invalidate()
    }
    
    func setupLocationDelegate() {
        LocationService.shared.delegate = self
        LocationService.shared.startUpdatingLocation()
    }
    
    func setupTableView() {
        tableView.estimatedRowHeight = UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136 ? 140 : 210
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        tableView.separatorColor = .clear
    }
    
    func setupGradientLoader() {
        gradientLoadingBar = GradientLoadingBar(height: 3.5, gradientColorList: [UIColor.hexColor(0xA848D3), UIColor.hexColor(0xF12711), UIColor.hexColor(0xEC008C), UIColor.hexColor(0xF25D6C), UIColor.hexColor(0xFC834F), UIColor.hexColor(0xF5AF19)], onView: self.view)
        gradientLoadingBar?.gradientView.alpha = 0
    }
    
    func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        refreshControl.tintColor = ThemeManager.currentColor().mainColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("DOWNLOAD_EVENT", comment: ""), attributes: nil)
    }
    
    @objc func appMovedToBackground() {
        token?.invalidate()
    }
    
    @objc func appMovedToForeground() {
        refreshData(nil)
        self.token = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isDisplay = true").observe({ (result) in
            self.tableView.applyChanges(changes: result, .participate)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate = true").count == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }

    @objc private func refreshData(_ sender: Any?) {
        getEventsParticipate()
    }
    
    @IBAction func didEmptyTapped(_ sender: UIButton) {
        delegate?.emptyTappedP()
    }
    
    func getMainDate(_ item: EventRealm) -> (String, String, String, String) {
        let date: String = item.dateStart!.toString(dateFormat: "d MMM HH mm")
        let dateArray = date.components(separatedBy: " ")
        return (dateArray[0], dateArray[1], dateArray[2], dateArray[3])
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navController = segue.destination as? NavigationParticipantViewController, let vc = navController.topViewController as? DescriptionViewController else { return }
        
        guard let eventIndex = (sender as? IndexPath)?.row else { return }
        
        if let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isParticipate == true")[eventIndex] as? EventRealm {
            vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            vc.navigationItem.leftItemsSupplementBackButton = true
            vc.event = item
            vc.eventType = .participate
            vc.userLocation = LocationService.shared.currentLocation
        }
        
    }
    
}

extension AUPViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nbEmptyCell != 0 || CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true").count == 0 {
            return nbEmptyCell
        } else {
            return CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true").count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if nbEmptyCell != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventParticipateCell", for: indexPath) as! EventParticipateCell
            cell.delegate = self
            let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isParticipate == true")[indexPath.row] as! EventRealm
            let (nbDate, month, hour, min) = getMainDate(item)
            cell.nbDateLbl.text = nbDate
            cell.monthLbl.text = month
            cell.hourLbl.text = "\(hour):\(min)"
            cell.titleLbl.text = item.title?.capitalized
            if let sport = item.sport {
                cell.sportIV.image = UIImage(data: sport.logo!)
                cell.sportLbl.textColor = UIColor.hexColor(UInt(sport.codeColor.value!))
                cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "").uppercased()
            }
            cell.priceLbl.text = item.price.value == 0 ? NSLocalizedString("FREE", comment: "") : "\(item.price.value!) \(item.currency!)"
            if item.nbParticipant.value == nil { cell.nbPlaceLbl.text = "•••" }
            else { cell.nbPlaceLbl.text = "\(item.nbMaxParticipant.value!-item.nbParticipant.value!)" }
            cell.distanceLbl.text = "\(LocationService.shared.getLocation(item.latitude.value!, item.longitude.value!)) \(NSLocalizedString("ACTUAL_POSITION", comment: ""))"
            cell.indexPath = indexPath
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136 ? 140 : 210
    }

    
}


extension AUPViewController {
    
    func startLoadingEvent() {
        nbEmptyCell = 4
        gradientLoadingBar?.show()
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        tableView.reloadData()
        tableView.showLoader(ThemeManager.currentTheme().downView)
    }
    
    func endLoadingEvent() {
        tableView.hideLoader()
    }
    
    func getEventsParticipate() {
        gradientLoadingBar?.show()
        refreshControl.endRefreshing()
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        var events : [EventRealm] = []
        let dataChangedCallback : EventRealm.getDocumentsUserParticipateCallback = { (result: Bool, documents: [QueryDocumentSnapshot]?) -> () in
            if result {
                for document in documents! {
                    let dataChangedCallback2 : EventRealm.getEventUserParticipateCallback = { (result: Bool, event: EventRealm?) ->() in
                        if result {
                            if event!.owner?.uuid != (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid! {
                                events.append(event!)
                            }
                        }
                        self.count += 1
                        if self.count == documents!.count && events.count != 0 {
                            self.count = 0
                            events = events.sorted(by: { (lhsData, rhsData) -> Bool in
                                return Int((lhsData.dateStart?.timeIntervalSince1970)!) < Int((rhsData.dateStart?.timeIntervalSince1970)!)
                            })
                            let (removeEvents, updateEvents, addEvents) = EventRealm().orderEventsParticipant(events)
                            EventRealm().removeEvents(removeEvents, .participate)
                            self.removeEmptyCell()
                            EventRealm().updateEvents(updateEvents, .participate)
                            EventRealm().addEvents(addEvents, .participate)
                            EventRealm().getNBParticipantUpdate(.participate)
                            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate = true").count == 0 { self.revealEmptyView() }
                            else { self.revealTableView() }
                        }
                        UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                            self.gradientLoadingBar?.gradientView.alpha = 0
                        }, completion: { (result) in
                            if result { self.gradientLoadingBar?.hide() }
                        })
                        self.endLoadingEvent()
                    }
                    EventRealm().getDocumentsUserParticipate(uidUser: (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!, document: document, dataChangedCallback2)
                }
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.gradientLoadingBar?.gradientView.alpha = 0
                }, completion: { (result) in
                    if result { self.gradientLoadingBar?.hide() }
                })
                self.endLoadingEvent()
            }
        }
        EventRealm().getAllDocumentsUserParticipate(dataChangedCallback)
    }
    
    func revealEmptyView() {
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true").count == 0 {
            topCst.constant = 500
            UIView.animate(withDuration: 0.3) {
                self.emptyView.alpha = 1
                self.tableView.alpha = 0
            }
        }
    }
    
    func revealTableView() {
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate == true").count >= 0 {
            topCst.constant = 8
            UIView.animate(withDuration: 0.1) {
                self.emptyView.alpha = 0
                self.tableView.alpha = 1
            }
        }
    }
    
    func removeEmptyCell() {
        if nbEmptyCell != 0 {
            var indexes = [IndexPath]()
            for index in 0...nbEmptyCell-1 {
                indexes.append(IndexPath(row: index, section: 0))
            }
            nbEmptyCell = 0
            tableView.deleteRows(at: indexes, with: .fade)
        }
    }
    
    
}


extension AUPViewController: UIScrollViewDelegate {
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    
}


extension AUPViewController: EventUserParticipateCellDelegate {
    
    func dscpTapped(_ indexPath: IndexPath) {
        performSegue(withIdentifier: "pushToDescpP", sender: indexPath)
    }
    
    func chatTapped(_ indexPath: IndexPath) {
        delegate?.chatTappedP(indexPath)
    }
    
    func cancelTapped(_ indexPath: IndexPath) {
        gradientLoadingBar?.show()
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        let item = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isParticipate == true")[indexPath.row] as! EventRealm
        ParticipantRealm().removeParticipant(item.uid, (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!, completion: { (result) in
            if result {
                UserDefaults.standard.set(true, forKey: "isNeedRefreshList")
                EventRealm().removeEvents([item], .participate)
                self.tableView.deleteRows(at: [indexPath], with: .fade)
                self.tableView.reloadSections(IndexSet(integer: 0), with: .fade)
                self.revealEmptyView()
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.gradientLoadingBar?.gradientView.alpha = 0
                }, completion: { (result) in
                    if result { self.gradientLoadingBar?.hide() }
                })
            }
        })
    }
    
    
}


extension AUPViewController: LocationUpdateProtocol {
    
    func locationDidChangeAuthorization(status: Int) {}
    
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {
        if userLocation == nil {
            LocationService.shared.stopUpdatingLocation()
            userLocation = CLLocation(latitude: latitude, longitude: longitude)
        }
    }

    
}


protocol EventUserParticipateCellDelegate {
    func chatTapped(_ indexPath: IndexPath)
    func cancelTapped(_ indexPath: IndexPath)
    func dscpTapped(_ indexPath: IndexPath)
}

class EventParticipateCell: UITableViewCell {
    
    var indexPath: IndexPath?
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var sportIV: UIImageView!
    @IBOutlet weak var sportLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var nbDateLbl: UILabel!
    @IBOutlet weak var nbPlaceLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
            borderView.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            borderView.layer.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
            borderView.layer.shadowOffset = ThemeManager.currentTheme().shadowOffset
            borderView.layer.shadowRadius = ThemeManager.currentTheme().shadowRadius
        }
    }
    
    
    var delegate: EventUserParticipateCellDelegate?
    
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        if let view = borderView {
            view.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
        }
    }
    
    @IBAction func didChatTapped(_ sender: Any) {
        delegate?.chatTapped(indexPath!)
    }
    
    @IBAction func didDeleteTapped(_ sender: Any) {
        delegate?.cancelTapped(indexPath!)
    }
    
    @IBAction func didSelectTapped(_ sender: Any) {
        delegate?.dscpTapped(indexPath!)
    }
    
}
