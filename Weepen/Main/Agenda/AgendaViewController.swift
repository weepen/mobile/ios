//
//  AgendaViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/14/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import SwipeMenuViewController
import GradientLoadingBar
import Ambience


class AgendaViewController: SwipeMenuViewController {

    private var datas: [String] = [NSLocalizedString("EVENT_SUB", comment: ""), NSLocalizedString("EVENT_CREATED", comment: "")]
    private var vcs: [UIViewController] = [AUPViewController(), AUCViewController()]
    var options = SwipeMenuViewOptions()

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)


        options.tabView.additionView.backgroundColor = ThemeManager.currentTheme().upperView
        options.tabView.additionView.shadowColor = ThemeManager.currentTheme().shadowColor
        options.tabView.additionView.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
        options.tabView.additionView.shadowRadius = ThemeManager.currentTheme().shadowRadius
        options.tabView.additionView.shadowOffset = ThemeManager.currentTheme().shadowOffset
        options.tabView.height = 70
        options.tabView.style = .segmented
        options.tabView.addition = .circle
        options.tabView.additionView.padding = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)

        options.tabView.itemView.font = UIFont(name: ThemeManager.currentTheme().fontNameBold, size: 16)!
        options.tabView.itemView.textColor = ThemeManager.currentTheme().unselectedFontColor
        options.tabView.itemView.selectedTextColor = ThemeManager.currentTheme().selectedFontColor
        swipeMenuView.reloadData(options: options)
        swipeMenuView.jump(to: UserDefaults.standard.integer(forKey: "pageIndexAgenda"), animated: false)


    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let userParticipateVC = storyboard?.instantiateViewController(withIdentifier: "AUPViewController") as! AUPViewController
        userParticipateVC.delegate = self
        let userCreateVC = storyboard?.instantiateViewController(withIdentifier: "AUCViewController") as! AUCViewController
        userCreateVC.delegate = self
        self.addChild(userParticipateVC)
        self.addChild(userCreateVC)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)

    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {}
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        UserDefaults.standard.set(toIndex, forKey: "pageIndexAgenda")
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    
    //MARK - SwipeMenuViewDataSource
    override func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return datas.count
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return datas[index]
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        if children.count > 0 {
            let vc = children[index]
            vc.didMove(toParent: self)
            return vc
        }
        return UIViewController()
    }

}

extension AgendaViewController: AUPViewControllerDelegate {
 
    func chatTappedP(_ indexPath: IndexPath) {
        let vc = ChatViewController(user: (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm), channel: Channel(name: (CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart")[indexPath.row] as! EventRealm).title!, id: (CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isParticipate == true")[indexPath.row] as! EventRealm).uid, event: (CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isParticipate == true")[indexPath.row] as! EventRealm)))
            navigationController?.pushViewController(vc, animated: true)
    }
    
    func emptyTappedP() {
        if let tabBarController = self.tabBarController as? MainTabBarViewController {
            tabBarController.selectedIndex = 0
        }
    }
    
    
}

extension AgendaViewController: AUCViewControllerDelegate {
    
    func chatTappedC(_ indexPath: IndexPath) {
        let vc = ChatViewController(user: (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm), channel: Channel(name: (CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[indexPath.row] as! EventRealm).title!, id: (CacheService.shared.getDataFromDB(object:  EventRealm(), "dateStart").filter("isCreate == true")[indexPath.row] as! EventRealm).uid, event: (CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isCreate == true")[indexPath.row] as! EventRealm)))
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func emptyTappedC() {
        let storyboard = UIStoryboard(name: "CreateEvent", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditEvent") as! CreateViewController
        self.present(vc, animated: true, completion: nil)
//        if let tabBarController = self.tabBarController as? MainTabBarViewController {
//            tabBarController.selectedIndex = 1
//        }
    }
    
    
}
