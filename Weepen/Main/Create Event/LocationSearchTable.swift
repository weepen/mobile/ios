//
//  LocationSearchTable.swift
//  Weepen
//
//  Created by Louis Cheminant on 29/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import MapKit

class LocationSearchTable: UITableViewController {

    weak var handleMapSearchDelegate: HandleMapSearch?
    var matchingItems: [MKMapItem] = []
    var mapView: MKMapView?
    
    var searchCompleter = MKLocalSearchCompleter()
    var searchResults = [MKLocalSearchCompletion]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchCompleter.delegate = self
        tableView.backgroundColor = ThemeManager.currentTheme().downView
    }
    
    
}

extension LocationSearchTable: MKLocalSearchCompleterDelegate {
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        searchResults = completer.results
        self.tableView.reloadData()
    }
    
    func completer(_ completer: MKLocalSearchCompleter, didFailWithError error: Error) {
        print(error)
    }
}

extension LocationSearchTable : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchBarText = searchController.searchBar.text else { return }
        searchCompleter.queryFragment = searchBarText
    }
    
    
}

extension LocationSearchTable {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResults.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let searchResult = searchResults[indexPath.row]
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "locationCell")
        cell.textLabel?.text = searchResult.title
        cell.detailTextLabel?.text = searchResult.subtitle
        cell.contentView.backgroundColor = ThemeManager.currentTheme().upperView
        cell.textLabel?.textColor = ThemeManager.currentTheme().selectedFontColor
        cell.detailTextLabel?.textColor = ThemeManager.currentTheme().selectedFontColor
        cell.backgroundColor = ThemeManager.currentTheme().downView
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let completion = searchResults[indexPath.row]
        
        let searchRequest = MKLocalSearch.Request(completion: completion)
        let search = MKLocalSearch(request: searchRequest)
        search.start { (response, error) in
            self.handleMapSearchDelegate?.dropPinZoomIn(placemark: (response?.mapItems[0].placemark)!)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
}
