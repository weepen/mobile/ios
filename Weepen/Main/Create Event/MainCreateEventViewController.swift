//
//  MainCreateEventViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 8/24/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

class MainCreateEventViewController: UIViewController {
    
    @IBOutlet weak var ivCreateEvent: UIImageView!
    @IBOutlet weak var ivBookGym: UIImageView!
    @IBOutlet weak var previousBtn1: UIImageView!
    @IBOutlet weak var previousBtn2: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        ivCreateEvent.tintColor = ThemeManager.currentColor().mainColor
        ivBookGym.tintColor = ThemeManager.currentColor().gradientColor[1]
        previousBtn1.transform = previousBtn1.transform.rotated(by: CGFloat(Double.pi / 1))
        previousBtn2.transform = previousBtn2.transform.rotated(by: CGFloat(Double.pi / 1))
        previousBtn1.tintColor = ThemeManager.currentColor().mainColor
        previousBtn2.tintColor = ThemeManager.currentColor().gradientColor[1]
    }
    

    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        if !ambience { return .default }
        
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
        
    }
    
    @IBAction func backCE(segue: UIStoryboardSegue) {}

}
