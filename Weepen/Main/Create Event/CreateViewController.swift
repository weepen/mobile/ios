//
//  CreateViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 13/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import SwiftRangeSlider
import MapKit
import Ambience
import FirebaseFunctions


class CreateViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIVisualEffectView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var sliderParticipants: RangeSlider!
    @IBOutlet weak var descpTextView: UpperTextView!
    @IBOutlet weak var mainTitleLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var titleLbl: UITextField!
    @IBOutlet weak var isPrivateSwitch: UISwitch!
    @IBOutlet weak var ivLocation: UIImageView!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var addStartDateIv: UIImageView!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var addEndDateIv: UIImageView!
    @IBOutlet weak var addEndDateBtn: UIButton!
    @IBOutlet weak var addStartDateBtn: UIButton!
    @IBOutlet var heighLocationViewNul: NSLayoutConstraint!
    @IBOutlet var heighLocationView: NSLayoutConstraint!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var heightChooseLocation: NSLayoutConstraint!
    @IBOutlet weak var chooseLocationView: UIView!
    @IBOutlet weak var submitBtn: UpperButton!
    @IBOutlet weak var changeLocationBtn: UIVisualEffectView!
    @IBOutlet weak var titleLocationLbl: UpperLabel!
    
    var nameSport = [String]()
    var event = EventRealm()
    var isModify = false
    var isModifyPlace = false
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    var location = NSLocalizedString("ADD_LOCATION", comment: "")
    var placemark: MKPlacemark?
    var indexCellSelected = 0
    var popStartDatePicker: PopDatePicker?
    var popEndDatePicker : PopDatePicker?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeLocationBtn.layer.cornerRadius = changeLocationBtn.frame.height/2
        
        collectionView.contentInsetAdjustmentBehavior = .never
        collectionView.register(UINib(nibName: "SportCell", bundle: nil), forCellWithReuseIdentifier: "SportCell")
        
        backButton.layer.cornerRadius = backButton.frame.height/2
        
        popStartDatePicker = PopDatePicker(with: addStartDateBtn)
        popEndDatePicker = PopDatePicker(with: addEndDateBtn)
        
        ivLocation.transform = ivLocation.transform.rotated(by: CGFloat(Double.pi / 1))
        locationLbl.textColor = ThemeManager.currentTheme().placehorderColor
        
        descpTextView.addDoneButtonOnKeyboard()
        descpTextView.placeholder = NSLocalizedString("PLACEHOLDER_TEXTVIEW", comment: "")
        
        
        if isModify {
            if event.placeUID != nil {
                titleLocationLbl.text = NSLocalizedString("PLACES", comment: "")
            }
            changeLocationBtn.alpha = 1
            heightChooseLocation.constant = 0
            chooseLocationView.alpha = 0
            mainTitleLbl.text = NSLocalizedString("MODIFY_EVENT", comment: "")
            titleLbl.text = event.title
            descpTextView.text = event.resume
            isPrivateSwitch.isOn = event.isPrivate.value!
            
            for (index, sport) in CacheService.shared.getDataFromDB(object: SportRealm()).enumerated() {
                if (sport as! SportRealm).sportUUID == event.sport?.sportUUID {
                    collectionView.selectItem(at: IndexPath(item: index, section: 0), animated: true, scrollPosition: .centeredHorizontally)
                    indexCellSelected = index
                }
            }
            sliderParticipants.lowerValue = Double(exactly: event.nbMinParticipant.value!)!
            sliderParticipants.upperValue = Double(exactly: event.nbMaxParticipant.value!)!
            
            self.addStartDateIv.alpha = 0
            self.startDateLbl.text = event.dateStart!.toDatePickerString()!
            self.startDateLbl.alpha = 1
            
            self.addEndDateIv.alpha = 0
            self.endDateLbl.text = event.dateEnd!.toDatePickerString()!
            self.endDateLbl.alpha = 1
            
            geocode(latitude: event.latitude.value!, longitude: event.longitude.value!) { data, error in
                self.placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: self.event.latitude.value!, longitude: self.event.longitude.value!), addressDictionary: nil)
                self.finishLocation(location: self.placemark!, false)
            }
        } else {
            changeLocationBtn.alpha = 0
            heighLocationView.isActive = false
            
            locationView.alpha = 0
            event.isPrivate.value = false
            event.nbMinParticipant.value = 2
            event.nbMaxParticipant.value = 4
            
            locationLbl.text = location
        }
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    override func viewDidLayoutSubviews() {
        sliderParticipants.updateLayerFramesAndPositions()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? MainFindSHViewController {
            destination.delegate = self
            if let sportEvent = event.sport {
                destination.sportFilter = "sportUID:\(sportEvent.sportUUID ?? "")"
            }
        }
        if let destination = segue.destination as? CreateEventMapViewController {
            destination.delegate = self
            if placemark != nil {
                destination.selectedPin = placemark
            }
            
        }
    }
    
    @IBAction func backCEVC(segue: UIStoryboardSegue) {}
    
    func prepareDatePicker(_ datePicker: UIDatePicker, _ date: Date) {
        datePicker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: Date())!.aroundTime(15)
        datePicker.date = date.aroundTime(30)
    }
    
    func changeStartTitle(_ title: String) {
        guard addStartDateIv.alpha == 1 else {
            startDateLbl.text = title
            return
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.addStartDateIv.alpha = 0
        }) { (result) in
            if result {
                self.startDateLbl.text = title
                UIView.animate(withDuration: 0.1, animations: {
                    self.startDateLbl.alpha = 1
                })
            }
        }
    }
    
    func changeEndTitle(_ title: String) {
        guard addEndDateIv.alpha == 1 else {
            endDateLbl.text = title
            return
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.addEndDateIv.alpha = 0
        }) { (result) in
            if result {
                self.endDateLbl.text = title
                UIView.animate(withDuration: 0.1, animations: {
                    self.endDateLbl.alpha = 1
                })
            }
        }
    }
    
    func enabledValidateBtn() {
        guard
            event.title != nil,
            event.title != "",
            event.resume != nil,
            event.resume != "",
            event.dateStart != nil,
            event.dateEnd != nil,
            event.dateEnd!.timeIntervalSince(event.dateStart!) >= 900,
            locationLbl?.text != nil,
            (event.latitude.value != 0 && event.longitude.value != 0)
        else {
            self.submitBtn.isEnabled = false
            return
        }
        submitBtn.isEnabled = true
    }
    
    @IBAction func changeLocation(_ sender: Any) {
        isModifyPlace = true
        changeLocationBtn.alpha = 0
        event.dateStart = nil
        event.dateEnd = nil
        event.longitude.value = nil
        event.latitude.value = nil
        event.name = nil
        event.thoroughfare = nil
        event.city = nil
        event.country = nil
        
        heighLocationView.isActive = false
        heighLocationViewNul.isActive = true
        
        self.locationView.alpha = 0
        self.heightChooseLocation.constant = 106
        self.chooseLocationView.alpha = 1
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        enabledValidateBtn()
    }
    
}


extension CreateViewController {
    
    @IBAction func didCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didPrivateChanged(_ sender: UISwitch) {
        if isModify {
            try! CacheService.shared.getDatabase().write {
                event.isPrivate.value = sender.isOn
            }
        } else {
            event.isPrivate.value = sender.isOn
        }
    }
    
    @IBAction func didParticipantChanged(_ sender: RangeSlider) {
        if isModify {
            try! CacheService.shared.getDatabase().write {
                event.nbMinParticipant.value = Int(exactly: sender.lowerValue)
                event.nbMaxParticipant.value = Int(exactly: sender.upperValue)
            }
        } else {
            event.nbMinParticipant.value = Int(exactly: sender.lowerValue)
            event.nbMaxParticipant.value = Int(exactly: sender.upperValue)
        }
    }
    
    @IBAction func addStartDate(_ sender: UIButton) {
        if (sender === addStartDateBtn) {
            let initDate : Date? = event.dateStart
            
            let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forButton : UIButton) -> () in
                if self.isModify {
                    try! CacheService.shared.getDatabase().write {
                        self.event.dateStart = Calendar.current.date(bySetting: .second, value: 0, of: newDate)
                    }
                } else {
                    self.event.dateStart = Calendar.current.date(bySetting: .second, value: 0, of: newDate)
                }
                self.changeStartTitle(Calendar.current.date(bySetting: .second, value: 0, of: newDate)!.toDatePickerString()!)
                self.enabledValidateBtn()
            }
            popStartDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            if let _initDate = initDate {
                guard let picker = self.popStartDatePicker?.datePickerVC.datePicker, let date = Calendar.current.date(bySetting: .second, value: 0, of: _initDate), let now = Calendar.current.date(bySetting: .second, value: 0, of: Date()) else { return }
                picker.minimumDate = now.aroundTime(15)
                picker.date = date
                self.changeStartTitle(date.toDatePickerString()!)
            } else {
                guard let picker = self.popStartDatePicker?.datePickerVC.datePicker, let date = Calendar.current.date(bySetting: .second, value: 0, of: Date()) else { return }
                picker.minimumDate = date.aroundTime(15)
                picker.date = date.aroundTime(30)
                self.changeStartTitle(date.aroundTime(30).toDatePickerString()!)
            }
        }
    }
    
    @IBAction func addEndDate(_ sender: UIButton) {
        if (sender === addEndDateBtn) {
            let initDate : Date? = event.dateEnd
            
            let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forButton : UIButton) -> () in
                if self.isModify {
                    try! CacheService.shared.getDatabase().write {
                        self.event.dateEnd = Calendar.current.date(bySetting: .second, value: 0, of: newDate)
                    }
                } else {
                    self.event.dateEnd = Calendar.current.date(bySetting: .second, value: 0, of: newDate)
                }
                self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: newDate)!.toDatePickerString()!)
                self.enabledValidateBtn()
            }
            popEndDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            if let _initDate = initDate {
                guard let picker = self.popEndDatePicker?.datePickerVC.datePicker else { return }
                
                if let startDate = self.event.dateStart {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(15))!
                    picker.date = _initDate
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: _initDate)!.toDatePickerString()!)
                } else {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(30))!
                    picker.date = Calendar.current.date(bySetting: .second, value: 0, of: _initDate)!
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: _initDate)!.toDatePickerString()!)
                }
            } else {
                guard let picker = self.popEndDatePicker?.datePickerVC.datePicker else { return }
                if let startDate = self.event.dateStart {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(15))!
                    picker.date = Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(60))!
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(60))!.toDatePickerString()!)
                } else {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(30))!
                    picker.date = Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(90))!
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(90))!.toDatePickerString()!)
                }
            }
        }
    }
    
    @IBAction func didCreateEvent(_ sender: UIButton) {
        
        guard let title = event.title, title.count >= 2, title.count <= 40 else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_TITLE", comment: "")); return }
        guard let descp = event.resume, descp.count > 14 else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_RESUME", comment: "")); return }
        guard let sport = event.sport?.sportUUID else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_SPORT", comment: "")); return }
        guard let minP = event.nbMinParticipant.value, minP > 0 else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: "Choisir au minimun 2 participants"); return }
        guard let maxP = event.nbMaxParticipant.value, maxP > minP else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_MAX_PART", comment: "")); return }
        guard let startDate = event.dateStart else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_START_DATE", comment: "")); return }
        guard let endDate = event.dateEnd, event.dateEnd!.timeIntervalSince(event.dateStart!) >= 900 else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_END_DATE", comment: "")); return }
        guard let _ = locationLbl.text, (event.latitude.value != 0 && event.longitude.value != 0) else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_COORDINATE", comment: "")); return }
        guard let uuidOwner = user.uuid else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_ERROR", comment: "")); return }
        
        guard let latitude = event.latitude.value else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_ERROR", comment: "")); return }
        guard let longitude = event.longitude.value else { showAlert(title: NSLocalizedString("CAUTION", comment: ""), message: NSLocalizedString("CAUTION_ERROR", comment: "")); return }
        
        self.view.addSubview(LoadActivityIndicatorService.sharedLAI.addActivityIndicator(self.view.frame, isProgess: true))
        
        if isModify {
            modifyEvent(uid: event.uid, title, descp, sport, minP, maxP, startDate, endDate, uuidOwner, latitude, longitude)
        } else {
            addEvent(title, descp, sport, minP, maxP, startDate, endDate, uuidOwner, latitude, longitude)
        }
        
        
    }
    
    func modifyEvent(uid: String, _ title: String, _ descp: String, _ sport: String, _ minP: Int, _ maxP: Int, _ startDate: Date, _ endDate: Date, _ uuidOwner: String, _ latitude: Double, _ longitude: Double) {
        let locale = Locale.current
        if let currencyCode = locale.currencyCode {
            if self.isModify {
                try! CacheService.shared.getDatabase().write {
                    event.currency = currencyCode
                }
            } else {
                event.currency = currencyCode
            }
        }
        
        
        EventRealm().updateEvent(uid: uid, description: descp, endDate: endDate, latitude: latitude, longitude: longitude, maxParticipants: maxP, minParticipants: minP, owner: uuidOwner, isPrivate: event.isPrivate.value!, price: 0, currency: event.currency ?? "", sponsored: false, sport: sport, startDate: startDate, title: title, city: event.city ?? "", country: event.country ?? "", name: event.name ?? "", zip: event.zip ?? "", thoroughfare: event.thoroughfare ?? "", placeUid: event.placeUID ?? "") { (result) in
            if result {
                if self.event.placeUID != "" && self.isModifyPlace {
                    self.bookSH(self.event.uid)
                } else {
                    self.push("Agenda", "AgendaView")
                }
            }
        }
    }
    
    func addEvent(_ title: String, _ descp: String, _ sport: String, _ minP: Int, _ maxP: Int, _ startDate: Date, _ endDate: Date, _ uuidOwner: String, _ latitude: Double, _ longitude: Double) {
        let locale = Locale.current
        if let currencyCode = locale.currencyCode {
            event.currency = currencyCode
        }
        print(event.placeUID)
        
        
        EventRealm().setEvent(description: descp, endDate: endDate, latitude: latitude, longitude: longitude, maxParticipants: maxP, minParticipants: minP, owner: uuidOwner, isPrivate: event.isPrivate.value!, price: 0, currency: event.currency ?? "", sponsored: false, sport: sport, startDate: startDate, title: title, city: event.city ?? "", country: event.country ?? "", name: event.name ?? "", zip: event.zip ?? "", thoroughfare: event.thoroughfare ?? "", placeUid: event.placeUID ?? "") { (result, uid)  in
            if self.event.placeUID != nil {
                self.bookSH(uid!)
            } else {
                self.push("Agenda", "AgendaView")
            }
        }
    }
    
    func bookSH(_ uid: String) {
        let start = event.dateStart!.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
        let end = event.dateEnd!.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
        let eventUID = uid
        let title = event.title!
        let resume = event.resume!
        let placeUID = event.placeUID!
        
        Functions.functions().httpsCallable("booking").call([
            "linkedTo": [
                "type": "event",
                "id": eventUID,
            ],
            "placeId": placeUID,
            "wish": [
                "title": title,
                "description": resume,
                "start":  start,
                "end": end,
            ]
        ]) { (result, error) in
            LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
            if let _ = error as NSError? {
                self.showAlertSH(NSLocalizedString("ERROR_SH_TITLE", comment: ""), NSLocalizedString("ERROR_SH_SUBTITLES", comment: ""), true, buttonTitles: [NSLocalizedString("CLOSE", comment: "")])
            } else if let _ = result?.data as? [String: Any] {
                self.showAlertSH(NSLocalizedString("BUSY_SH_TITLE", comment: ""), NSLocalizedString("BUSY_SH_SUBTITLES", comment: ""), true, buttonTitles: [NSLocalizedString("CLOSE", comment: "")])
            } else {
                self.push("Agenda", "AgendaView")
            }
        }
    }
    
    func showAlertSH(_ title: String, _ subtitle: String, _ isError: Bool, buttonTitles: [String]) {
        let alert: AlertView = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
        if isError { alert.showError(title, subTitle: subtitle) }
        else { alert.showSuccess(title, subTitle: subtitle) }
        for title in buttonTitles {
            alert.addButton(title) {
                alert.appearance.shouldAutoDismiss = true
                if title == NSLocalizedString("COMPLETE", comment: "") {
                } else {
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    func push(_ storyboard: String, _ controllerID: String) {
        DispatchQueue.main.async {
            UserDefaults.standard.set(true, forKey: "isNeedRefreshList")
            UserDefaults.standard.set(true, forKey: "isNeedRefreshAgenda")
            UserDefaults.standard.set(1, forKey: "pageIndexAgenda")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tab = storyboard.instantiateViewController(withIdentifier: "MainTabController") as! MainTabBarViewController
            tab.selectedIndex = 1
            self.present(tab, animated: true)
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width)).showError(title, subTitle: message)
        alert.alertview.addButton(NSLocalizedString("CLOSE", comment: ""), textColor: .white, showDurationStatus: true) {
            alert.alertview.dismiss(animated: true, completion: nil)
            alert.close()
        }
    }
    
    
}


extension CreateViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CacheService.shared.getDataFromDB(object: SportRealm()).count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        var firstCell: SportCell!
        if isModify {
            if let cell = collectionView.cellForItem(at: IndexPath(item: indexPath.row, section: 0)) as? SportCell {
                firstCell = cell
            }
        } else {
            if let cell = collectionView.cellForItem(at: IndexPath(item: indexCellSelected, section: 0)) as? SportCell {
                firstCell = cell
            }
        }
        if firstCell != nil {
            if firstCell.isSelected { firstCell.isSelected = false }
        }
        if self.isModify {
            try! CacheService.shared.getDatabase().write {
                event.sport = (CacheService.shared.getDataFromDB(object: SportRealm())[indexPath.row] as! SportRealm)
            }
        } else {
            event.sport = (CacheService.shared.getDataFromDB(object: SportRealm())[indexPath.row] as! SportRealm)
        }
        cell.isSelected = true
        indexCellSelected = (collectionView.indexPath(for: cell)?.row)!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height-32, height: collectionView.frame.height-32)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportCell", for: indexPath) as! SportCell
        let sports = CacheService.shared.getDataFromDB(object: SportRealm())
        guard case let sport as SportRealm = sports[indexPath.row] else {
            print("***SPORT CREATE EVENT ERROR***")
            return cell
        }
        cell.sportIV.image = UIImage(data: sport.logo!)
        cell.sportIV.addBlackFilter()
        cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "")
        cell.sportUUID = sport.sportUUID
        cell.isSelected = false
        if indexPath.row == indexCellSelected {
            cell.isSelected = true
            if self.isModify {
                try! CacheService.shared.getDatabase().write {
                    event.sport = sport
                }
            } else {
                event.sport = sport
            }
        } else {
            cell.isSelected = false
        }
        return cell
    }
    
    
}

extension CreateViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}

extension CreateViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        descpTextView.becomeFirstResponder()
        enabledValidateBtn()
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.isModify {
            try! CacheService.shared.getDatabase().write {
                event.title = textField.text
                enabledValidateBtn()
            }
        } else {
            event.title = textField.text
            enabledValidateBtn()
        }
    }
}

extension CreateViewController: UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        if self.isModify {
            try! CacheService.shared.getDatabase().write {
                event.resume = textView.text
                enabledValidateBtn()
            }
        } else {
            event.resume = textView.text
            enabledValidateBtn()
        }
    }
}

extension CreateViewController: BookSHViewDelegate {
    func finishBooked(name: String, zip: String, thoroughfare: String, city: String, country: String, latitude: Double, longitude: Double, startDate: Date, endDate: Date, placeUid: String, _ update: Bool) {
        titleLocationLbl.text = NSLocalizedString("PLACES", comment: "")
        changeLocationBtn.alpha = 1
        
        event.name = name
        event.zip = zip
        event.city = city
        event.country = country
        event.thoroughfare = thoroughfare
        event.latitude.value = latitude
        event.longitude.value = longitude
        event.dateStart = startDate
        event.dateEnd = endDate
        event.placeUID = placeUid
        
        heighLocationView.isActive = true
        heighLocationViewNul.isActive = false
        
        self.locationView.alpha = 1
        self.heightChooseLocation.constant = 0
        self.chooseLocationView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        locationLbl.text = "\(event.name ?? "")\n\(event.thoroughfare ?? "")\n\(event.zip ?? "") \(event.city ?? "")\n\(event.country ?? "")"
        locationLbl.textColor = ThemeManager.currentTheme().selectedFontColor
        
        self.addStartDateIv.alpha = 0
        self.startDateLbl.text = event.dateStart!.toDatePickerString()!
        self.startDateLbl.alpha = 1
        
        self.addEndDateIv.alpha = 0
        self.endDateLbl.text = event.dateEnd!.toDatePickerString()!
        self.endDateLbl.alpha = 1
        
        addEndDateBtn.isEnabled = false
        addStartDateBtn.isEnabled = false
        self.enabledValidateBtn()
        
    }
    
    
}

extension CreateViewController: LocationViewDelegate {
    func finishLocation(location: MKPlacemark, _ update: Bool) {
        titleLocationLbl.text = NSLocalizedString("LOCATION", comment: "")
        changeLocationBtn.alpha = 1
        
        heighLocationView.isActive = true
        heighLocationViewNul.isActive = false
        
        self.locationView.alpha = 1
        self.heightChooseLocation.constant = 0
        self.chooseLocationView.alpha = 0
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        
        placemark = location
        
        guard let _placemark = placemark else { return }
        if let _zip = _placemark.postalCode { event.zip = _zip }
        if let _city = _placemark.locality { event.city = _city }
        if let _country = _placemark.country { event.country = _country }
        if let thoroughfare = _placemark.thoroughfare {
            if let subThoroughfare = _placemark.subThoroughfare { event.thoroughfare = "\(subThoroughfare) \(thoroughfare)" }
            else { event.thoroughfare = thoroughfare }
        }
        if let name = _placemark.name {
            event.name = name
            if let zip = event.zip {
                locationLbl.text = "\(name)\n\(zip), \(event.city ?? "")\n\(event.country ?? "")"
            } else {
                locationLbl.text = "\(name)\n\(event.city ?? "")\n\(event.country ?? "")"
            }
            
        } else {
            locationLbl.text = "\(event.thoroughfare ?? "")\n\(event.zip ?? "") \(event.city ?? "")\n\(event.country ?? "")"
        }
        locationLbl.textColor = ThemeManager.currentTheme().selectedFontColor
        if update {
            if self.isModify {
                try! CacheService.shared.getDatabase().write {
                    event.latitude.value = _placemark.coordinate.latitude
                    event.longitude.value = _placemark.coordinate.longitude
                }
            } else {
                event.latitude.value = _placemark.coordinate.latitude
                event.longitude.value = _placemark.coordinate.longitude
            }
        }
        self.enabledValidateBtn()
    }
}

extension Date {
    func aroundTime(_ time: Int) -> Date {
        let minuteGranuity = 15
        let times = time/15
        
        let hour = Calendar.current.component(.hour, from: self)
        let minute = Calendar.current.component(.minute, from: self)
        
        let floorMinute = minute - (minute % minuteGranuity)
        let floorDate = Calendar.current.date(bySettingHour: hour, minute: floorMinute, second: 0, of: self)!
        
        var dates: [Date] = []
        for minutes in stride(from: minuteGranuity, through: minuteGranuity*times, by: minuteGranuity) {
            let newDate = Calendar.current.date(byAdding: .minute, value: minutes, to: floorDate)!
            dates.append(newDate)
        }
        return dates.last!
    }
}
