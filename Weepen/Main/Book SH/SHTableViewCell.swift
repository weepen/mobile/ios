//
//  SHTableViewCell.swift
//  Weepen
//
//  Created by Louis Cheminant on 12/11/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience


protocol SHCellDelegate {
    func dscpTapped(_ indexPath: IndexPath)
}


class SHTableViewCell: UITableViewCell {
    
    var indexPath: IndexPath?
    
    @IBOutlet weak var arrowIV: UIImageView! {
        didSet {
            arrowIV.transform = arrowIV.transform.rotated(by: CGFloat(Double.pi / 1))
            arrowIV.tintColor = ThemeManager.currentColor().mainColor
        }
    }
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var sportIV: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var coverIV: UIImageView!
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
            borderView.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            borderView.layer.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
            borderView.layer.shadowOffset = ThemeManager.currentTheme().shadowOffset
            borderView.layer.shadowRadius = ThemeManager.currentTheme().shadowRadius
        }
    }
    
    var delegate: SHCellDelegate?

    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        if let view = borderView {
            view.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
        }
    }
    
    @IBAction func didSelect(_ sender: Any) {
        delegate?.dscpTapped(indexPath!)
    }
    
}
