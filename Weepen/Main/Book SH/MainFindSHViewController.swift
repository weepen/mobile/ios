//
//  MainFindSHViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 12/11/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import MapKit
import GradientLoadingBar
import RealmSwift

protocol BookSHViewDelegate {
    func finishBooked(name: String, zip: String, thoroughfare: String, city: String, country: String, latitude: Double, longitude: Double, startDate: Date, endDate: Date, placeUid: String, _ update: Bool)
}

class MainFindSHViewController: UIViewController {
    
    @IBOutlet weak var bgCancel: UIVisualEffectView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var filterBtn: UpperButton!
    @IBOutlet weak var bgSearchBarView: UIView!
    @IBOutlet weak var noEventView: UIView!
    
    let refreshControl = UIRefreshControl()

    var userLocation: CLLocation?
    var token: NotificationToken?
    var nbEmptyCell = 0
    var arrayFilter = [String]()
    var isLoaded = false
    var popRadiusSlider: PopRadiusSlider?
    private var gradientLoadingBar: BottomGradientLoadingBar?
    var delegate: BookSHViewDelegate?
    var sportFilter = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        self.token = CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid").observe({ (result) in
            self.tableView.applyPlaceChanges(changes: result)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isParticipate = true").count == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableView()
        setupLocationDelegate()
        setupGradientLoader()
        setupRefreshControl()
        scrollView.isScrollEnabled = false
        if CacheService.shared.getDataFromDB(object: SportHallRealm()).count == 0 { startLoadingEvent() }
        searchPlace("")
    }
    
    func setupView() {
        bgCancel.layer.cornerRadius = bgCancel.frame.height/2
        CacheService.shared.addData(object: FilterRealm(id: 2, radius: 50, isPrivate: false, isSponsored: false, minPrice: 0, maxPrice: 10, date: Date())!)
        arrayFilter.append("50 " + NSLocalizedString("DISTANCE", comment: ""))
    }
    
    func setupTableView() {
        tableView.estimatedRowHeight = 85
        tableView.rowHeight = UITableView.automaticDimension
        tableView.allowsSelection = false
        tableView.separatorColor = .clear
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupLocationDelegate() {
        LocationService.shared.delegate = self
        LocationService.shared.startUpdatingLocation()
    }
    func setupGradientLoader() {
        gradientLoadingBar = BottomGradientLoadingBar(height: 3.5, gradientColorList: [UIColor.hexColor(0xA848D3), UIColor.hexColor(0xF12711), UIColor.hexColor(0xEC008C), UIColor.hexColor(0xF25D6C), UIColor.hexColor(0xFC834F), UIColor.hexColor(0xF5AF19)], onView: bgSearchBarView)
        gradientLoadingBar?.gradientView.alpha = 0
    }
    
    func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        refreshControl.tintColor = ThemeManager.currentColor().mainColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("DOWNLOAD_PLACES", comment: ""), attributes: nil)
    }
    
    @objc func appMovedToBackground() {
        token?.invalidate()
    }
    
    @objc func appMovedToForeground() {
        refreshData(nil)
        self.token = CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid").observe({ (result) in
            self.tableView.applyPlaceChanges(changes: result)
            if CacheService.shared.getDataFromDB(object: SportHallRealm()).count == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }
    
    @objc private func refreshData(_ sender: Any?) {
        searchPlace(searchBar.text!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let vc = segue.destination as? DescriptionSHViewController else { return }
        guard let eventIndex = (sender as? IndexPath)?.row else { return }

        if let sportHall = CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid")[eventIndex] as? SportHallRealm {
            vc.sportHall = sportHall
            vc.delegate = self
        }
    }
    
    
}

// MARK: IBAction Function
extension MainFindSHViewController {
    @IBAction func changedMode(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y), animated: true)
        case 1:
            scrollView.setContentOffset(CGPoint(x: self.view.frame.width, y: scrollView.contentOffset.y), animated: true)
        default:
            break
        }
    }
    
    @IBAction func didFilters(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "FilterSHViewController") as! FilterSHViewController
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func backMSH(segue: UIStoryboardSegue) {}
    
    
}


// MARK: ViewController Getter
extension MainFindSHViewController {
    func getRadius() -> UInt {
        if let radius = (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).radius.value {
            return UInt(radius)
        }
        return UInt(50)
    }
    
    func getFilters() -> String {
        if getSportFilter() != "" {
            return getSportFilter()
        }
        return ""
    }
    
    func getSportFilter() -> String {
//        var sportFilter = ""
//        for (index, sport) in (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.enumerated() {
//            if index == 0 {
//                sportFilter = "sportUID:\(sport.sportUUID!)"
//            } else {
//                sportFilter = sportFilter + " OR sportUID:\(sport.sportUUID!)"
//            }
//        }
        return sportFilter
    }
    
    func getLatLng() -> (Double, Double, Bool) {
        guard let location = LocationService.shared.currentLocation else {
            return(0.0, 0.0, false)
        }
        return (Double(location.coordinate.latitude), Double(location.coordinate.longitude), true)
    }
    
    
}


// MARK: Events Cycle
extension MainFindSHViewController {
    func createPlaceArray(_ hits: [[String : Any]]) {
        var places = [SportHallRealm]()
        for hit in hits {
            if let place = SportHallRealm(dataFromAlgolia: hit) {
                places.append(place)
            }
        }
        let (removePlaces, updatePlaces, addPlaces) = SportHallRealm().orderPlaces(places)
        SportHallRealm().removePlaces(removePlaces)
        removeEmptyCell()
        SportHallRealm().updatePlaces(updatePlaces)
        SportHallRealm().addPlaces(addPlaces)
        if CacheService.shared.getDataFromDB(object: SportHallRealm()).count != self.tableView.numberOfRows(inSection: 0) { self.tableView.addMissRow(CacheService.shared.getDataFromDB(object: SportHallRealm()).count) }
        if CacheService.shared.getDataFromDB(object: SportHallRealm()).count == 0 { revealEmptyView() }
        else { revealTableView() }
        endLoadingEvent()
    }
    
    func searchPlace(_ text: String) {
        gradientLoadingBar?.show()
        isLoaded = true
        refreshControl.endRefreshing()
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        
        let search = text
        let filters = getFilters()
        let radius = text != "" ? 1000000000 : getRadius()
        let distance = getLatLng()
        if !distance.2 { return }
        
        AlgoliaService.shared.getEvent(index: AlgoliaService.shared.indexPlaces, search: search, filters: filters, radius: radius, latitude: distance.0, longitude: distance.1) { (result, nbPages, content) in
            if result {
                self.createPlaceArray(content)
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.gradientLoadingBar?.gradientView.alpha = 0
                }, completion: { (result) in
                    if result {
                        self.gradientLoadingBar?.hide()
                        self.isLoaded = false
                    }
                })
            } else {
                let alert: AlertView = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
                alert.showWarning("Une erreur s'est produite", subTitle: "Il semble que votre connexion soit lente")
                alert.addButton("Fermer") {
                    alert.appearance.shouldAutoDismiss = true
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    
}


// MARK: Loading Functions
extension MainFindSHViewController {
    func startLoadingEvent() {
        nbEmptyCell = 4
        tableView.reloadData()
        tableView.showLoader(ThemeManager.currentTheme().downView)
    }
    
    func endLoadingEvent() {
        tableView.hideLoader()
    }
    
    func revealEmptyView() {
        if CacheService.shared.getDataFromDB(object: SportHallRealm()).count == 0 {
            UIView.animate(withDuration: 0.3) {
                self.noEventView.alpha = 1
                self.tableView.alpha = 0
            }
        }
    }
    
    func revealTableView() {
        if CacheService.shared.getDataFromDB(object: SportHallRealm()).count >= 0 {
            UIView.animate(withDuration: 0.1) {
                self.noEventView.alpha = 0
                self.tableView.alpha = 1
            }
        }
    }
    
    func removeEmptyCell() {
        if nbEmptyCell != 0 {
            var indexes = [IndexPath]()
            for index in 0...nbEmptyCell-1 {
                indexes.append(IndexPath(row: index, section: 0))
            }
            nbEmptyCell = 0
            tableView.deleteRows(at: indexes, with: .fade)
        }
    }

    
}

extension MainFindSHViewController: VerifyBookSHViewDelegate {
    func finishVerifiedBook(name: String, zip: String, thoroughfare: String, city: String, country: String, latitude: Double, longitude: Double, startDate: Date, endDate: Date, placeUid: String, _ update: Bool) {
        delegate?.finishBooked(name: name, zip: zip, thoroughfare: thoroughfare, city: city, country: country, latitude: latitude, longitude: longitude, startDate: startDate, endDate: endDate, placeUid: placeUid, update)
    }
    
    
}


// MARK: TableView Delegate
extension MainFindSHViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nbEmptyCell != 0 {
            return nbEmptyCell
        } else {
            return CacheService.shared.getDataFromDB(object: SportHallRealm()).count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if nbEmptyCell != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SHCell", for: indexPath) as! SHTableViewCell
            cell.delegate = self
            let item = CacheService.shared.getDataFromDB(object: SportHallRealm(), "uid")[indexPath.row] as! SportHallRealm
            cell.titleLbl.text = item.name?.capitalized
            cell.distanceLbl.text = "\(LocationService.shared.getLocation(item.latitude.value!, item.longitude.value!))"
            if let cover = item.cover {
                cell.coverIV.image = UIImage(data: cover)
            } else {
                SportHallRealm().downloadCoverSH(item)
            }
            if let sport = item.sport {
                cell.sportIV.image = UIImage(data: sport.logo!)
            }
            cell.indexPath = indexPath
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    
}


// MARK: SearchBar Delegate
extension MainFindSHViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchBarText = searchBar.text {
            searchPlace(searchBarText.replacingCharacters(in: Range(range, in: searchBarText)!, with: text))
            return true
        }
        return false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchBarText = searchBar.text {
            if searchBarText == "" {
                searchBar.showsCancelButton = false
                self.view.endEditing(true)
                searchPlace(searchBarText)
            }
        }
    }
    
    
}


// MARK: CollectionView Delegate
extension MainFindSHViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm
        if arrayFilter[indexPath.row] == "" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportFilter", for: indexPath) as! FilterSportCell
            cell.bgView.backgroundColor = UIColor.hexColor(UInt((filter.sports[indexPath.row-1].codeColor.value)!))
            cell.removeBtn.image = UIImage(named: "Cancel")
            cell.removeBtn.image? = (cell.removeBtn.image?.withRenderingMode(.alwaysTemplate))!
            cell.removeBtn.tintColor = .white
            cell.sportPicture.image = UIImage(data: filter.sports[indexPath.row-1].logo!)
            cell.sportPicture.image? = (cell.sportPicture.image?.withRenderingMode(.alwaysTemplate))!
            cell.sportPicture.tintColor = .white
            cell.bgView.layer.cornerRadius = cell.bgView.frame.height/2
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterRadiusCell", for: indexPath) as! FilterRadiusCell
            popRadiusSlider = PopRadiusSlider(with: cell)
            cell.radiusLbl.text = arrayFilter[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if arrayFilter[indexPath.row] == "" {
            return CGSize(width: 50, height: collectionView.frame.height)
        } else {
            let size = arrayFilter[indexPath.row].size(withAttributes: [NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameRegular, size: 16)!])
            return CGSize(width: size.width+20, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        try! CacheService.shared.getDatabase().write({
            if arrayFilter[indexPath.row] == "" {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.remove(at: (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.index(of: ((CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports[indexPath.row-1] ))!)
            } else {
                let initRadius : Float? = Float((CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).radius.value!)
                
                let dataChangedCallback : PopRadiusSlider.PopRadiusSliderCallback = { (newRadius: Float, forCell: FilterRadiusCell, refresh: Bool) -> () in
                    if let cell = collectionView.cellForItem(at: indexPath) as? FilterRadiusCell {
                        cell.radiusLbl.font = Int(newRadius) > 100 ? cell.radiusLbl.font.withSize(14) : cell.radiusLbl.font.withSize(16)
                        cell.radiusLbl.text = "\(Int(newRadius)) " + NSLocalizedString("DISTANCE", comment: "")
                        if refresh {
                            self.arrayFilter[indexPath.row] = cell.radiusLbl.text!
                            try! CacheService.shared.getDatabase().write {
                                (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).radius.value = Int(newRadius)
                            }
                            self.searchPlace(self.searchBar.text!)
                        }
                    }
                }
                popRadiusSlider!.pick(self, initRadius: initRadius, dataChanged: dataChangedCallback)
                return
            }
            arrayFilter.remove(at: indexPath.row)
            collectionView.deleteItems(at: [indexPath])
            if arrayFilter.count == 1 {
                UIView.animate(withDuration: 0.5, animations: {
                    self.filterBtn.setTitle(NSLocalizedString("FILTERS", comment: ""), for: .normal)
                })
                self.view.layoutIfNeeded()
            }
            searchPlace(searchBar.text!)
        })
    }
    
    
}


// MARK: Location Delegate
extension MainFindSHViewController: LocationUpdateProtocol {
    func locationDidChangeAuthorization(status: Int) {}
    
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {
        if userLocation == nil {
            LocationService.shared.stopUpdatingLocation()
            userLocation = CLLocation(latitude: latitude, longitude: longitude)
        }
    }
    
    
}


// MARK: Filter Delegate
extension MainFindSHViewController: FilterSHDelegate {
    func filterSelected() {
        createArrayFilter()
        let text = arrayFilter.count > 1 ? NSLocalizedString("FILTERS", comment: "") + "•" + "\(arrayFilter.count-1)" : NSLocalizedString("FILTERS", comment: "")
        filterBtn.setTitle(text, for: .normal)
        filterCollectionView.reloadData()
        searchPlace(searchBar.text!)
    }
    
    func createArrayFilter() {
        arrayFilter = []
        let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm
        arrayFilter.append("\(filter.radius.value!) " + NSLocalizedString("DISTANCE", comment: ""))
        for _ in filter.sports {
            arrayFilter.append("")
        }
        if filter.minPrice.value! > 0 || filter.maxPrice.value! < 10 {
            arrayFilter.append("\(filter.minPrice.value!)-\(filter.maxPrice.value!)")
        }
    }
    
    
}


// MARK: SHCell Delegate
extension MainFindSHViewController: SHCellDelegate {
    func dscpTapped(_ indexPath: IndexPath) {
        performSegue(withIdentifier: "pushToDescpSH", sender: indexPath)
    }
    
    
}
