//
//  DescriptionSHViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 20/11/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import MapKit
import FirebaseFunctions

protocol VerifyBookSHViewDelegate {
    func finishVerifiedBook(name: String, zip: String, thoroughfare: String, city: String, country: String, latitude: Double, longitude: Double, startDate: Date, endDate: Date, placeUid: String, _ update: Bool)
}

class DescriptionSHViewController: UIViewController {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var coverIv: UIImageView!
    @IBOutlet weak var addressTitleLbl: UpperLabel!
    @IBOutlet weak var addressSubtitlesLbl: UpperLabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bgVev: UIVisualEffectView!
    @IBOutlet weak var reserveBtn: UpperButton!
    @IBOutlet weak var sportCV: UICollectionView!
    @IBOutlet weak var availabilityView: UIView!
    @IBOutlet weak var addStartDateBtn: UIButton!
    @IBOutlet weak var addEndDateBtn: UIButton!
    @IBOutlet weak var addStartDateIv: UIImageView!
    @IBOutlet weak var addStartDateLbl: UpperLabel!
    @IBOutlet weak var addEndDateIv: UIImageView!
    @IBOutlet weak var addEndDateLbl: UpperLabel!
    @IBOutlet weak var checkAvailabilityBtn: UpperButton!
    
    var sportHall = SportHallRealm()
    var isWithEvent = true
    var event = EventRealm()
    var popStartDatePicker: PopDatePicker?
    var popEndDatePicker : PopDatePicker?
    var startDateG: Date?
    var endDateG: Date?
    var delegate: VerifyBookSHViewDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        collectionViewSetup()
        
        fillView()
    }
    
    func setupView() {
        availabilityView.alpha = 0
        bgVev.layer.cornerRadius = bgVev.frame.height/2
        reserveBtn.setTitle(NSLocalizedString("BOOK", comment: ""), for: .normal)
        displayWay((LocationService.shared.currentLocation?.coordinate)!, CLLocationCoordinate2D(latitude: sportHall.latitude.value!, longitude: sportHall.longitude.value!))
        popStartDatePicker = PopDatePicker(with: addStartDateBtn)
        popEndDatePicker = PopDatePicker(with: addEndDateBtn)
    }
    
    func fillView() {
        titleLbl.text = sportHall.name
        coverIv.image = UIImage(data: sportHall.cover ?? Data())
        addressTitleLbl.text = sportHall.name! != "" ? "\(sportHall.name!)" : "\(sportHall.thoroughfare!)"
        addressSubtitlesLbl.text = "\(sportHall.zip!) \(sportHall.city!), \(sportHall.country!)"
        
        let pointAnnotation = MKPointAnnotation()
        pointAnnotation.title = sportHall.name! != "" ? "\(sportHall.name!)" : "\(sportHall.thoroughfare!)"
        pointAnnotation.subtitle = "\(sportHall.zip!) \(sportHall.city!), \(sportHall.country!)"
        pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(truncating: NSNumber(value: sportHall.latitude.value!)), longitude: CLLocationDegrees(truncating: NSNumber(value: sportHall.latitude.value!)))
        self.mapView.addAnnotation(pointAnnotation)
        self.mapView.selectAnnotation(pointAnnotation, animated: true)
    }
    
    func collectionViewSetup() {
        sportCV.contentInsetAdjustmentBehavior = .never
        sportCV.register(UINib(nibName: "SportCell", bundle: nil), forCellWithReuseIdentifier: "SportCell")
    }
    

}


// MARK: IBAction
extension DescriptionSHViewController {
    @IBAction func didReserve(_ sender: Any) {
        UIView.animate(withDuration: 0.3) {
            self.availabilityView.alpha = 1
        }
    }
    
    @IBAction func touchBg(_ sender: Any) {
        if self.availabilityView.alpha == 1 {
            UIView.animate(withDuration: 0.3) {
                self.availabilityView.alpha = 0
            }
        }
    }
    
    @IBAction func didCheckAvailability(_ sender: Any) {
        self.view.addSubview(LoadActivityIndicatorService.sharedLAI.addActivityIndicator(self.view.frame, isProgess: true))
        checkPlaces()
    }
    
    @IBAction func didAddStartDate(_ sender: UIButton) {
        if (sender === addStartDateBtn) {
            let initDate : Date? = startDateG
            
            let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forButton : UIButton) -> () in
                self.startDateG = Calendar.current.date(bySetting: .second, value: 0, of: newDate)
                self.changeStartTitle(Calendar.current.date(bySetting: .second, value: 0, of: newDate)!.toDatePickerString()!)
                if (self.startDateG != nil) && (self.endDateG != nil) {
                    self.checkAvailabilityBtn.isEnabled = true
                }
            }
            popStartDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            if let _initDate = initDate {
                guard let picker = self.popStartDatePicker?.datePickerVC.datePicker, let date = Calendar.current.date(bySetting: .second, value: 0, of: _initDate), let now = Calendar.current.date(bySetting: .second, value: 0, of: Date()) else { return }
                picker.minimumDate = now.aroundTime(15)
                picker.date = date
                self.changeStartTitle(date.toDatePickerString()!)
            } else {
                guard let picker = self.popStartDatePicker?.datePickerVC.datePicker, let date = Calendar.current.date(bySetting: .second, value: 0, of: Date()) else { return }
                picker.minimumDate = date.aroundTime(15)
                picker.date = date.aroundTime(30)
                self.changeStartTitle(date.aroundTime(30).toDatePickerString()!)
            }
        }
    }
    
    @IBAction func didEndStartDate(_ sender: UIButton) {
        if (sender === addEndDateBtn) {
            let initDate : Date? = endDateG
            
            let dataChangedCallback : PopDatePicker.PopDatePickerCallback = { (newDate : Date, forButton : UIButton) -> () in
                self.endDateG = Calendar.current.date(bySetting: .second, value: 0, of: newDate)
                self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: newDate)!.toDatePickerString()!)
                if (self.startDateG != nil) && (self.endDateG != nil) {
                    self.checkAvailabilityBtn.isEnabled = true
                }
            }
            popEndDatePicker!.pick(self, initDate: initDate, dataChanged: dataChangedCallback)
            if let _initDate = initDate {
                guard let picker = self.popEndDatePicker?.datePickerVC.datePicker else { return }
                
                if let startDate = startDateG {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(15))!
                    picker.date = _initDate
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: _initDate)!.toDatePickerString()!)
                } else {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(30))!
                    picker.date = Calendar.current.date(bySetting: .second, value: 0, of: _initDate)!
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: _initDate)!.toDatePickerString()!)
                }
            } else {
                guard let picker = self.popEndDatePicker?.datePickerVC.datePicker else { return }
                if let startDate = startDateG {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(15))!
                    picker.date = Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(60))!
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: startDate.aroundTime(60))!.toDatePickerString()!)
                } else {
                    picker.minimumDate = Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(30))!
                    picker.date = Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(90))!
                    self.changeEndTitle(Calendar.current.date(bySetting: .second, value: 0, of: Date().aroundTime(90))!.toDatePickerString()!)
                }
            }
        }
    }
    
    
}


// MARK: Functions View
extension DescriptionSHViewController {
    func displayWay(_ source: CLLocationCoordinate2D, _ destination: CLLocationCoordinate2D) {
        let sourcePlacemark = MKPlacemark(coordinate: source, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destination, addressDictionary: nil)
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = sportHall.name?.capitalizingFirstLetter()
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        self.mapView.showAnnotations([destinationAnnotation], animated: true )
        self.mapView.setCenter(destination, animated: true)
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: .aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
            self.mapView.setVisibleMapRect(rect, edgePadding: UIEdgeInsets(top: 0.0, left: 50.0, bottom: 0.0, right: 50.0), animated: true)
            
        }
        
    }
    
    func checkPlaces() {
        let start = startDateG?.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
        let end = endDateG?.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZZZZZ")
        
        
        Functions.functions().httpsCallable("isPlaceAvailable").call([
            "placeId": sportHall.uid!,
            "wish": [
                "start":  start!,
                "end": end!,
            ]
        ]) { (result, error) in
            LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
            if let _ = error as NSError? {
                self.showAlertSH(NSLocalizedString("ERROR_SH_TITLE", comment: ""), NSLocalizedString("ERROR_SH_SUBTITLES", comment: ""), true, buttonTitles: [NSLocalizedString("CLOSE", comment: "")])
            }
            if let _ = result?.data as? [String: Any] {
                self.showAlertSH(NSLocalizedString("BUSY_SH_TITLE", comment: ""), NSLocalizedString("BUSY_SH_SUBTITLES", comment: ""), true, buttonTitles: [NSLocalizedString("CLOSE", comment: "")])
            }
            self.showAlertSH(NSLocalizedString("VALID_SH_TITLE", comment: ""), NSLocalizedString("VALID_SH_SUBTITLES", comment: ""), false, buttonTitles: [NSLocalizedString("COMPLETE", comment: ""), NSLocalizedString("CLOSE", comment: "")])
        }
    }
    
    func changeStartTitle(_ title: String) {
        guard addStartDateIv.alpha == 1 else {
            addStartDateLbl.text = title
            return
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.addStartDateIv.alpha = 0
        }) { (result) in
            if result {
                self.addStartDateLbl.text = title
                UIView.animate(withDuration: 0.1, animations: {
                    self.addStartDateLbl.alpha = 1
                })
            }
        }
    }
    
    func changeEndTitle(_ title: String) {
        guard addEndDateIv.alpha == 1 else {
            addEndDateLbl.text = title
            return
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
            self.addEndDateIv.alpha = 0
        }) { (result) in
            if result {
                self.addEndDateLbl.text = title
                UIView.animate(withDuration: 0.1, animations: {
                    self.addEndDateLbl.alpha = 1
                })
            }
        }
    }
    
    func showAlertSH(_ title: String, _ subtitle: String, _ isError: Bool, buttonTitles: [String]) {
        let alert: AlertView = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
        if isError { alert.showError(title, subTitle: subtitle) }
        else { alert.showSuccess(title, subTitle: subtitle) } 
        for title in buttonTitles {
            alert.addButton(title) {
                alert.appearance.shouldAutoDismiss = true
                if title == NSLocalizedString("COMPLETE", comment: "") {
                    self.delegate?.finishVerifiedBook(name: self.sportHall.name!, zip: self.sportHall.zip!, thoroughfare: self.sportHall.thoroughfare!, city: self.sportHall.city!, country: self.sportHall.country!, latitude: self.sportHall.latitude.value!, longitude: self.sportHall.longitude.value!, startDate: self.startDateG!, endDate: self.endDateG!, placeUid: self.sportHall.uid!, false)
                    self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                    
                } else {
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    
}


// MARK: CollectionView Delegate
extension DescriptionSHViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return CacheService.shared.getDataFromDB(object: SportRealm()).filter("sportUUID = '\(sportHall.sport?.sportUUID ?? "")'").count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportCell", for: indexPath) as! SportCell
        guard case let sport as SportRealm = CacheService.shared.getDataFromDB(object: SportRealm()).filter("sportUUID = '\(sportHall.sport?.sportUUID ?? "")'").first else {
            print("***SPORT CREATE EVENT ERROR***")
            return cell
        }
        cell.sportIV.image = UIImage(data: sport.logo!)
        cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "")
        cell.sportLbl.textColor = UIColor.hexColor(UInt((sport.codeColor.value)!))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height-32, height: collectionView.frame.height-32)
    }
    
    
}


// MARK: Map Delegate
extension DescriptionSHViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = ThemeManager.currentColor().mainColor
        renderer.lineWidth = 2.0
        return renderer
    }
    
    
}
