//
//  FilterSHViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 19/11/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

@objc protocol FilterSHDelegate {
    func filterSelected()
}

class FilterSHViewController: UIViewController {
    
    @IBOutlet weak var bgBackBtn: UIVisualEffectView!
    @IBOutlet weak var bgResetBtn: UIVisualEffectView!
    @IBOutlet weak var sportCollectionView: UICollectionView!
    @IBOutlet weak var thanksLbl: UILabel!
    
    @IBOutlet weak var maxDistanceSlider: UISlider!
    @IBOutlet weak var maxDistanceLabel: UILabel!
    let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm
    let sports = CacheService.shared.getDataFromDB(object: SportRealm())
    var delegate: FilterSHDelegate?
    var isChange = false
    
    let oldRadius = (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).radius.value
    let oldSports = (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports
    var oldSportsCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        maxDistanceSlider.tintColor = ThemeManager.currentColor().mainColor
        maxDistanceSlider.value = Float(filter.radius.value ?? 50)
        maxDistanceLabel.text = String(Int(maxDistanceSlider.value)) + " " + NSLocalizedString("DISTANCE", comment: "")
        bgBackBtn.layer.cornerRadius = bgBackBtn.frame.height/2
        bgResetBtn.layer.cornerRadius = bgResetBtn.frame.height/2
        setupCollectionView()
        oldSportsCount = oldSports.count
        addSwipeGestureRecognizer()
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        if !ambience { return .default }
        
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
        
    }
    
    func setupCollectionView() {
        sportCollectionView.allowsMultipleSelection = true
        sportCollectionView.register(UINib(nibName: "SportCell", bundle: nil), forCellWithReuseIdentifier: "SportCell")
    }
    
    @IBAction func didSlideMaxDistance(_ sender: UISlider) {
        isChange = Int(sender.value) != oldRadius ? true : isChange
        
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).radius.value = Int(sender.value)
        }
        maxDistanceLabel.text = String(Int(maxDistanceSlider.value)) + " " + NSLocalizedString("DISTANCE", comment: "")
    }
    
    @IBAction func didCancelTapped(_ sender: Any) {
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).radius.value = oldRadius
            (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports = oldSports
        }
    }
    
    @IBAction func didResetFilter(_ sender: UIButton) {
        CacheService.shared.deleteFromDb(object: CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm, completion: {
            CacheService.shared.addData(object: FilterRealm(id: 2, radius: 50, isPrivate: false, isSponsored: false, minPrice: 0, maxPrice: 10, date: Date())!)
            self.delegate?.filterSelected()
        })
    }
    
    func isSportsChange(_ sport1:[SportRealm], sport2: [SportRealm]) -> Bool {
        if oldSportsCount != sport2.count { return true }
        let result = zip(sport1, sport2).enumerated().filter() { $1.0 == $1.1 }.map{$0.0}
        if result.count > 0 { return true }
        return isChange
    }
    
    @IBAction func didValidateTapped(_ sender: UIButton) {
        isChange = isSportsChange(Array(oldSports), sport2: Array((CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports))
        if isChange { delegate?.filterSelected() }
    }
    
    
}

extension FilterSHViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sports.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        selected(cell: cell, sport: sports[indexPath.row] as! SportRealm)
        do {
            try CacheService.shared.getDatabase().write {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.append(sports[indexPath.row] as! SportRealm)
            }
        } catch let error {
            print("***SPORT FILTER ERROR: [\(error)]***")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        unselected(cell: cell)
        do {
            try CacheService.shared.getDatabase().write {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.remove(at: (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.index(of: sports[indexPath.row] as! SportRealm)!)
            }
        } catch let error {
            print("***SPORT FILTER ERROR: [\(error)]***")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        if cell.isEnable {
            let cell = collectionView.cellForItem(at: indexPath) as! SportCell
            unselected(cell: cell)
            do {
                try CacheService.shared.getDatabase().write {
                    (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.remove(at: (CacheService.shared.getDataFromDB(object: FilterRealm()).last as! FilterRealm).sports.index(of: sports[indexPath.row] as! SportRealm)!)
                }
            } catch let error {
                print("***SPORT FILTER ERROR: [\(error)]***")
            }
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height-32, height: collectionView.frame.height-32)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportCell", for: indexPath) as! SportCell
        let sports = CacheService.shared.getDataFromDB(object: SportRealm())
        guard case let sport as SportRealm = sports[indexPath.row] else {
            print("***SPORT CREATE EVENT ERROR***")
            return cell
        }
        cell.sportIV.image = UIImage(data: sport.logo!)
        cell.sportIV.addBlackFilter()
        cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "")
        cell.sportUUID = sport.sportUUID
        
        if let sport = CacheService.shared.getDatabase().objects(FilterRealm.self).last?.sports.filter("sportUUID = '\(cell.sportUUID!)'").last {
            selected(cell: cell, sport: sport)
            cell.isEnable = true
        } else {
            cell.isEnable = false
            unselected(cell: cell)
        }
        
        return cell
    }
    
    func selected(cell: SportCell, sport: SportRealm) {
        cell.sportLbl.textColor = UIColor.hexColor(UInt((sport.codeColor.value)!))
        cell.sportIV.image = UIImage(data: sport.logo!)
    }
    
    func unselected(cell: SportCell) {
        cell.sportLbl.textColor = ThemeManager.currentTheme().unselectedFontColor
        cell.sportIV.addBlackFilter()
    }
    
}

extension FilterSHViewController {
    func addSwipeGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleGesture))
        tap.numberOfTouchesRequired = 2
        tap.numberOfTapsRequired = 5
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleGesture() {
        thanksLbl.alpha = thanksLbl.alpha == 0 ? 1 : 0
    }
}
