//
//  MapViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 21/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import MapKit

class EventLocation: NSObject, MKAnnotation {
    var title: String?
    var coordinate: CLLocationCoordinate2D
    var index: Int!
    
    init(title: String, coordinate: CLLocationCoordinate2D, index: Int) {
        self.title = title
        self.coordinate = coordinate
        self.index = index
    }
}


class MapViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var bgButton: UIVisualEffectView!
    @IBOutlet weak var pageController: UIPageControl!
    var pointAnnotations = [EventLocation]()
    var isSelectAnnotation = false
    var indexLastCell = 0
    var indexLastPin = 0
    let collectionMargin = CGFloat(16)
    let itemSpacing = CGFloat(8)
    var itemWidth = CGFloat(0)
    var itemHeight = CGFloat(0)
    var userLocation: CLLocation?
    let distanceFormatter = MKDistanceFormatter()
    let CacheMgr = CacheService.shared
    var radius = Double((CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value!)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        radius = (CacheService.shared.getDataFromDB(object: EventRealm(), "distance").last as! EventRealm).distance.value!/1000
        
        createAnnotation()
        
        
        bgButton.layer.cornerRadius = bgButton.frame.height/2
        addBlurStatusBar()
        setupCollectionViewLayout()
        mapView.setCenter(LocationService.shared.currentLocation!.coordinate, animated: true)
        mapView.reloadInputViews()
    }
    
    func addBlurStatusBar() {
        let statusBarHeight = UIApplication.shared.statusBarFrame.height
        let blur = UIBlurEffect(style: .light)
        let blurStatusBar = UIVisualEffectView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: statusBarHeight))
        blurStatusBar.effect = blur
        view.addSubview(blurStatusBar)
    }
    
    func createAnnotation() {
        let events = CacheMgr.getDataFromDB(object: EventRealm())
        for (index, event) in events.enumerated() {
            let annotation = EventLocation(title: (event as! EventRealm).title!.capitalizingFirstLetter(), coordinate: CLLocationCoordinate2D(latitude: (event as! EventRealm).latitude.value!, longitude:(event as! EventRealm).longitude.value!), index: index)
            pointAnnotations.append(annotation)
        }
        mapView.addAnnotations(pointAnnotations)
        
        let newAnnotations = ContestedAnnotationTool.annotationsByDistributingAnnotations(annotations: mapView.annotations) { (oldAnnotation:MKAnnotation, newCoordinate:CLLocationCoordinate2D) in
            if let ano = oldAnnotation as? EventLocation {
                return EventLocation(title: ano.title!, coordinate: newCoordinate, index: ano.index)
            }
            return EventLocation(title: "", coordinate: CLLocationCoordinate2D(latitude: 0, longitude: 0), index: 0)
        }
        
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotations(newAnnotations)
        
        mapView.showAnnotations(mapView.annotations, animated: true)
    }
    
    @IBAction func didReturn(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    func setupCollectionViewLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        itemWidth =  UIScreen.main.bounds.width - collectionMargin * 2.0
        if UIDevice().userInterfaceIdiom == .phone {
            switch UIScreen.main.nativeBounds.height {
            case 1136:
                itemHeight = 130
            default:
                itemHeight = 144
            }
        }
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        layout.headerReferenceSize = CGSize(width: collectionMargin, height: 0)
        layout.footerReferenceSize = CGSize(width: collectionMargin, height: 0)
        
        layout.minimumLineSpacing = itemSpacing
        layout.scrollDirection = .horizontal
        collectionView!.collectionViewLayout = layout
        collectionView?.decelerationRate = UIScrollView.DecelerationRate.fast
    }
    
    @IBAction func didChangedEvent(_ sender: UIPageControl) {
        if !isSelectAnnotation {
            let selectedAnnotation = pointAnnotations[pageController.currentPage]
            let scalingFactor: Double = abs((cos(2 * .pi * selectedAnnotation.coordinate.latitude / 360.0)))
            let span = MKCoordinateSpan(latitudeDelta: CLLocationDegrees(radius / 69.0), longitudeDelta: CLLocationDegrees(radius / (scalingFactor * 69.0)))
            let region = MKCoordinateRegion(center: selectedAnnotation.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            mapView.selectAnnotation(selectedAnnotation, animated: true)
            isSelectAnnotation = false
        }
    }
}

extension MapViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.pageController.numberOfPages = CacheMgr.getDataFromDB(object: EventRealm()).count
        return CacheMgr.getDataFromDB(object: EventRealm()).count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellEvent", for: indexPath) as! EventCollectionCell
        let item = CacheMgr.getDataFromDB(object: EventRealm(), "dateStart")[indexPath.row] as! EventRealm
        let (nbDate, month, hour, min) = getMainDate(item)
        cell.nbDayLabel.text = nbDate
        cell.monthLabel.text = month
        cell.hourLabel.text = "\(hour):\(min)"
        cell.titleLabel.text = item.title!.capitalized
        cell.logoView.image = UIImage(data: (item.sport?.logo!)!)
        cell.sportLabel.text = item.sport?.title!
        cell.sportLabel.textColor = UIColor.hexColor(UInt((item.sport?.codeColor.value)!))
        cell.priceLabel.text = item.price.value! == 0.0 ? NSLocalizedString("FREE", comment: "") : "\(item.price.value!) \(item.currency!)"
        cell.placeLabel.text =  "\(item.nbMaxParticipant.value!-item.nbParticipant.value!)"
        cell.distanceLabel.text = "\(getLocation(item)) \(NSLocalizedString("ACTUAL_POSITION", comment: ""))"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if pageController.currentPage + 2 == CacheMgr.getDataFromDB(object: EventRealm()).count && !isSelectAnnotation && indexPath.row + 3 == CacheMgr.getDataFromDB(object: EventRealm()).count {
            let selectedAnnotation = pointAnnotations[pageController.currentPage+1]
            let scalingFactor: Double = abs((cos(2 * .pi * LocationService.shared.currentLocation!.coordinate.latitude / 360.0)))
            let span = MKCoordinateSpan(latitudeDelta: CLLocationDegrees(radius / 69.0), longitudeDelta: CLLocationDegrees(radius / (scalingFactor * 69.0)))
            let region = MKCoordinateRegion(center: LocationService.shared.currentLocation!.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            mapView.setCenter(selectedAnnotation.coordinate, animated: true)
            mapView.selectAnnotation(selectedAnnotation, animated: true)
            isSelectAnnotation = false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !isSelectAnnotation {
            let selectedAnnotation = pointAnnotations[pageController.currentPage]
            let scalingFactor: Double = abs((cos(2 * .pi * LocationService.shared.currentLocation!.coordinate.latitude / 360.0)))
            let span = MKCoordinateSpan(latitudeDelta: CLLocationDegrees(radius / 69.0), longitudeDelta: CLLocationDegrees(radius / (scalingFactor * 69.0)))
            let region = MKCoordinateRegion(center: LocationService.shared.currentLocation!.coordinate, span: span)
            mapView.setRegion(region, animated: true)
            mapView.setCenter(selectedAnnotation.coordinate, animated: true)
            mapView.selectAnnotation(selectedAnnotation, animated: true)
            isSelectAnnotation = false
        }
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        isSelectAnnotation = false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "DescriptionEvent") as! DescriptionViewController
        if let event = CacheMgr.getDataFromDB(object: EventRealm(), "dateStart")[indexPath.row] as? EventRealm {
            vc.event = event
            vc.userLocation = LocationService.shared.currentLocation
            DispatchQueue.main.async {
                self.present(vc, animated: true, completion: nil)
            }
        }
    }

    
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let selectedAnnotation = view.annotation as? EventLocation
        if let index = selectedAnnotation?.index {
            isSelectAnnotation = true
            self.collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    
}

public struct ContestedAnnotationTool {
    
    private static let radiusOfEarth = Double(6378100)
    
    public typealias annotationRelocator = ((_ oldAnnotation:MKAnnotation, _ newCoordinate:CLLocationCoordinate2D) -> (MKAnnotation))
    
    public static func annotationsByDistributingAnnotations(annotations: [MKAnnotation], constructNewAnnotationWithClosure ctor: annotationRelocator) -> [MKAnnotation] {
        let coordinateToAnnotations = groupAnnotationsByCoordinate(annotations: annotations)
        var newAnnotations = [MKAnnotation]()
        
        for (_, annotationsAtCoordinate) in coordinateToAnnotations {
            let newAnnotationsAtCoordinate = ContestedAnnotationTool.annotationsByDistributingAnnotationsContestingACoordinate(annotations: annotationsAtCoordinate, constructNewAnnotationWithClosure: ctor)
            newAnnotations.append(contentsOf: newAnnotationsAtCoordinate)
        }
        return newAnnotations
    }
    
    private static func groupAnnotationsByCoordinate(annotations: [MKAnnotation]) -> [CLLocationCoordinate2D: [MKAnnotation]] {
        var coordinateToAnnotations = [CLLocationCoordinate2D: [MKAnnotation]]()
        for annotation in annotations {
            let coordinate = annotation.coordinate
            let annotationsAtCoordinate = coordinateToAnnotations[coordinate] ?? [MKAnnotation]()
            coordinateToAnnotations[coordinate] = annotationsAtCoordinate + [annotation]
        }
        return coordinateToAnnotations
    }
    
    private static func annotationsByDistributingAnnotationsContestingACoordinate(annotations: [MKAnnotation], constructNewAnnotationWithClosure ctor: annotationRelocator) -> [MKAnnotation] {
        var newAnnotations = [MKAnnotation]()
        let contestedCoordinates = annotations.map{ $0.coordinate }
        let newCoordinates = coordinatesByDistributingCoordinates(coordinates: contestedCoordinates)
        
        for (i, annotation) in annotations.enumerated() {
            let newCoordinate = newCoordinates[i]
            let newAnnotation = ctor(annotation, newCoordinate)
            newAnnotations.append(newAnnotation)
        }
        return newAnnotations
    }
    
    private static func coordinatesByDistributingCoordinates(coordinates: [CLLocationCoordinate2D]) -> [CLLocationCoordinate2D] {
        if coordinates.count == 1 {
            return coordinates
        }
        
        var result = [CLLocationCoordinate2D]()
        let distanceFromContestedLocation: Double = 3.0 * Double(coordinates.count) / 2.0
        let radiansBetweenAnnotations = (.pi * 2) / Double(coordinates.count)
        
        for (i, coordinate) in coordinates.enumerated() {
            let bearing = radiansBetweenAnnotations * Double(i)
            let newCoordinate = calculateCoordinateFromCoordinate(coordinate: coordinate, onBearingInRadians: bearing, atDistanceInMetres: distanceFromContestedLocation)
            result.append(newCoordinate)
        }
        return result
    }
    
    private static func calculateCoordinateFromCoordinate(coordinate: CLLocationCoordinate2D, onBearingInRadians bearing: Double, atDistanceInMetres distance: Double) -> CLLocationCoordinate2D {
        
        let coordinateLatitudeInRadians = coordinate.latitude * .pi / 180;
        let coordinateLongitudeInRadians = coordinate.longitude * .pi / 180;
        
        let distanceComparedToEarth = distance / radiusOfEarth;
        let resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearing));
        let resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearing) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
        let latitude = resultLatitudeInRadians * 180 / .pi
        let longitude = resultLongitudeInRadians * 180 / .pi
        
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
}

extension CLLocationCoordinate2D: Hashable {
    static public func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
    
    public var hashValue: Int {
        get {
            return (latitude.hashValue&*397) &+ longitude.hashValue;
        }
    }
}

extension MapViewController {
    func getMainDate(_ event: EventRealm) -> (String, String, String, String) {
        let date: String = event.dateStart!.toString(dateFormat: "d MMM HH mm")
        let dateArray = date.components(separatedBy: " ")
        return (dateArray[0], dateArray[1], dateArray[2], dateArray[3])
    }
    
    func getHour(_ event: EventRealm) -> (String, String, String, String) {
        let date: String = event.dateStart!.toString(dateFormat: "d MMM HH mm ")
        let dateArray = date.components(separatedBy: " ")
        return (dateArray[0], dateArray[1], dateArray[2], dateArray[3])
    }
    
    func getLocation(_ event: EventRealm) -> String {
        if let userLocation = LocationService.shared.currentLocation {
            let location = CLLocation(latitude: CLLocationDegrees(truncating: NSNumber(value: event.latitude.value!)), longitude: CLLocationDegrees(truncating: NSNumber(value: event.longitude.value!)))
            let formattedDistance = distanceFormatter.string(fromDistance: location.distance(from: userLocation))
            return formattedDistance
        }
        return ""
    }
    
    
}

extension MapViewController: UIScrollViewDelegate {
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let pageWidth = Float(itemWidth + itemSpacing)
        let targetXContentOffset = Float(targetContentOffset.pointee.x)
        let contentWidth = Float(collectionView!.contentSize.width  )
        var newPage = Float(self.pageController.currentPage)
        
        if velocity.x == 0 {
            newPage = floor( (targetXContentOffset - Float(pageWidth) / 2) / Float(pageWidth)) + 1.0
        } else {
            newPage = Float(velocity.x > 0 ? self.pageController.currentPage + 1 : self.pageController.currentPage - 1)
            if newPage < 0 {
                newPage = 0
            }
            if (newPage > contentWidth / pageWidth) {
                newPage = ceil(contentWidth / pageWidth) - 1.0
            }
        }
        
        self.pageController.currentPage = Int(newPage)
        let point = CGPoint (x: CGFloat(newPage * pageWidth), y: targetContentOffset.pointee.y)
        targetContentOffset.pointee = point
    }
}

