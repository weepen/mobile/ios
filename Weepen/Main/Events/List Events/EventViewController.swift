//
//  EventViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 21/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Hero
import MapKit
import ListPlaceholder
import Ambience
import RealmSwift
import GradientLoadingBar
import Firebase


class EventViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var dateFilterBtn: UpperButton!
    @IBOutlet weak var heightNavbarCst: NSLayoutConstraint!
    @IBOutlet weak var mapBtn: UIButton!
    @IBOutlet weak var separeBtnVw: UpperView!
    @IBOutlet weak var createBtn: UpperButton!
    @IBOutlet weak var filterBtn: UpperButton!
    @IBOutlet weak var separateFilterView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var noEventView: UIView!
    
    let refreshControl = UIRefreshControl()
    let LocationMgr = LocationService.shared
    let CacheMgr = CacheService.shared
    var token: NotificationToken?
    
    var loadedPage: UInt = 0
    var nbPages: UInt = 0
    var previousCell: EventCell?
    var userLocation: CLLocation?
    var isLoaded = false
    var nbEmptyCell = 0
    var count = 0
    var arrayFilter = [String]()
    var popRadiusSlider: PopRadiusSlider?
    fileprivate var collapseDetailViewController = true
    private var gradientLoadingBar: BottomGradientLoadingBar?
    
    
    // MARK: ViewController Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserRealm().setTokenRegistrationUser((CacheMgr.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!, UserDefaults.standard.object(forKey: "registrationToken") as! String)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appMovedToForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        setupPreferences()
        EventRealm().getNBParticipantUpdate(.display)
        self.token = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isDisplay = true").observe({ (result) in
            self.tableView.applyChanges(changes: result, .display)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count == 0 && self.nbEmptyCell == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LocationMgr.delegate = self
        LocationMgr.startUpdatingLocation()
        self.setupView()
//        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventCell")

   
        if CacheMgr.getDatabase().objects(EventRealm.self).filter("isDisplay = true").count == 0 { startLoadingEvent() }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserDefaults.standard.set(false, forKey: "isDisplayVC")
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        self.token?.invalidate()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let navController = segue.destination as? UINavigationController, let vc = navController.topViewController as? DescriptionViewController else { return }
        collapseDetailViewController = false
        guard let eventIndex = tableView.indexPathForSelectedRow?.row else { return }
        
        if let event = CacheMgr.getDataFromDB(object: EventRealm(), "dateStart").filter("isDisplay = true")[eventIndex] as? EventRealm {
            vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
            vc.navigationItem.leftItemsSupplementBackButton = true
            vc.event = event
            vc.delegate = self
            vc.userLocation = LocationMgr.currentLocation
            let cell = tableView.cellForRow(at: IndexPath(row: eventIndex, section: 0)) as! EventCell
            vc.cell = cell
            cell.sportIV.hero.id = "imageSport"
            cell.titleLbl.hero.id = "title"
            cell.borderView.hero.id = "border"
        }
    }
    
    // MARK: ViewController Setup Functions
    func setupPreferences() {
        UserDefaults.standard.set(true, forKey: "isDisplayVC")
        if UserDefaults.standard.bool(forKey: "isNeedRefreshList") {
            UserDefaults.standard.set(false, forKey: "isNeedRefreshList")
            refreshData(nil)
        }
    }
    
    func setupView() {
        if let splitController = self.splitViewController as? MasterSplitViewController { splitController.delegate = self }
        if let tabController = self.tabBarController as? MainTabBarViewController { tabController.reinstateAmbience() }
        self.reinstateAmbience()
        setupRefreshControl()
        
        tableView.estimatedRowHeight = UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136 ? 140 : 160
        
        CacheService.shared.addData(object: FilterRealm(id: 1, radius: 50, isPrivate: false, isSponsored: false, minPrice: 0, maxPrice: 10, date: Date())!)
        arrayFilter.append("50 " + NSLocalizedString("DISTANCE", comment: ""))
        
        gradientLoadingBar = BottomGradientLoadingBar(height: 3.5, gradientColorList: [UIColor.hexColor(0xA848D3), UIColor.hexColor(0xF12711), UIColor.hexColor(0xEC008C), UIColor.hexColor(0xF25D6C), UIColor.hexColor(0xFC834F), UIColor.hexColor(0xF5AF19)], onView: loadingView)
        gradientLoadingBar?.gradientView.alpha = 0
    }
    
    @objc func appMovedToBackground() {
        token?.invalidate()
    }
    
    @objc func appMovedToForeground() {
        refreshData(nil)
        self.token = CacheService.shared.getDataFromDB(object: EventRealm(), "dateStart").filter("isDisplay = true").observe({ (result) in
            self.tableView.applyChanges(changes: result, .display)
            if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count == 0 { self.revealEmptyView() }
            else { self.revealTableView() }
        })
    }
    
    func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        refreshControl.tintColor = ThemeManager.currentColor().mainColor
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("DOWNLOAD_EVENT", comment: ""), attributes: nil)
    }
    
    @objc private func refreshData(_ sender: Any?) {
        searchEvent(searchBar.text!)
    }
    
    // MARK: ViewController UI
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        guard let currentState = notification.userInfo?["currentState"] as? AmbienceState else { return }
        if let button = mapBtn {
            button.tintColor = currentState == .invert ? .white : .hexColor(0x0E0E15)
        }
        if let button2 = createBtn {
            button2.tintColor = currentState == .invert ? .white : .hexColor(0x0E0E15)
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    
}


// MARK: IBAction Function
extension EventViewController {
    @IBAction func back(segue: UIStoryboardSegue) {}
    
    @IBAction func didMap(_ sender: UIButton) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        vc.userLocation = LocationMgr.currentLocation
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func didDateFilter(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Date", bundle: nil).instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    @IBAction func didFilters(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        vc.delegate = self
        DispatchQueue.main.async {
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    
}


// MARK: Events Cycle
extension EventViewController {
    func createEventArray(_ hits: [[String : Any]]) {
        var events = [EventRealm]()
        for hit in hits {
            if let event = EventRealm(dataFromAlgolia: hit) {
                events.append(event)
            }
        }
        let (removeEvents, updateEvents, addEvents) = EventRealm().orderEventsDisplay(events)
        EventRealm().removeEvents(removeEvents, .display)
        removeEmptyCell()
        EventRealm().updateEvents(updateEvents, .display)
        EventRealm().addEvents(addEvents, .display)
        EventRealm().getNBParticipantUpdate(.display)
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count != self.tableView.numberOfRows(inSection: 0) { self.tableView.addMissRow(CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count) }
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count == 0 { revealEmptyView() }
        else { revealTableView() }
        endLoadingEvent()
    }
    
    func searchEvent(_ text: String) {
        gradientLoadingBar?.show()
        isLoaded = true
        refreshControl.endRefreshing()
        UIView.animate(withDuration: 0.5) {
            self.gradientLoadingBar?.gradientView.alpha = 1
        }
        
        let search = text
        let filters = getFilters()
        let radius = getRadius()
        let distance = getLatLng()
        if !distance.2 { return }
        
        AlgoliaService.shared.getEvent(index: AlgoliaService.shared.indexEvents, search: search, filters: filters, radius: radius, latitude: distance.0, longitude: distance.1) { (result, nbPages, content) in
            if result {
                self.count = 0
                self.createEventArray(content)
                UIView.animate(withDuration: 0.5, delay: 0, options: [], animations: {
                    self.gradientLoadingBar?.gradientView.alpha = 0
                }, completion: { (result) in
                    if result {
                        self.gradientLoadingBar?.hide()
                        self.isLoaded = false
                    }
                })
            } else {
                let alert: AlertView = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
                alert.showWarning("Une erreur s'est produite", subTitle: "Il semble que votre connexion soit lente")
                alert.addButton("Fermer") {
                    alert.appearance.shouldAutoDismiss = true
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    
}


// MARK: Loading Functions
extension EventViewController {
    func startLoadingEvent() {
        nbEmptyCell = 4
        tableView.reloadData()
        tableView.showLoader(ThemeManager.currentTheme().downView)
    }
    
    func endLoadingEvent() {
        tableView.hideLoader()
    }
    
    func revealEmptyView() {
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count == 0 {
            UIView.animate(withDuration: 0.3) {
                self.noEventView.alpha = 1
                self.tableView.alpha = 0
            }
        }
    }
    
    func revealTableView() {
        if CacheService.shared.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count >= 0 {
            UIView.animate(withDuration: 0.1) {
                self.noEventView.alpha = 0
                self.tableView.alpha = 1
            }
        }
    }
    
    func removeEmptyCell() {
        if nbEmptyCell != 0 {
            var indexes = [IndexPath]()
            for index in 0...nbEmptyCell-1 {
                indexes.append(IndexPath(row: index, section: 0))
            }
            nbEmptyCell = 0
            tableView.deleteRows(at: indexes, with: .fade)
        }
    }
    
    
}


// MARK: ViewController Getter
extension EventViewController {
    func getMainDate(_ item: EventRealm) -> (String, String, String, String) {
        let date: String = item.dateStart!.toString(dateFormat: "d MMM HH mm")
        let dateArray = date.components(separatedBy: " ")
        return (dateArray[0], dateArray[1], dateArray[2], dateArray[3])
    }
    
    func getHour(_ item: EventRealm) -> (String, String, String, String) {
        let date: String = item.dateStart!.toString(dateFormat: "d MMM HH mm ")
        let dateArray = date.components(separatedBy: " ")
        return (dateArray[0], dateArray[1], dateArray[2], dateArray[3])
    }
    
    func getRadius() -> UInt {
        return UInt((CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value!)
    }
    
    func getFilters() -> String {
        var filter = getPrivateFilter() + " AND " + getSponsoredFilter() + " AND " + getPriceFilter() + " AND " + getDate()
        if getSportFilter() != "" {
            filter += " AND " + getSportFilter()
        }
        return filter
    }
    
    func getSportFilter() -> String {
        var sportFilter = ""
        for (index, sport) in (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.enumerated() {
            if index == 0 {
                sportFilter = "sportUID:\(sport.sportUUID!)"
            } else {
                sportFilter = sportFilter + " OR sportUID:\(sport.sportUUID!)"
            }
        }
        return sportFilter
    }
    
    func getPrivateFilter() -> String {
        return (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isSponsored == true ? "private:true" : "private:false"
    }
    
    func getSponsoredFilter() -> String {
        return (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isSponsored == true ? "sponsored:true" : "sponsored:false"
    }
    
    func getPriceFilter() -> String {
        let minPrice = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).minPrice.value!
        var maxPrice = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).maxPrice.value!
        if maxPrice == 10 {
            maxPrice = 1000000000
        }
        return "reservation.price: \(minPrice) TO \(maxPrice)"
    }
    
    
    func getLatLng() -> (Double, Double, Bool) {
        LocationMgr.startUpdatingLocation(true)
        if userLocation != nil {
            return (Double((userLocation?.coordinate.latitude)!), Double((userLocation?.coordinate.longitude)!), true)
        }
        return(0.0, 0.0, false)
    }
    
    func getDate() -> (String) {
        var date = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).date!
        if date.timeIntervalSinceNow < 0 {
            date = Date()
        }
        return "startDate > \(date.timeIntervalSince1970)"
    }
    
    
}


// MARK: TableView Delegate
extension EventViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if nbEmptyCell != 0 {
            return nbEmptyCell
        } else {
            return CacheMgr.getDatabase().objects(EventRealm.self).filter("isDisplay = true").count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if nbEmptyCell != 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyCell", for: indexPath)
            return cell
        } else {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventTableViewCell
//            return cell
            if (indexPath.row + 5) >= CacheMgr.getDataFromDB(object: EventRealm()).filter("isDisplay = true").count {
                //loadMore()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventCell
            let item = CacheMgr.getDataFromDB(object: EventRealm(), "dateStart").filter("isDisplay = true")[indexPath.row] as! EventRealm
            let (nbDate, month, hour, min) = getMainDate(item)
            cell.nbDateLbl.text = nbDate
            cell.monthLbl.text = month
            cell.hourLbl.text = "\(hour):\(min)"
            cell.titleLbl.text = item.title?.capitalized
            if let sport = item.sport {
                cell.sportIV.image = UIImage(data: sport.logo!)
                cell.sportLbl.textColor = UIColor.hexColor(UInt(sport.codeColor.value!))
                cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "").uppercased()
            }
            cell.priceLbl.text = item.price.value == 0 ? NSLocalizedString("FREE", comment: "") : "\(item.price.value!) \(item.currency!)"
            if item.nbParticipant.value == nil { cell.nbPlaceLbl.text = "•••" }
            else { cell.nbPlaceLbl.text = "\(item.nbMaxParticipant.value!-item.nbParticipant.value!)" }
            cell.distanceLbl.text = "\(LocationService.shared.getLocation(item.latitude.value!, item.longitude.value!)) \(NSLocalizedString("ACTUAL_POSITION", comment: ""))"
            if UIScreen.main.bounds.height == 568.0 {
                for constraint in cell.priceLbl.constraints {
                    if constraint.identifier == "leading" || constraint.identifier == "trailling" {
                        constraint.constant = 8
                    }
                }
                cell.priceLbl.layoutIfNeeded()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 160
        return UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 1136 ? 140 : 160
    }
    
    
}


// MARK: ScrollView Delegate
extension EventViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == tableView {
            UIView.animate(withDuration: 0.3) {
                let actualPosition = scrollView.panGestureRecognizer.translation(in: scrollView.superview)
                if (actualPosition.y >= 0){
                    self.heightNavbarCst.constant = 130
                    self.mapBtn.alpha = 1
                    self.createBtn.alpha = 1
                    self.separeBtnVw.alpha = 1
                    self.filterBtn.alpha = 1
                    self.dateFilterBtn.alpha = 1
                } else {
                    self.heightNavbarCst.constant = 56
                    self.mapBtn.alpha = 0
                    self.createBtn.alpha = 0
                    self.separeBtnVw.alpha = 0
                    self.filterBtn.alpha = 0
                    self.dateFilterBtn.alpha = 0
                }
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
}


// MARK: DescriptionView Delegate
extension EventViewController: DescriptionViewDelegate {
    func getCell(cell: EventCell, _ isUnregister: Bool) {
        previousCell = cell
    }
    
    
}


// MARK: Location Delegate
extension EventViewController: LocationUpdateProtocol {
    func locationDidChangeAuthorization(status: Int) {}
    
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {
//        if userLocation == nil {
        print("location updated")
            userLocation = CLLocation(latitude: latitude, longitude: longitude)
            searchEvent(searchBar.text!)
//        }
    }
    
    
}


// MARK: SearchBar Delegate
extension EventViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if let searchBarText = searchBar.text {
            searchEvent(searchBarText.replacingCharacters(in: Range(range, in: searchBarText)!, with: text))
            return true
        }
        return false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchBarText = searchBar.text {
            if searchBarText == "" {
                searchBar.showsCancelButton = false
                self.view.endEditing(true)
                searchEvent(searchBarText)
            }
        }
    }
    
    
}


// MARK: SplitView Delegate
extension EventViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return collapseDetailViewController
    }
    
    
}


// MARK: CollectionView Delegate
extension EventViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayFilter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm
        if arrayFilter[indexPath.row] == "Sponso." {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterSponsoredCell", for: indexPath) as! FilterSponsoredCell
            cell.removeBtn.image = UIImage(named: "Cancel")
            cell.removeBtn.image? = (cell.removeBtn.image?.withRenderingMode(.alwaysTemplate))!
            return cell
        } else if arrayFilter[indexPath.row] == "Priv." {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterPrivateCell", for: indexPath) as! FilterPrivateCell
            cell.removeBtn.image = UIImage(named: "Cancel")
            cell.removeBtn.image? = (cell.removeBtn.image?.withRenderingMode(.alwaysTemplate))!
            return cell
        } else if arrayFilter[indexPath.row] == "" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportFilter", for: indexPath) as! FilterSportCell
            cell.bgView.backgroundColor = UIColor.hexColor(UInt((filter.sports[indexPath.row-1].codeColor.value)!))
            cell.removeBtn.image = UIImage(named: "Cancel")
            cell.removeBtn.image? = (cell.removeBtn.image?.withRenderingMode(.alwaysTemplate))!
            cell.removeBtn.tintColor = .white
            cell.sportPicture.image = UIImage(data: filter.sports[indexPath.row-1].logo!)
            cell.sportPicture.image? = (cell.sportPicture.image?.withRenderingMode(.alwaysTemplate))!
            cell.sportPicture.tintColor = .white
            cell.bgView.layer.cornerRadius = cell.bgView.frame.height/2
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterRadiusCell", for: indexPath) as! FilterRadiusCell
            popRadiusSlider = PopRadiusSlider(with: cell)
            cell.radiusLbl.text = arrayFilter[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if arrayFilter[indexPath.row] == "" {
            return CGSize(width: 50, height: collectionView.frame.height)
        } else if arrayFilter[indexPath.row] == "Priv." {
            return CGSize(width: 66, height: collectionView.frame.height)
        } else if arrayFilter[indexPath.row] == "Sponso." {
            return CGSize(width: 94, height: collectionView.frame.height)
        } else {
            let size = arrayFilter[indexPath.row].size(withAttributes: [NSAttributedString.Key.font: UIFont(name: ThemeManager.currentTheme().fontNameRegular, size: 16)!])
            return CGSize(width: size.width+20, height: collectionView.frame.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        try! CacheService.shared.getDatabase().write({
            if arrayFilter[indexPath.row] == "" {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.remove(at: (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.index(of: ((CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports[indexPath.row-1] ))!)
            } else if arrayFilter[indexPath.row] == "Priv." {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isPrivate = false
            } else if arrayFilter[indexPath.row] == "Sponso." {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isSponsored = false
            } else {
                let initRadius : Float? = Float((CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value!)
                
                let dataChangedCallback : PopRadiusSlider.PopRadiusSliderCallback = { (newRadius: Float, forCell: FilterRadiusCell, refresh: Bool) -> () in
                    if let cell = collectionView.cellForItem(at: indexPath) as? FilterRadiusCell {
                        cell.radiusLbl.font = Int(newRadius) > 100 ? cell.radiusLbl.font.withSize(14) : cell.radiusLbl.font.withSize(16)
                        cell.radiusLbl.text = "\(Int(newRadius)) " + NSLocalizedString("DISTANCE", comment: "")
                        if refresh {
                            self.arrayFilter[indexPath.row] = cell.radiusLbl.text!
                            try! CacheService.shared.getDatabase().write {
                                (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value = Int(newRadius)
                            }
                            self.searchEvent(self.searchBar.text!)
                        }
                    }
                }
                popRadiusSlider!.pick(self, initRadius: initRadius, dataChanged: dataChangedCallback)
                return
            }
            arrayFilter.remove(at: indexPath.row)
            collectionView.deleteItems(at: [indexPath])
            if arrayFilter.count == 1 {
                UIView.animate(withDuration: 0.5, animations: {
                    self.filterBtn.setTitle(NSLocalizedString("FILTERS", comment: ""), for: .normal)
                })
                self.view.layoutIfNeeded()
            }
            searchEvent(searchBar.text!)
        })
    }
    
    
}


// MARK: DateFilter Delegate
extension EventViewController: DateFilterDelegate {
    func filterSelected(date: String) {
        dateFilterBtn.setTitle(date, for: .normal)
        searchEvent(searchBar.text!)
    }
    
    
}


// MARK: Filter Delegate
extension EventViewController: FilterDelegate {
    func filterSelected() {
        createArrayFilter()
        let text = arrayFilter.count > 1 ? NSLocalizedString("FILTERS", comment: "") + "•" + "\(arrayFilter.count-1)" : NSLocalizedString("FILTERS", comment: "")
        filterBtn.setTitle(text, for: .normal)
        filterCollectionView.reloadData()
        searchEvent(searchBar.text!)
    }
    
    func createArrayFilter() {
        arrayFilter = []
        let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm
        arrayFilter.append("\(filter.radius.value!) " + NSLocalizedString("DISTANCE", comment: ""))
        for _ in filter.sports {
            arrayFilter.append("")
        }
        if filter.minPrice.value! > 0 || filter.maxPrice.value! < 10 {
            arrayFilter.append("\(filter.minPrice.value!)-\(filter.maxPrice.value!)")
        }
        if filter.isSponsored == true {
            arrayFilter.append("Sponso.")
        }
        if filter.isPrivate == true {
            arrayFilter.append("Priv.")
        }
    }
}


//    func loadMore() {
//        if loadedPage + 1 >= nbPages { return }
//        let nextQuery = Query(copy: query)
//        nextQuery.page = loadedPage + 1
//        indexSearch.search(nextQuery, completionHandler: { (data , error) -> Void in
//            if (nextQuery.query != self.query.query) || (error != nil) {
//                return
//            }
//            self.loadedPage = nextQuery.page!
//            for array in data! where array.key == "hits" {
//                let hits = array.value as! [[String:Any]]
//                self.setEvent(hits)
//            }
//        })
//    }
