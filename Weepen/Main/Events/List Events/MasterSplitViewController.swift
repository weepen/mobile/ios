//
//  MasterSplitViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 06/08/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

class MasterSplitViewController: UISplitViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.preferredDisplayMode = .allVisible
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        if !ambience { return .default }
        
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
        
    }
    
    
}
