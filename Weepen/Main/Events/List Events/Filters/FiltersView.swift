//
//  FiltersView.swift
//  Weepen
//
//  Created by Louis Cheminant on 22/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Magnetic
import CoreData
import SwiftRangeSlider


@objc protocol DateFilterViewDelegate {
    func getInfo(_ string: String, index: Int, _ date: String)
}

class DateFilterView: YNDropDownView {
    var dateFilterDelegate: DateFilterViewDelegate?
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var clearButton: UIButton!
    
    var isSelected = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        isSelected = true
        self.alwaysSelected(at: 0)
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        guard let date = formatter.date(from: datePicker.date.iso8601) else {
            print("can't convert time string")
            return
        }
        formatter.timeZone = TimeZone.current
        dateFilterDelegate?.getInfo(configTime(), index: 0, formatter.string(from: date))
        self.hideMenu()
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        if isSelected == false {
            self.normalSelected(at: 0)
        }
        self.hideMenu()
    }
    
    @IBAction func didClearFilter(_ sender: UIButton) {
        isSelected = false
        self.changeMenu(title: "Auj.", status: .normal, at: 0)
        dateFilterDelegate?.getInfo("", index: 0, "")
        self.hideMenu()
    }
    
    override func dropDownViewOpened() {
        datePicker.minimumDate = Date()
        clearButton.alpha = isSelected ? 1 : 0
    }
    
    override func dropDownViewClosed() {  }
    
    // TODO: Review this function
    func configTime() -> String {
        let dateStr = datePicker.date.offset(from: Date())
        switch dateStr {
        case "aujourd'hui":
            return "Auj."
        case "1 jour":
            return "Dem."
        default:
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM"
            let selectedDate = dateFormatter.string(from: datePicker.date)
            return selectedDate
        }
    }
    
}

@objc protocol SportFilterViewDelegate {
    func getInfoSport(_ string: [String], index: Int)
}

class SportFilterView: YNDropDownView {
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var sportView: MagneticView! {
        didSet {
            magnetic.magneticDelegate = self
        }
    }
    
    var magnetic: Magnetic { return sportView.magnetic }
    var sportFilterDelegate: SportFilterViewDelegate?
    var isSelected = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        isSelected = true
        self.alwaysSelected(at: 1)
//        sportChoiceStr.removeAll()
//        for sportC in sportChoice {
//            sportChoiceStr.append(sportC.title!)
//        }
//        sportFilterDelegate?.getInfoSport(sportChoiceStr, index: 1)
        self.hideMenu()
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        if isSelected == false {
            self.normalSelected(at: 1)
        }
        self.hideMenu()
    }
    
    @IBAction func clearFilterClicked(_ sender: UIButton) {
        isSelected = false
        
        
//        let nodes = self.magnetic.selectedChildren
//        for node in nodes {
//            node.isSelected = false
//        }
//        sportChoice.removeAll()
        self.changeMenu(title: "Sport", status: .normal, at: 1)
//        sportFilterDelegate?.getInfoSport([], index: 1)
        self.hideMenu()
    }
    
    override func dropDownViewOpened() {
        clearButton.alpha = isSelected ? 1 : 0

//        if magnetic.children.count == 1 {
//            for case let sport as SportRealm in CacheService.shared.getDataFromDB(object: SportRealm()) {
//                let node = Node(text: sport.title!.capitalized, image: UIImage(named: sport.title!), color: UIColor.hexColor(UInt((sport.codeColor.value)!)), radius: 40)
//                magnetic.addChild(node)
//                if user.sports.filter("sportUUID = '\(sport.sportUUID!)'").count > 0 {
//                    node.isSelected = true
//                }
//            }
//        }
    }
    
    
}

extension SportFilterView: MagneticDelegate {
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
//        if let sport = allSports.first(where: { $0.title == node.text?.lowercased() }) {
//            sportChoice.append(sport)
//        }
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
//        if let sport = sportChoice.first(where: { $0.title == node.text?.lowercased() }) {
//            sportChoice = sportChoice.filter() { $0 !== sport }
//        }
    }
    
}

@objc protocol TypeFilterViewDelegate {
    func getInfoType(_ string: String, index: Int)
}

class TypeFilterView: YNDropDownView {
    
    @IBOutlet weak var typeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var clearButton: UIButton!
    
    var isSelected = false
    var typeFilterDelegate: TypeFilterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        isSelected = true
        self.alwaysSelected(at: 2)
        typeFilterDelegate?.getInfoType(typeSegmentedControl.titleForSegment(at: typeSegmentedControl.selectedSegmentIndex)!, index: 2)
        self.hideMenu()
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        if isSelected == false {
            self.normalSelected(at: 2)
        }
        self.hideMenu()
    }

    @IBAction func clearButtonClicked(_ sender: Any) {
        isSelected = false
        self.changeMenu(title: "Type", status: .normal, at: 2)
        typeFilterDelegate?.getInfoType("", index: 2)
        self.hideMenu()
    }
    
    override func dropDownViewOpened() {
        clearButton.alpha = isSelected ? 1 : 0
    }
    
}

@objc protocol PriceFilterViewDelegate {
    func getInfoPrice(_ min: String, _ max: String, index: Int)
}

class PriceFilterView: YNDropDownView {
    
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var priceSlider: RangeSlider!
    
    var isSelected = false
    var priceFilterDelegate: PriceFilterViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func confirmButtonClicked(_ sender: Any) {
        isSelected = true
        self.alwaysSelected(at: 3)
        priceFilterDelegate?.getInfoPrice(String(priceSlider.lowerValue), String(priceSlider.upperValue), index: 3)
        self.hideMenu()
    }
    
    @IBAction func cancelButtonClicked(_ sender: Any) {
        if isSelected == false {
            self.normalSelected(at: 3)
        }
        self.hideMenu()
    }
    
    @IBAction func clearButtonClicked(_ sender: Any) {
        isSelected = false
        self.changeMenu(title: "Prix", status: .normal, at: 3)
        priceFilterDelegate?.getInfoPrice("0", "0", index: 3)
        self.hideMenu()
    }
    
    override func dropDownViewOpened() {
        clearButton.alpha = isSelected ? 1 : 0
    }
    
    
}

