//
//  FilterViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 02/08/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

@objc protocol FilterDelegate {
    func filterSelected()
}

class FilterViewController: UIViewController {
    
    @IBOutlet weak var bgBackBtn: UIVisualEffectView!
    @IBOutlet weak var bgResetBtn: UIVisualEffectView!
    @IBOutlet weak var sportCollectionView: UICollectionView!
    @IBOutlet weak var priceSlider: UpperRangeSlider!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var thanksLbl: UILabel!
    
    @IBOutlet weak var privateSwitch: UISwitch!
    @IBOutlet weak var sponsoredSwitch: UISwitch!
    @IBOutlet weak var maxDistanceSlider: UISlider!
    @IBOutlet weak var maxDistanceLabel: UILabel!
    let filter = CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm
    let sports = CacheService.shared.getDataFromDB(object: SportRealm())
    var delegate: FilterDelegate?
    var isChange = false
    
    let oldPrivate = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isPrivate
    let oldSponsored = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isSponsored
    let oldRadius = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value
    let oldMinPrice = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).minPrice.value
    let oldMaxPrice = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).maxPrice.value
    let oldSports = (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports
    var oldSportsCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        maxDistanceSlider.tintColor = ThemeManager.currentColor().mainColor
        maxDistanceSlider.value = Float(filter.radius.value ?? 50)
        maxDistanceLabel.text = String(Int(maxDistanceSlider.value)) + " " + NSLocalizedString("DISTANCE", comment: "")
        priceSlider.trackHighlightTintColor = ThemeManager.currentColor().mainColor
        priceLabel.text = String(Int(priceSlider.lowerValue)) + "-" + String(Int(priceSlider.upperValue)) + " " + NSLocalizedString("CURRENCY", comment: "")
        bgBackBtn.layer.cornerRadius = bgBackBtn.frame.height/2
        bgResetBtn.layer.cornerRadius = bgResetBtn.frame.height/2
        privateSwitch.isOn = filter.isPrivate
        sponsoredSwitch.isOn = filter.isSponsored
        priceSlider.lowerValue = Double(filter.minPrice.value!)
        priceSlider.upperValue = Double(filter.maxPrice.value!)
        displayPrice()
        setupCollectionView()
        oldSportsCount = oldSports.count
        addSwipeGestureRecognizer()
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        if !ambience { return .default }
        
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        priceSlider.updateLayerFramesAndPositions()
    }
    
    func displayPrice() {
        var lowerPrice = String(Int(priceSlider.lowerValue))
        var upperPrice = String(Int(priceSlider.upperValue))
        var currency = NSLocalizedString("CURRENCY", comment: "")
        if priceSlider.lowerValue == 0 {
            lowerPrice = NSLocalizedString("FREE", comment: "")
            currency = ""
        }
        if priceSlider.upperValue == 10 {
            upperPrice = "10+"
        }
        
        if priceSlider.lowerValue == priceSlider.upperValue {
            priceLabel.text = lowerPrice + " " + currency
        } else {
            priceLabel.text = lowerPrice + "-" + upperPrice + " " + NSLocalizedString("CURRENCY", comment: "")
        }
    }
    
    func setupCollectionView() {
        sportCollectionView.allowsMultipleSelection = true
        sportCollectionView.register(UINib(nibName: "SportCell", bundle: nil), forCellWithReuseIdentifier: "SportCell")
    }
    
    @IBAction func didTappedPrivate(_ sender: UISwitch) {
        isChange = sender.isOn != oldPrivate ? true : isChange
        
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isPrivate = sender.isOn
        }
    }
    
    @IBAction func didTappedSponsored(_ sender: UISwitch) {
        isChange = sender.isOn != oldSponsored ? true : isChange
        
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isSponsored = sender.isOn
        }
    }
    
    @IBAction func didSlideMaxDistance(_ sender: UISlider) {
        isChange = Int(sender.value) != oldRadius ? true : isChange
      
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value = Int(sender.value)
        }
        maxDistanceLabel.text = String(Int(maxDistanceSlider.value)) + " " + NSLocalizedString("DISTANCE", comment: "")
    }
    
    @IBAction func didSlidePrice(_ sender: UpperRangeSlider) {
        isChange = Int(sender.lowerValue) != oldMinPrice || Int(sender.upperValue) != oldMaxPrice ? true : false
        
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).minPrice.value = Int(sender.lowerValue)
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).maxPrice.value = Int(sender.upperValue)
        }
        displayPrice()
    }
    
    @IBAction func didCancelTapped(_ sender: Any) {
        try! CacheService.shared.getDatabase().write {
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isPrivate = oldPrivate
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).isSponsored = oldSponsored
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).radius.value = oldRadius
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).minPrice.value = oldMinPrice
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).maxPrice.value = oldMaxPrice
            (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports = oldSports
        }
    }
    
    @IBAction func didResetFilter(_ sender: UIButton) {
        CacheService.shared.deleteFromDb(object: CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm, completion: {
            CacheService.shared.addData(object: FilterRealm(id: 2, radius: 50, isPrivate: false, isSponsored: false, minPrice: 0, maxPrice: 10, date: Date())!)
            self.delegate?.filterSelected()
        })
    }
    
    func isSportsChange(_ sport1:[SportRealm], sport2: [SportRealm]) -> Bool {
        if oldSportsCount != sport2.count { return true }
        let result = zip(sport1, sport2).enumerated().filter() { $1.0 == $1.1 }.map{$0.0}
        if result.count > 0 { return true }
        return isChange
    }
    
    @IBAction func didValidateTapped(_ sender: UIButton) {
        isChange = isSportsChange(Array(oldSports), sport2: Array((CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports))
        if isChange { delegate?.filterSelected() }
    }
    
    
}

extension FilterViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sports.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        selected(cell: cell, sport: sports[indexPath.row] as! SportRealm)
        do {
            try CacheService.shared.getDatabase().write {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.append(sports[indexPath.row] as! SportRealm)
            }
        } catch let error {
            print("***SPORT FILTER ERROR: [\(error)]***")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        unselected(cell: cell)
        do {
            try CacheService.shared.getDatabase().write {
                (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.remove(at: (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.index(of: sports[indexPath.row] as! SportRealm)!)
            }
        } catch let error {
            print("***SPORT FILTER ERROR: [\(error)]***")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let cell = collectionView.cellForItem(at: indexPath) as! SportCell
        if cell.isEnable {
            let cell = collectionView.cellForItem(at: indexPath) as! SportCell
            unselected(cell: cell)
            do {
                try CacheService.shared.getDatabase().write {
                    (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.remove(at: (CacheService.shared.getDataFromDB(object: FilterRealm()).first as! FilterRealm).sports.index(of: sports[indexPath.row] as! SportRealm)!)
                }
            } catch let error {
                print("***SPORT FILTER ERROR: [\(error)]***")
            }
            return false
        }
        return true
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height-32, height: collectionView.frame.height-32)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SportCell", for: indexPath) as! SportCell
        let sports = CacheService.shared.getDataFromDB(object: SportRealm())
        guard case let sport as SportRealm = sports[indexPath.row] else {
            print("***SPORT CREATE EVENT ERROR***")
            return cell
        }
        cell.sportIV.image = UIImage(data: sport.logo!)
        cell.sportIV.addBlackFilter()
        cell.sportLbl.text = NSLocalizedString("\(sport.title!.uppercased())", comment: "")
        cell.sportUUID = sport.sportUUID
        
        if let sport = CacheService.shared.getDatabase().objects(FilterRealm.self).first?.sports.filter("sportUUID = '\(cell.sportUUID!)'").first {
            selected(cell: cell, sport: sport)
            cell.isEnable = true
        } else {
            cell.isEnable = false
            unselected(cell: cell)
        }

        return cell
    }
    
    func selected(cell: SportCell, sport: SportRealm) {
        cell.sportLbl.textColor = UIColor.hexColor(UInt((sport.codeColor.value)!))
        cell.sportIV.image = UIImage(data: sport.logo!)
    }
    
    func unselected(cell: SportCell) {
        cell.sportLbl.textColor = ThemeManager.currentTheme().unselectedFontColor
        cell.sportIV.addBlackFilter()
    }
    
}

extension FilterViewController {
    func addSwipeGestureRecognizer() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleGesture))
        tap.numberOfTouchesRequired = 2
        tap.numberOfTapsRequired = 5
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleGesture() {
        thanksLbl.alpha = thanksLbl.alpha == 0 ? 1 : 0
    }
}
