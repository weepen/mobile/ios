//
//  WeepenSearchBar.swift
//  Weepen
//
//  Created by Louis Cheminant on 23/01/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience


class EventCell: UITableViewCell {

    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var sportIV: UIImageView!
    @IBOutlet weak var sportLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var hourLbl: UILabel!
    @IBOutlet weak var nbDateLbl: UILabel!
    @IBOutlet weak var nbPlaceLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
            borderView.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            borderView.layer.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
            borderView.layer.shadowOffset = ThemeManager.currentTheme().shadowOffset
            borderView.layer.shadowRadius = ThemeManager.currentTheme().shadowRadius
        }
    }
    
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        if let view = borderView {
            view.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
        }
    }
    

}

