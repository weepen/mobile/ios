//
//  FilterCell.swift
//  Weepen
//
//  Created by Louis Cheminant on 8/9/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

class FilterSportCell: UICollectionViewCell {
    @IBOutlet weak var removeBtn: UIImageView! {
        didSet {
            removeBtn.image? = (removeBtn.image?.withRenderingMode(.alwaysTemplate))!
            removeBtn.tintColor = .white
        }
    }
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var sportPicture: UIImageView! {
        didSet {
            sportPicture.image? = (sportPicture.image?.withRenderingMode(.alwaysTemplate))!
            sportPicture.tintColor = .white
        }
    }
    
    
}

class FilterPrivateCell: UICollectionViewCell {
    @IBOutlet weak var removeBtn: UIImageView! {
        didSet {
            removeBtn.image? = (removeBtn.image?.withRenderingMode(.alwaysTemplate))!
        }
    }
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var descpLbl: UILabel!
    
    
}

class FilterSponsoredCell: UICollectionViewCell {
    @IBOutlet weak var removeBtn: UIImageView! {
        didSet {
            removeBtn.image? = (removeBtn.image?.withRenderingMode(.alwaysTemplate))!
        }
    }
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var descpLbl: UILabel!
}

class FilterRadiusCell: UICollectionViewCell {
    @IBOutlet weak var radiusLbl: UILabel!
    @IBOutlet weak var radiusBtn: UIButton!
}

