//
//  EventCollectionCell.swift
//  Weepen
//
//  Created by Louis Cheminant on 21/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

class EventCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var sportLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nbDayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var placeLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var borderView: UIView! {
        didSet {
            borderView.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
            borderView.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
            borderView.layer.shadowOpacity = ThemeManager.currentTheme().shadowOpacity
            borderView.layer.shadowOffset = ThemeManager.currentTheme().shadowOffset
            borderView.layer.shadowRadius = ThemeManager.currentTheme().shadowRadius
        }
    }
    
}
