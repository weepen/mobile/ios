//
//  ParticipantCollectionViewCell.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/02/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

class ParticipantCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nbParticipanLbl: UILabel!
    @IBOutlet weak var shadowView: UpperView!

    override var isSelected: Bool{
        didSet{
            if self.isSelected {
                self.nbParticipanLbl.textColor = .white
                self.shadowView.backgroundColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
                self.shadowView.layer.shadowColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)

            } else {
                self.nbParticipanLbl.textColor = #colorLiteral(red: 0.2901639342, green: 0.2902185321, blue: 0.2901567817, alpha: 1)
                self.shadowView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                self.shadowView.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
    }
    
}
