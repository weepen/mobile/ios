//
//  DateCollectionViewCell.swift
//  Weepen
//
//  Created by Louis Cheminant on 07/02/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

class FullDateCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var monthLbl: UILabel!
    @IBOutlet weak var nbLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    
    override var isSelected: Bool {
        didSet {
            monthLbl.textColor = isSelected == true ? .white : .black
            dayLbl.textColor = isSelected == true ? .white :  .black
            nbLbl.textColor = isSelected == true ? .white :  .black
        }
    }
    
    func populateItem(date: Date, highlightColor: UIColor, darkColor: UIColor, locale: Locale) {
        
        let mdateFormatter = DateFormatter()
        mdateFormatter.dateFormat = "MMMM"
        mdateFormatter.locale = locale
        monthLbl.text = mdateFormatter.string(from: date)
        monthLbl.textColor = isSelected == true ? .white : darkColor.withAlphaComponent(0.5)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE"
        dateFormatter.locale = locale
        dayLbl.text = dateFormatter.string(from: date).uppercased()
        dayLbl.textColor = isSelected == true ? .white : darkColor.withAlphaComponent(0.5)
        
        let numberFormatter = DateFormatter()
        numberFormatter.dateFormat = "d"
        numberFormatter.locale = locale
        nbLbl.text = numberFormatter.string(from: date)
        nbLbl.textColor = isSelected == true ? .white : darkColor
        
        contentView.layer.borderColor = darkColor.withAlphaComponent(0.2).cgColor
        contentView.backgroundColor = isSelected == true ? highlightColor : .white
    }
    
}

