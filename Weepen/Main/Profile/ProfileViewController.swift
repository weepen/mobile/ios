//
//  ProfileViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 09/10/2017.
//  Copyright © 2017 Louis Cheminant. All rights reserved.
//

import UIKit
import Magnetic
import MobileCoreServices
import SkyFloatingLabelTextField
import RealmSwift
import Firebase
import Ambience

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var ivPicture: UIImageView!
    @IBOutlet weak var btnPicture: UIButton!
    @IBOutlet weak var btnName: UIButton!
    @IBOutlet weak var locationLbl: UpperLabel!
    @IBOutlet weak var btnBirthday: UpperButton!
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var imageMail: UpperImageView!
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnTheme: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var pictureLoader: UIActivityIndicatorView!
    @IBOutlet weak var magneticView: MagneticView! {
        didSet {
            magnetic.magneticDelegate = self
            magnetic.backgroundColor = .clear
        }
    }
    
    let LocationMgr = LocationService.shared
    
    var allNode: [Node] = []
    var isModify = false
    var nbSport = 0
    var sportBeforeModify = List<SportRealm>()
    var sportBeforeModifyUids = [String]()
    var magnetic: Magnetic {
        return magneticView.magnetic
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
            errorUser()
            return
        }
        UserRealm().addSnapshotListenerUserPrivate(user.uuid!) { (date) in
            try! CacheService.shared.getDatabase().write {
                
                user.birthday = date
                let formatter = DateFormatter()
                formatter.dateStyle = .medium
                let langStr = Locale.current.languageCode
                if langStr == "fr" {
                    formatter.dateFormat = "dd MMM"
                } else {
                    formatter.dateFormat = "MMM dd"
                }
                self.btnBirthday.setTitle(formatter.string(from: date), for: .normal)
            }
        }
        UserRealm().addSnapshotListenerUserPublic(user.uuid!) { (name) in
            try! CacheService.shared.getDatabase().write {
                user.name = name
                self.btnName.setTitle((user.name)!, for: .normal)
            }
        }
        UserRealm().addSnapshotListenerUserSports(user.uuid!) { (sports) in
            try! CacheService.shared.getDatabase().write(
                transaction: {
                    CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.removeAll()
                    for sport in sports {
                        CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.append(sport)
                    }
            }, completion: {
                self.setupSportView(sports)
            })
        }
        
    }
    
    func errorUser() {
        let alertController = UIAlertController(title: NSLocalizedString("CAUTION_ERROR", comment: ""), message: NSLocalizedString("CAUTION_ERROR_LOGOUT", comment: ""), preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("CLOSE", comment: ""), style: .default, handler: { action in
            UserRealm().logoutUser { (result, vc) in
                if result {
                    self.present(vc, animated: true, completion: nil)
                } else {
                    self.showAlert(NSLocalizedString("CAUTION_ERROR", comment: ""), NSLocalizedString("CAUTION_ERROR_RESTART", comment: ""))
                }
            }
        }))
        self.present(alertController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pictureLoader.stopAnimating()
        pictureLoader.color = ThemeManager.currentTheme().selectedFontColor
        btnLogout.layer.cornerRadius = btnLogout.frame.size.height/2
        
        fillUserInfo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UserRealm().removeSnapshotListenerUserPrivate()
        UserRealm().removeSnapshotListenerUserPublic()
        UserRealm().removeSnapshotListenerUserSports()
        
    }
    
    func setupSportView(_ sports: List<SportRealm>) {
        var sportUUID = [String]()
        var nodeUUID = [String]()
        for node in allNode {
            nodeUUID.append(node.uuid!)
        }
        
        for sport in sports {
            sportUUID.append(sport.sportUUID!)
        }
        let sym = Set(sportUUID).symmetricDifference(Set(nodeUUID))
        for sport in sym {
            if nodeUUID.contains(sport) {
                for node in allNode {
                    if node.uuid == sport {
                        node.removeFromParent()
                        allNode.remove(at: allNode.firstIndex(of: node)!)
                    }
                }
            } else {
                if let sportNode = CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.filter("sportUUID = '\(sport)'").first {
                    let node = Node(text: NSLocalizedString("\(sportNode.title!.uppercased())", comment: ""), image: UIImage(data: sportNode.cover!), color: UIColor.hexColor(UInt((sportNode.codeColor.value)!)), uuid: sportNode.sportUUID!, radius: 40)
                    magnetic.addChild(node)
                    allNode.append(node)
                    node.isSelected = true
                }
                
            }
        }
        magneticView.isUserInteractionEnabled = false
    }
    
    func fillUserInfo() {
        guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
            errorUser()
            return
        }
        if user.picture != nil {
            ivPicture.image = UIImage.init(data: (user.picture!))
        } else {
            pictureLoader.startAnimating()
            user.getPictureUser(user) { (data) in
                self.ivPicture.image = UIImage.init(data: data)
                self.pictureLoader.stopAnimating()
            }
        }
        btnName.setTitle((user.name)!, for: .normal)
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.dateFormat = "dd MMM"
        btnBirthday.setTitle(formatter.string(from: (user.birthday)!), for: .normal)
        locationLbl.text = NSLocalizedString("LOADING", comment: "")
        LocationMgr.getCity { (city) in
            self.locationLbl.text = city
        }
        if user.email != nil {
            imageMail.image = UIImage(named: "mail")
            btnMail.setTitle(user.email, for: .normal)
        } else {
            imageMail.image = UIImage(named: "phone")
            btnMail.setTitle(user.phone, for: .normal)
        }
    }
    
    @IBAction func ThemeTapped(_ sender: UIButton) {
        if Ambience.currentState == .regular {
            btnTheme.setImage(UIImage(named: "moon"), for: .normal)
            Ambience.currentState = .invert
        } else {
            btnTheme.setImage(UIImage(named: "sun"), for: .normal)
            Ambience.currentState = .regular
        }
        if let tabBarController = self.tabBarController as? MainTabBarViewController {
            tabBarController.changeTheme(Ambience.currentState)
        }
    }
    
    @IBAction func logoutTapped(_ sender: Any?) {
        self.view.addSubview(LoadActivityIndicatorService.sharedLAI.addActivityIndicator(self.view.frame, isProgess: true))
        UserRealm().removeSnapshotListenerUserPublic()
        UserRealm().removeSnapshotListenerUserPrivate()
        UserRealm().removeSnapshotListenerUserSports()
        UserRealm().logoutUser { (result, vc) in
            LoadActivityIndicatorService.sharedLAI.removeActivityIndicator()
            if result {
                self.present(vc, animated: true, completion: nil)
            } else {
                self.showAlert(NSLocalizedString("CAUTION_ERROR", comment: ""), NSLocalizedString("CAUTION_ERROR_RESTART", comment: ""))
            }
        }
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        isModify = !isModify
        if isModify {
            for sport in (CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports)! {
                sportBeforeModifyUids.append(sport.sportUUID!)
            }
            magneticView.isUserInteractionEnabled = true
            nbSport = (CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.count)!
            for case let sport as SportRealm in CacheService.shared.getDataFromDB(object: SportRealm()) {
                if CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.filter("sportUUID = '\(sport.sportUUID!)'").count == 0 {
                    print(sport.title)
                    print(sport.cover)
                    print(sport.codeColor.value)
                    print(sport.sportUUID)
                    let node = Node(text: NSLocalizedString("\(sport.title!.uppercased())", comment: ""), image: UIImage(data: sport.cover!), color: UIColor.hexColor(UInt((sport.codeColor.value)!)), uuid: sport.sportUUID!, radius: 40)
                    magnetic.addChild(node)
                    allNode.append(node)
                }
            }
            btnEdit.layer.borderWidth = 3
            btnEdit.layer.borderColor = UIColor.white.cgColor
            btnPicture.isEnabled = true
            btnName.isEnabled = true
            btnBirthday.isEnabled = true
            btnDelete.alpha = 1
            btnDelete.isEnabled = true
        } else {
            magneticView.isUserInteractionEnabled = false
            
            for node in allNode {
                if CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.filter("sportUUID = '\(node.uuid!)'").count == 0 {
                    node.removeFromParent()
                    allNode.remove(at: allNode.firstIndex(of: node)!)
                }
            }
            
            var sportAfterModifyUid = [String]()
            for sport in (CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports)! {
                sportAfterModifyUid.append(sport.sportUUID!)
            }
            var sportsToAdd = [Any]()
            var sportsToRemove = [String]()
            let sym = Set(sportAfterModifyUid).symmetricDifference(Set(sportBeforeModifyUids))
            for sport in sym {
                if (sportBeforeModifyUids).contains(sport) {
                    sportsToRemove.append(sport)
                } else {
                    sportsToAdd.append(FirebaseService.shared.dbSport.document(sport))
                }
            }
            guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
                errorUser()
                return
            }
            if sportsToAdd.count > 0 { user.setSportInfo(user.uuid!, sports: sportsToAdd) { (result) in }}
            if sportsToRemove.count > 0 {user.removeSportInfo(user.uuid!, sports: sportsToRemove) { (result) in}}
            
            btnEdit.layer.borderColor = UIColor.clear.cgColor
            btnPicture.isEnabled = false
            btnName.isEnabled = false
            btnBirthday.isEnabled = false
            btnDelete.alpha = 0
            btnDelete.isEnabled = false
            sportBeforeModifyUids.removeAll()
        }
    }
    
    @IBAction func pictureTapped(_ sender: UIButton) {
        guard UIImagePickerController.isSourceTypeAvailable(.photoLibrary) else {
            return
        }
        
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
        
        present(imagePicker, animated: true)
    }
    
    @IBAction func changeName(_ sender: UIButton) {
        let alert = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
        let subview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width * 0.8, height: 70))
        subview.frame.origin.x = self.view.frame.minX
        let textfield = SkyFloatingLabelTextField()
        textfield.placeholder = NSLocalizedString("FIRST_NAME", comment: "")
        textfield.frame = CGRect(x: subview.frame.width/2-(self.view.frame.width * 0.6)/2-10, y: 10, width: self.view.frame.width * 0.6, height: 50)
        textfield.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
        subview.addSubview(textfield)
        alert.customSubview = subview
        alert.addButton(NSLocalizedString("MODIFY", comment: ""), textColor: .white, showDurationStatus: true) {
            if !textfield.text!.isName() && (textfield.text?.count)! < 2 && (textfield.text?.count)! > 64 {
                textfield.errorMessage = NSLocalizedString("CAUTION", comment: "")
                textfield.shake()
            } else {
                self.changeInfo(textfield.text!, "firstName", "PUBLIC", completion: { result -> Void in
                    if result {
                        self.btnName.setTitle(textfield.text!, for: .normal)
                        try! CacheService.shared.getDatabase().write {
                            guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
                                self.errorUser()
                                return
                            }
                            user.name = textfield.text!
                        }
                    }
                })
                alert.appearance.shouldAutoDismiss = true
                alert.dismiss(animated: true, completion: nil)
            }
        }
        alert.addButton(NSLocalizedString("CANCEL", comment: ""), textColor: .white, showDurationStatus: true) {
            alert.appearance.shouldAutoDismiss = true
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit(NSLocalizedString("CHANGE_FIRST_NAME", comment: ""), subTitle: "")
    }
    
    @IBAction func changeBirthday(_ sender: UIButton) {
        let alert = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
        let subview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width * 0.8, height: 70))
        subview.frame.origin.x = self.view.frame.minX
        let datePicker = UIDatePicker(frame: CGRect(x: subview.frame.width/2-(self.view.frame.width * 0.8)/2-8, y: 10, width: self.view.frame.width * 0.8, height: 70))
        datePicker.setValue(ThemeManager.currentTheme().selectedFontColor, forKeyPath: "textColor")
        datePicker.datePickerMode = .date
        datePicker.minimumDate = limitDate().1
        datePicker.maximumDate = limitDate().0
        
        guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
            errorUser()
            return
        }
        datePicker.date = (user.birthday!)
        subview.addSubview(datePicker)
        alert.customSubview = subview
        alert.addButton(NSLocalizedString("MODIFY", comment: ""), textColor: .white, showDurationStatus: true) {
            
            self.changeInfo(datePicker.date.iso8601, "dateOfBirth", "PRIVATE", completion: { result -> Void in
                if result {
                    try! CacheService.shared.getDatabase().write { user.birthday = datePicker.date }
                    self.btnBirthday.setTitle(datePicker.date.toString(dateFormat: "dd MMM"), for: .normal)
                    alert.appearance.shouldAutoDismiss = true
                    
                }
            })
            alert.appearance.shouldAutoDismiss = true
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addButton(NSLocalizedString("CANCEL", comment: ""), textColor: .white, showDurationStatus: true) {
            alert.appearance.shouldAutoDismiss = true
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit(NSLocalizedString("CHANGE_BIRTH", comment: ""), subTitle: "")
    }
    
    func limitDate() -> (Date, Date) {
        let gregorian = Calendar(identifier: .gregorian)
        var components = DateComponents()
        components.year = -18
        let minDate = gregorian.date(byAdding: components, to: Date())
        components.year = -100
        let maxDate = gregorian.date(byAdding: components, to: Date())
        return (minDate!, maxDate!)
    }
    
    @IBAction func deleteAccountTapped(_ sender: UIButton) {
        let alert = AlertView(appearance: ThemeManager.getAppearanceAlert(self.view.frame.width))
        let subview = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width * 0.8, height: 120))
        subview.frame.origin.x = self.view.frame.minX
        let label = UILabel(frame: CGRect(x: subview.frame.width/2-(self.view.frame.width * 0.6)/2-10, y: 10, width: self.view.frame.width * 0.6, height: 50))
        label.text = NSLocalizedString("DELETE_ACCOUNT_MESSAGE", comment: "")
        label.numberOfLines = 0
        label.font = UIFont.init(name: ThemeManager.currentTheme().fontNameBold, size: 17)
        label.textColor = ThemeManager.currentTheme().selectedFontColor
        label.adjustsFontSizeToFitWidth = true
        subview.addSubview(label)
        let textfield = SkyFloatingLabelTextField(frame: CGRect(x: subview.frame.width/2-(self.view.frame.width * 0.6)/2-10, y: 60, width: self.view.frame.width * 0.6, height: 50))
        textfield.placeholder = NSLocalizedString("DELETE", comment: "")
        textfield.autocapitalizationType = .words
        textfield.keyboardType = .default
        textfield.layer.cornerRadius = ThemeManager.currentTheme().cornerRadius
        subview.addSubview(textfield)
        alert.customSubview = subview
        alert.addButton(NSLocalizedString("DELETE", comment: ""), textColor: .white, showDurationStatus: true) {
            if textfield.text! == NSLocalizedString("DELETE", comment: "") {
                self.deleteAccount()
                alert.appearance.shouldAutoDismiss = true
                alert.dismiss(animated: true, completion: nil)
            } else {
                textfield.errorMessage = NSLocalizedString("CAUTION", comment: "")
                textfield.shake()
            }
        }
        alert.addButton(NSLocalizedString("CANCEL", comment: ""), textColor: .white, showDurationStatus: true) {
            alert.appearance.shouldAutoDismiss = true
            alert.dismiss(animated: true, completion: nil)
        }
        alert.showEdit(NSLocalizedString("DELETE_ACCOUNT", comment: ""), subTitle: "")
    }
    
    func deleteAccount() {
        AuthService.sharedAuth.deleteAccount(completion: { (result) in
            if result {
                UserRealm().logoutUser { (result, vc) in
                    self.present(vc, animated: true)
                }
            }
        })
    }
    
    func changeInfo(_ info: String, _ key: String, _ document: String, completion:@escaping (Bool) -> ()) {
        guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
            errorUser()
            return
        }
        let refPublic = FirebaseService.shared.dbUser.document(user.uuid!).collection("visibility").document(document)
        let infosPublic = [key : info] as [String : Any]
        refPublic.setData(infosPublic) { dataError in
            if let error = dataError {
                print(error)
            } else {
                completion(true)
            }
        }
    }
    
    
}

extension ProfileViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        defer {
            picker.dismiss(animated: true)
        }
        guard let image = info[.originalImage] as? UIImage else {
            return
        }
        ivPicture.image = image
        try! CacheService.shared.getDatabase().write {
            guard let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm else {
                errorUser()
                return
            }
            user.picture = self.ivPicture.image!.jpegData(compressionQuality: 1)
            user.setPictureUser(user.uuid!, user)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        defer {
            picker.dismiss(animated: true)
        }
    }
    
    
}

// MARK: - MagneticDelegate
extension ProfileViewController: MagneticDelegate {
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        if isModify {
            try! CacheService.shared.getDatabase().write { CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.append(CacheService.shared.getDataFromDB(object: SportRealm()).filter("sportUUID = '\(node.uuid!)'").first as! SportRealm) }
            btnEdit.isEnabled = true
        }
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        if isModify {
            if let sport = CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.filter("sportUUID = '\(node.uuid!)'").first {
                try! CacheService.shared.getDatabase().write { CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.remove(at: (CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.index(of: sport)!)!) }
            }
        }
        if CacheService.shared.getDatabase().objects(UserRealm.self).first?.sports.count == 0 {
            btnEdit.isEnabled = false
        }
    }
    
}
