//
//  MainTabBarViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 30/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Ambience

class MainTabBarViewController: UITabBarController {
    
    let LocationMgr = LocationService.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBar.tintColor = ThemeManager.currentColor().mainColor
        NotificationCenter.default.addObserver(self, selector: #selector(applicationIsActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch LocationMgr.isAuthorized() {
        case 0:
            performSegue(withIdentifier: "noLocation", sender: nil)
        case -1:
            LocationMgr.askAuthorizationLocation()
        default:break
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func applicationIsActive() {
        switch LocationMgr.isAuthorized() {
        case 0:
            performSegue(withIdentifier: "noLocation", sender: nil)
        default:break
        }
    }
    
    func changeTheme(_ currentState: AmbienceState) {
        for (index, item) in self.tabBar.items!.enumerated() {
            if currentState == .invert {
                switch index {
                case 0:
                    item.image = UIImage(named: "Main-B")?.withRenderingMode(.alwaysOriginal)
                case 1:
                    item.image = UIImage(named: "Agenda-B")?.withRenderingMode(.alwaysOriginal)
                case 2:
                    item.image = UIImage(named: "Profile-B")?.withRenderingMode(.alwaysOriginal)
                case 3:
                    item.image = UIImage(named: "")?.withRenderingMode(.alwaysOriginal)
                default:
                    item.image = UIImage()
                }
            } else {
                switch index {
                case 0:
                    item.image = UIImage(named: "Main")?.withRenderingMode(.alwaysTemplate)
                    
                case 1:
                    item.image = UIImage(named: "Agenda")?.withRenderingMode(.alwaysTemplate)
                case 2:
                    item.image = UIImage(named: "Profile")?.withRenderingMode(.alwaysTemplate)
                case 3:
                    item.image = UIImage(named: "")?.withRenderingMode(.alwaysTemplate)
                default:
                    item.image = UIImage()
                }
            }
        }
        
        
        UIView.transition(with: self.tabBar, duration: 1, options: [.beginFromCurrentState, .transitionCrossDissolve], animations: {
            self.tabBar.barStyle = currentState == .invert ? .black : .default
            self.tabBar.barTintColor = currentState == .invert ? .hexColor(0x0E0E15) : .white
            self.tabBar.unselectedItemTintColor = ThemeManager.currentTheme().unselectedFontColor
        }, completion: nil)
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        
        guard let currentState = notification.userInfo?["currentState"] as? AmbienceState else { return }
        
        changeTheme(currentState)
    }
    
    
}

extension MainTabBarViewController: LocationUpdateProtocol {
    func locationDidUpdateToLocation(latitude: Double, longitude: Double) {}
    
    func locationDidChangeAuthorization(status: Int) {
        switch status {
        case 0:
            performSegue(withIdentifier: "noLocation", sender: nil)
        default:
            break
        }
    }
}
