//
//  ProfileParticipantViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/4/18.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import Magnetic
import MapKit
import RealmSwift
import Ambience


class ProfileParticipantViewController: UIViewController {
    
    @IBOutlet weak var pictureLoader: UIActivityIndicatorView!
    @IBOutlet weak var ivParticipant: UpperImageView!
    @IBOutlet weak var lblName: UpperLabel!
    @IBOutlet weak var lblMail: UpperLabel!
    @IBOutlet weak var lblLocation: UpperLabel!
    @IBOutlet weak var sportView: MagneticView! {
        didSet {
            magnetic.backgroundColor = .clear
        }
    }
    
    var isParticipant = false
    var participant: ParticipantRealm?
    var owner: OwnerRealm!
    var magnetic: Magnetic {
        return sportView.magnetic
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.setNeedsStatusBarAppearanceUpdate()
        if (participant != nil) {
            isParticipant = true
        }
        
        fillInfoParticipant()
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        pictureLoader.color = ThemeManager.currentTheme().selectedFontColor
        if isParticipant { getPartitipantInfo() } else { self.setupSportView(owner.sports) }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func getPartitipantInfo() {
        ParticipantRealm().getSportParticipantInfo(participant!.uuid!) { (sports) in
            if let participant = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(self.participant!.uuid!)'").first as? ParticipantRealm {
                try! CacheService.shared.getDatabase().write({
                    guard !participant.isInvalidated else { return }
                    participant.sports = sports
                    self.setupSportView(sports)
                })
            }
        }
    }
    
    func fillInfoParticipant() {
        lblName.text = isParticipant ? participant?.name : owner.name
        lblMail.text = NSLocalizedString("NOT_DEFINE", comment: "")
        
        if let data = (isParticipant ? participant?.picture : owner.picture) {
            pictureLoader.stopAnimating()
            ivParticipant.image = UIImage(data: data)
        } else {
            ParticipantRealm().getPictureParticipant((isParticipant ? participant?.uuid : owner.uuid)!, isParticipant ? participant?.urlPicture : owner.urlPicture) { (data, url) in
                DispatchQueue.main.async {
                    
                    try! CacheService.shared.getDatabase().write(transaction: {
                        if self.isParticipant {
                            if let participant = CacheService.shared.getDataFromDB(object: ParticipantRealm()).filter("uuid = '\(self.participant!.uuid!)'").first as? ParticipantRealm {
                                self.pictureLoader.stopAnimating()
                                guard !participant.isInvalidated else { return }
                                participant.picture = data
                                participant.urlPicture = url
                                self.ivParticipant.image = UIImage(data: data)
                            }
                        } else {
                            if let owner = CacheService.shared.getDataFromDB(object: OwnerRealm()).filter("uuid = '\(self.owner!.uuid!)'").first as? OwnerRealm {
                                self.pictureLoader.stopAnimating()
                                guard !owner.isInvalidated else { return }
                                owner.picture = data
                                owner.urlPicture = url
                                self.ivParticipant.image = UIImage(data: data)
                            }
                            
                        }
                    }, completion: { return })
                }
            }
        }
        self.lblLocation.text = NSLocalizedString("LOADING", comment: "")
        getCity { (city) in
            self.lblLocation.text = city
        }
    }
    
    func getCity(completion:@escaping (String) -> ()) {
        let geoCoder = CLGeocoder()
        let latitude =  isParticipant ? participant?.latitude.value : owner.latitude.value
        let longitude =  isParticipant ? participant?.longitude.value : owner.longitude.value
        let location = CLLocation(latitude: latitude!, longitude: longitude!)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if let city = placemarks?[0].locality {
                completion(city)
            }
        })
    }
    
    func setupSportView(_ sports: List<SportRealm>) {
        for sport in sports {
            var cover: UIImage? = nil
            if let image = sport.cover {
                cover = UIImage(data: image)!
            }
            let node = Node(text: NSLocalizedString("\(sport.title!.uppercased())", comment: ""), image: cover, color: UIColor.hexColor(UInt((sport.codeColor.value)!)), uuid: sport.sportUUID!, radius: 40)
            node.isSelected = true
            node.fillColor = .clear
            magnetic.addChild(node)
        }
        sportView.isUserInteractionEnabled = false
    }
    
}

class NavigationParticipantViewController: UINavigationController {
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
