//
//  DescriptionView.swift
//  Weepen
//
//  Created by Louis Cheminant on 10/02/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit

class DescriptionView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("DescriptionView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
}
