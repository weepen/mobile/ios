//
//  ParticipantsView.swift
//  Weepen
//
//  Created by Louis Cheminant on 13/06/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import RealmSwift

protocol ParticipantsViewDelegate {
    func didSelectParticipant(_ participant: ParticipantRealm?)
}

class ParticipantsView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nbParticipantLbl: UILabel!
    
    var event: EventRealm!
    var uuids: [String] = []
    var uuidsParticpant: [String] = []
    var count = 0
    var delegate: ParticipantsViewDelegate?
    var filteredParticipants = [ParticipantRealm]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("ParticipantsView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        let nib = UINib(nibName: "CellParticipants", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CellParticipants")
       
    }
    
    func setUUIDs(_ uuids: [String]) {
        self.uuids = uuids
        nbParticipantLbl.text = "\(uuids.count)/\(event.nbMaxParticipant.value!)"
        createParticipantArray()
        tableView.hideSearchBar()
    }
    
    func createParticipantArray() {
        var participants = [ParticipantRealm]()
        for uuid in self.uuids where uuid != event.owner?.uuid! {
            if let participant = ParticipantRealm(uid: uuid) {
                participants.append(participant)
            }
        }
        let (removeParticipants, addParticipants) = ParticipantRealm().orderParticipants(participants, event)
        self.removeCell(removeParticipants)
        self.addCell(participants, addParticipants)
    }
    
    func removeCell(_ participants: [Object]) {
        if participants.count > 0 {
            var removeIndexes: [IndexPath] = []
            for case let participant as ParticipantRealm in participants {
                if let index = (Array(event.participants)).index(where: {$0.uuid! == participant.uuid!}) {
                    removeIndexes.append(IndexPath(row: index+1, section: 0))
                }
            }
            ParticipantRealm().removeParticipants(participants, event)
            tableView.deleteRows(at: removeIndexes, with: .fade)
        }
    }
    
    func addCell(_ newParticipants: [ParticipantRealm], _ participants: [Object]) {
        if participants.count > 0 {
            var addIndexes: [IndexPath] = []
            for i in 1...participants.count {
                addIndexes.append(IndexPath(row: (tableView.numberOfRows(inSection: 0)+i)-1, section: 0))
            }
            ParticipantRealm().addParticipants(participants, event)
          
            tableView.insertRows(at: addIndexes, with: .fade)
        }
    }
    
}

extension ParticipantsView: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let event = CacheService.shared.getDatabase().objects(EventRealm.self).filter("uid = '\(event.uid)'").first {
            return event.participants.count + 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CellParticipants", for: indexPath) as? CellParticipants, let item = CacheService.shared.getDatabase().objects(EventRealm.self).filter("uid = '\(event.uid)'").first else { return UITableViewCell() }
        if indexPath.row == 0 {
            if item.owner?.uuid == (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid {
                cell.participantName.text = NSLocalizedString("YOU", comment: "")
                return addUserAsParticipant(cell)
            } else {
                if let name = item.owner!.name {
                    cell.participantName.text = name
                    cell.loadParticipantName.hideLoader()
                    cell.ivArrow.alpha = 1
                } else { cell.loadParticipantPicture.hideLoader() }
                if let picture = item.owner!.picture {
                    cell.participantPicture.image = UIImage.init(data: (picture))
                    cell.loadParticipantPicture.hideLoader()
                } else { cell.loadParticipantPicture.showLoader(ThemeManager.currentTheme().downView) }
                return cell
            }
        }
        if event.participants[indexPath.row-1].uuid ==  (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid {
            cell.participantName.text = NSLocalizedString("YOU", comment: "")
            return addUserAsParticipant(cell)
        } else {
            return addNewParticipant(cell, indexPath, event)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 58
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? CellParticipants {
            if cell.ivArrow.alpha == 1 {
                if indexPath.row == 0 {
                    delegate?.didSelectParticipant(nil)
                } else {
                    delegate?.didSelectParticipant(event.participants[indexPath.row-1])
                }
            }
        }
    }
    
}


extension ParticipantsView {
    
    func addNewParticipant(_ cell: CellParticipants, _ indexPath: IndexPath, _ event: EventRealm) -> CellParticipants {
        cell.loadParticipantName.showLoader(ThemeManager.currentTheme().downView)
        cell.loadParticipantPicture.showLoader(ThemeManager.currentTheme().downView)
        guard indexPath.row <= self.tableView.numberOfRows(inSection: 0), let participant = CacheService.shared.getDataFromDB(object: ParticipantRealm(), "uuid").filter("uuid = '\(event.participants[indexPath.row-1].uuid!)'").first as? ParticipantRealm else {
            return cell
        }
        if let name = participant.name {
            cell.participantName.text = name
            cell.loadParticipantName.hideLoader()
            cell.ivArrow.alpha = 1
        } else {
            ParticipantRealm().getParticipantInfo(participant.uuid!) { (name, latitude, longitude) in
                do {
                    try CacheService.shared.getDatabase().write {
                        participant.name = name
                        participant.latitude.value = latitude
                        participant.longitude.value = longitude
                        cell.participantName.text = name
                        cell.ivArrow.alpha = 1
                        cell.loadParticipantName.hideLoader()
                    }
                } catch {
                    print("error")
                }
            }
        }
        if let data = participant.picture {
            cell.participantPicture.image = UIImage.init(data: data)
            cell.loadParticipantPicture.hideLoader()
        } else {
            ParticipantRealm().getPictureParticipant(participant.uuid!, participant.urlPicture) { (data, url) in
                DispatchQueue.main.async {
                do {
                        try CacheService.shared.getDatabase().write {
                            guard !participant.isInvalidated else { return }
                            participant.picture = data
                            participant.urlPicture = url
                            cell.participantPicture.image = UIImage.init(data: data)
                            cell.loadParticipantPicture.hideLoader()
                        }
                    
                } catch {
                    print("error")
                }
                }
            }
        }
        return cell
    }
    
    func addUserAsParticipant(_ cell: CellParticipants) -> CellParticipants {
        if let data = (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).picture {
            cell.participantPicture.image = UIImage.init(data: (data))
            cell.ivArrow.alpha = 0
            cell.loadParticipantPicture.hideLoader()
        } else {
            cell.loadParticipantPicture.showLoader(ThemeManager.currentTheme().downView)
            UserRealm().getPictureUser((CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm), completion: { (data) in
                cell.participantPicture.image = UIImage.init(data: data)
                cell.loadParticipantPicture.hideLoader()
            })
        }
        return cell
    }
    
    
}


class CellParticipants: UITableViewCell {
    @IBOutlet weak var loadParticipantPicture: UIView! {
        didSet {
            loadParticipantPicture.layer.cornerRadius = loadParticipantPicture.frame.height/2
        }
    }
    @IBOutlet weak var loadParticipantName: UIView! {
        didSet {
            loadParticipantName.layer.cornerRadius = loadParticipantName.frame.height/2
        }
    }
    @IBOutlet weak var participantPicture: UIImageView! {
        didSet {
            participantPicture.layer.cornerRadius = participantPicture.frame.height/2
        }
    }
    @IBOutlet weak var participantName: UILabel! {
        didSet {
            participantName.layer.cornerRadius = participantName.frame.height/2
        }
    }
    @IBOutlet weak var ivArrow: UIImageView! {
        didSet {
            ivArrow.transform = ivArrow.transform.rotated(by: CGFloat(Double.pi / 1))
            ivArrow.alpha = 0
        }
    }
}
