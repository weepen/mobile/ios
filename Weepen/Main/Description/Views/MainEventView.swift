//
//  MainEventView.swift
//  Weepen
//
//  Created by Louis Cheminant on 02/02/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import MapKit

class MainEventView: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var ownerIV: UIImageView!
    @IBOutlet weak var streetLbl: UILabel!
    @IBOutlet weak var zipLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var lengthLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var loadOwner: UIView!
    @IBOutlet weak var loadOwnerIv: UIView!
    @IBOutlet weak var loadStreet: UIView!
    @IBOutlet weak var loadZip: UIView!
    @IBOutlet weak var heightOwnerViewCst: NSLayoutConstraint!
    @IBOutlet weak var widthOwnerViewCst: NSLayoutConstraint!
    @IBOutlet weak var heightAddressCst: NSLayoutConstraint!
    @IBOutlet weak var heightDateCst: NSLayoutConstraint!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
        ownerIV.layer.cornerRadius = ownerIV.frame.height/2
        if UIScreen.main.bounds.height == 568.0 {
            heightOwnerViewCst.constant = 35
            widthOwnerViewCst.constant = 35
            heightAddressCst.constant = 63
            ownerIV.layer.cornerRadius = 35/2
            heightDateCst.constant = 55
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("MainEventView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

}
