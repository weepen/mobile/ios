//
//  DescriptionViewController.swift
//  Weepen
//
//  Created by Louis Cheminant on 21/03/2018.
//  Copyright © 2018 Louis Cheminant. All rights reserved.
//

import UIKit
import CHIPageControl
import MapKit
import ListPlaceholder
import Ambience
import SkyFloatingLabelTextField
import Firebase
import RealmSwift
import FirebaseFirestore

protocol DescriptionViewDelegate {
    func getCell(cell: EventCell, _ isUnregister: Bool)
}

class DescriptionViewController: UIViewController {
    
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var titleEvent: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: CHIPageControlAleppo!
    @IBOutlet weak var whenResume: UILabel!
    @IBOutlet weak var whereResume: UILabel!
    @IBOutlet weak var priceResume: UILabel!
    @IBOutlet weak var bgButton: UIVisualEffectView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var resButton: UpperButton!
    @IBOutlet weak var loadBtn: UIView!
    @IBOutlet weak var heightMapCst: NSLayoutConstraint!
    @IBOutlet weak var widthButtonCst: NSLayoutConstraint!
    @IBOutlet weak var heightButtonCst: NSLayoutConstraint!
    @IBOutlet weak var heightPageControlCst: NSLayoutConstraint!
    @IBOutlet weak var heightScrollCst: NSLayoutConstraint!
    @IBOutlet weak var heightCloseButtonCst: NSLayoutConstraint!
    
    var cell = EventCell()
    let mainView = MainEventView()
    let descriptionView = DescriptionView()
    let participantsView = ParticipantsView()
    var pointAnnotation:MKPointAnnotation!
    let distanceFormatter = MKDistanceFormatter()
    var userLocation: CLLocation?
    var alert: AlertViewResponder!
    var isFirstTime = true
    var participantToPush: ParticipantRealm?
    private var listener : ListenerRegistration!
    var eventType: EventType = .display
    
    var delegate: DescriptionViewDelegate?
    
    var event: EventRealm!
    let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm
    var isUnregister = false
    fileprivate func baseQuery(_ uid: String) -> Query {
        return FirebaseService.shared.dbEvent.document(uid).collection("participants")
    }
    fileprivate var query: Query? {
        didSet {
            if let listener = listener {
                listener.remove()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationController = self.navigationController {
            if UIDevice.current.userInterfaceIdiom == .pad {
                navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
                navigationController.navigationBar.shadowImage = UIImage()
                navigationController.navigationBar.isTranslucent = true
                navigationController.view.backgroundColor = .clear
            } else {
                navigationController.isNavigationBarHidden = true
            }
        }
        if self.eventType == .participate {
            heightButtonCst.constant = 0
        }
        getAllParticipantsListener() { (result, uuids) in
            if result {
                self.participantsView.setUUIDs(uuids)
                if self.eventType == .display {
                    if self.user.uuid != self.event.owner?.uuid && self.isFirstTime {
                        self.isFirstTime = false
                        if uuids.filter({ $0 == (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid }).count > 0 {
                            self.showCancelBtn()
                        } else {
                            if uuids.count == self.event.nbMaxParticipant.value {
                                self.showFullBtn()
                            } else {
                                self.showRegisterBtn()
                            }
                        }
                    }
                    if uuids.count == self.event.nbMaxParticipant.value {
                        self.showFullBtn()
                    }
                }
                
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if event == nil { showAlert("Error Event in [DescriptionViewController]", "event is nil") {
            self.dismiss(animated: true, completion: nil)
            }}
        
        setupView()
        setupPageController()
        
        if eventType == .display {
            loadBtn.showLoader(ThemeManager.currentTheme().downView)
        }

        updateBtnResevation()

        
        participantsView.delegate = self
        participantsView.event = event
        self.query = baseQuery(event.uid)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.listener.remove()
        if let navigationController = self.navigationController {
            navigationController.isNavigationBarHidden = false
        }
    }
    
    func getAllParticipantsListener(completion:@escaping (Bool, [String]) -> ()) {
        self.listener = query?.addSnapshotListener { (queryDocument, error) in
            var particpantsUUID: [String] = []
            guard let document = queryDocument, error == nil else {
                return
            }
            for document in document.documents {
                if let ref: DocumentReference = document.data()["ref"] as! DocumentReference? {
                    let uidParticipant = ref.path.replacingOccurrences(of: FirebaseService.shared.pathUser, with: "", options: .literal, range: nil)
                    particpantsUUID.append(uidParticipant)
                }
            }
            completion(true, particpantsUUID)
        }
    }
    
    
    func setupPageController() {
        pageControl.tintColors = [#colorLiteral(red: 0.9410842061, green: 0.941242218, blue: 0.9668614268, alpha: 1), #colorLiteral(red: 0.9410842061, green: 0.941242218, blue: 0.9668614268, alpha: 1), #colorLiteral(red: 0.9410842061, green: 0.941242218, blue: 0.9668614268, alpha: 1)]
        
        var pageWidth = UIScreen.main.bounds.width-32
        if pageWidth > 500 {
            pageWidth = 500
            heightCloseButtonCst.constant = 0
        }
        var pageHeight = scrollView.bounds.height
        if UIScreen.main.bounds.height == 568.0 { pageHeight = 231 }
        
        scrollView.contentSize = CGSize(width: 3*pageWidth, height: pageHeight)
        
        mainView.frame = CGRect(x: 0, y: 0, width: pageWidth, height: pageHeight)
        descriptionView.frame = CGRect(x: pageWidth, y: 0, width: pageWidth, height: pageHeight)
        participantsView.frame = CGRect(x: pageWidth*2, y: 0, width: pageWidth, height: pageHeight)
        
        scrollView.addSubview(mainView)
        scrollView.addSubview(descriptionView)
        scrollView.addSubview(participantsView)
    }
    
    func setupView() {
        mapView.delegate = self
        bgButton.layer.cornerRadius = bgButton.frame.height/2
        displayWay((userLocation?.coordinate)!, CLLocationCoordinate2D(latitude: event.latitude.value!, longitude: event.longitude.value!))
        fillView()
    }
    
    func updateBtnResevation() {
        if let user = CacheService.shared.getDataFromDB(object: UserRealm()).first as? UserRealm {
            if user.uuid == event.owner?.uuid {
                self.mainView.nameLbl.text = NSLocalizedString("YOU", comment: "")
                self.mainView.ownerIV.image = UIImage(data: (user.picture)!)
                if eventType == .display { self.showDeleteBtn() }
            } else {
                guard event.owner?.name != nil, event.owner?.picture != nil, event.owner!.sports.count > 0 else {
                    getOwner()
                    return
                }
                self.mainView.nameLbl.text = event.owner?.name
                self.mainView.ownerIV.image = UIImage(data: (event.owner?.picture)!)
            }
        } else {
            showAlert("Realm User Error", "Error: No user in cache")
        }
    }
    
    func getOwner() {
        guard let curentEvent = EventRealm().getCurrentEvent(self.event.uid) else { return }
        mainView.loadOwner.showLoader(ThemeManager.currentTheme().downView)
        mainView.loadOwnerIv.showLoader(ThemeManager.currentTheme().downView)
        OwnerRealm().getOwnerInfo((event.owner?.uuid!)!, completion: { (name, latitude, longitude) in
            OwnerRealm().getSportOwnerInfo((self.event.owner?.uuid!)!, completion: { (sports) in
                DispatchQueue.main.async  {
                    try! CacheService.shared.getDatabase().write {
                        guard !curentEvent.isInvalidated else { return }
                        curentEvent.owner?.name = name
                        curentEvent.owner?.latitude.value = latitude
                        curentEvent.owner?.longitude.value = longitude
                        curentEvent.owner?.sports.append(objectsIn: sports)
                        self.participantsView.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                        self.mainView.nameLbl.text = name
                        self.mainView.loadOwner.hideLoader()
                    }
                }
                OwnerRealm().getPictureOwner((self.event.owner?.uuid!)!, self.event.owner!.urlPicture, completion: { (data, url) in
                    DispatchQueue.main.async  {
                        try! CacheService.shared.getDatabase().write {
                            guard !curentEvent.isInvalidated else { return }
                            curentEvent.owner?.picture = data
                            curentEvent.owner?.urlPicture = url
                            self.participantsView.tableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                            self.mainView.ownerIV.image = UIImage(data: data)
                            self.mainView.loadOwnerIv.hideLoader()
                        }
                    }
                })
            })
        })
    }
    
    func fillView() {
        logo.image = UIImage(data: (event.sport?.logo)!)
        titleEvent.text = event.title?.capitalizingFirstLetter()
        if let date = event.dateStart {
            whenResume.text = date.offset(from: (Date())).capitalizingFirstLetter()
        }
        whereResume.text = getLocation()
        if event.price.value == 0.0 {
            priceResume.text = NSLocalizedString("FREE", comment: "")
        } else {
            priceResume.text = "\(String(describing: event?.price)) \(String(describing: event?.currency))"
        }
        descriptionView.descriptionTextView.text = event?.resume
        let (start_hour, end_hour, date) = getDate()
        mainView.dateLbl.text = "\(start_hour) - \(end_hour)"
        mainView.dayLbl.text = date
        let interval = event.dateEnd!.timeIntervalSince(event.dateStart!)
        secondsToHoursMinutesSeconds(Int(interval), result: { (h, m, s) in
            self.mainView.lengthLbl.text = "\(h)\(NSLocalizedString("H", comment: ""))\(self.timeText(m))"
        })
        if let part = event.nbParticipant.value, let maxP = event.nbMaxParticipant.value {
            participantsView.nbParticipantLbl.text = "\(part)/\(maxP)"
        }
        self.mainView.streetLbl.text = event.name! != "" ? "\(event.name!)" : "\(event.thoroughfare!)"
        self.mainView.zipLbl.text = "\(event.zip!) \(event.city!), \(event.country!)"
        
        self.pointAnnotation = MKPointAnnotation()
        self.pointAnnotation.title = event.name! != "" ? "\(event.name!)" : "\(event.thoroughfare!)"
        self.pointAnnotation.subtitle = "\(event.zip!) \(event.city!), \(event.country!)"
        self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(truncating: NSNumber(value: self.event.latitude.value!)), longitude: CLLocationDegrees(truncating: NSNumber(value: self.event.latitude.value!)))
        self.mapView.addAnnotation(self.pointAnnotation)
        self.mapView.selectAnnotation(self.pointAnnotation, animated: true)
    }
    
    @IBAction func didReserve(_ sender: UIButton) {
        if self.resButton.title(for: .normal) == NSLocalizedString("BOOK", comment: "") {
            loadBtn.showLoader(ThemeManager.currentTheme().downView)
            ParticipantRealm().setParticipant(event.uid, (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!, completion: { (result) in
                if result {
                    try! CacheService.shared.getDatabase().write {
                        guard !self.event.isInvalidated else { return }
                        self.event.isParticipate.value = true
                        self.showCancelBtn()
                    }
                }
                self.loadBtn.hideLoader()
            })
        } else if self.resButton.title(for: .normal) == NSLocalizedString("CANCEL", comment: "") {
            loadBtn.showLoader(ThemeManager.currentTheme().downView)
            ParticipantRealm().removeParticipant(event.uid, (CacheService.shared.getDataFromDB(object: UserRealm()).first as! UserRealm).uuid!, completion: { (result) in
                if result {
                    try! CacheService.shared.getDatabase().write {
                        guard !self.event.isInvalidated else { return }
                        self.event.isParticipate.value = false
                        self.showRegisterBtn()
                    }
                }
                self.loadBtn.hideLoader()
            })
        } else {
            modifyEvent()
        }
        
    }
    
    func showRegisterBtn() {
        self.resButton.setTitle(NSLocalizedString("BOOK", comment: ""), for: .normal)
        self.resButton.setTitleColor(.white, for: .normal)
        self.resButton.backgroundColor = ThemeManager.currentTheme().validationColor
        self.resButton.layer.shadowColor = ThemeManager.currentTheme().validationColor.cgColor
        self.loadBtn.hideLoader()
    }
    
    func showCancelBtn() {
        self.resButton.setTitle(NSLocalizedString("CANCEL", comment: ""), for: .normal)
        
        self.resButton.backgroundColor = ThemeManager.currentTheme().errorColor
        self.resButton.layer.shadowColor = ThemeManager.currentTheme().errorColor.cgColor
        self.loadBtn.hideLoader()
    }
    
    func showDeleteBtn() {
        self.resButton.setTitle(NSLocalizedString("MODIFY", comment: ""), for: .normal)
        self.resButton.backgroundColor = ThemeManager.currentTheme().warningColor
        self.resButton.layer.shadowColor = ThemeManager.currentTheme().warningColor.cgColor
        self.loadBtn.hideLoader()
    }
    
    func showFullBtn() {
        self.resButton.setTitle(NSLocalizedString("FULL", comment: ""), for: .normal)
        self.resButton.setTitleColor(ThemeManager.currentTheme().selectedFontColor, for: .normal)
        self.resButton.backgroundColor = ThemeManager.currentTheme().upperView
        self.resButton.layer.shadowColor = ThemeManager.currentTheme().shadowColor.cgColor
        self.loadBtn.hideLoader()
    }
    
    @IBAction func didBack(_ sender: UIButton) {
        self.delegate?.getCell(cell: cell, isUnregister)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didReturn(_ sender: UIScreenEdgePanGestureRecognizer) {
        self.delegate?.getCell(cell: cell, isUnregister)
        dismiss(animated: true, completion: nil)
    }
    
    override func ambience(_ notification: Notification) {
        super.ambience(notification)
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if !ambience { return .default }
        switch Ambience.currentState {
        case .invert:
            return .lightContent
        case .contrast, .regular:
            return .default
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pushToProfile" {
            let vc = segue.destination as! ProfileParticipantViewController
            vc.owner = event.owner
            vc.participant = participantToPush
        }
    }
    
    func modifyEvent() {
        UserDefaults.standard.set(1, forKey: "pageIndexAgenda")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tab = storyboard.instantiateViewController(withIdentifier: "MainTabController") as! MainTabBarViewController
        tab.selectedIndex = 1
        self.present(tab, animated: true)
    }
    
}



extension DescriptionViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var width = UIScreen.main.bounds.width
        if width > 500 { width = 500 }
        let page : Int = Int(round(scrollView.contentOffset.x / width))
        pageControl.set(progress: page, animated: true)
    }
    
    @IBAction func pageControlDidPage(sender: AnyObject) {
        let xOffset = scrollView.bounds.width * CGFloat(pageControl.currentPage)
        scrollView.setContentOffset(CGPoint(x: xOffset, y: 0), animated: true)
    }
    
    
}

extension DescriptionViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = ThemeManager.currentColor().mainColor
        renderer.lineWidth = 2.0
        return renderer
    }
    
    
}


extension DescriptionViewController {
    
    func getLocation() -> String {
        if let userLocation = userLocation  {
            let location = CLLocation(latitude: CLLocationDegrees(truncating: NSNumber(value: event.latitude.value!)), longitude: CLLocationDegrees(truncating: NSNumber(value: event.longitude.value!)))
            let formattedDistance = distanceFormatter.string(fromDistance: location.distance(from: userLocation))
            return formattedDistance
        }
        return ""
    }
    
    func getDate() -> (String, String, String) {
        let start_date: String = event.dateStart!.toString(dateFormat: "HH:mm")
        let end_date: String = event.dateEnd!.toString(dateFormat: "HH:mm")
        let date: String = event.dateStart!.toString(dateFormat: "EEEE d MMMM")
        return (start_date, end_date, date)
    }
    
    func secondsToHoursMinutesSeconds(_ seconds : Int, result: @escaping (Int, Int, Int)->()) {
        result(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func timeText(_ s: Int) -> String {
        return s < 10 ? "0\(s)" : "\(s)"
    }
    
    func geocode(latitude: Double, longitude: Double, completion: @escaping (CLPlacemark?, Error?) -> ())  {
        CLGeocoder().reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude)) { completion($0?.first, $1) }
    }
    
    func displayWay(_ source: CLLocationCoordinate2D, _ destination: CLLocationCoordinate2D) {
        let sourcePlacemark = MKPlacemark(coordinate: source, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destination, addressDictionary: nil)
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = event?.title?.capitalizingFirstLetter()
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        self.mapView.showAnnotations([destinationAnnotation], animated: true )
        self.mapView.setCenter(destination, animated: true)
        
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: .aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
            self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, edgePadding: UIEdgeInsets(top: 50.0, left: 50.0, bottom: 100.0, right: 50.0), animated: true)
            
        }
        
    }
    
    
    
}

extension DescriptionViewController: ParticipantsViewDelegate {
    func didSelectParticipant(_ participant: ParticipantRealm?) {
        participantToPush = participant
        performSegue(withIdentifier: "pushToProfile", sender: nil)
    }
    
}
