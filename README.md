# Weepen iOS Application

Weepen is a platform to find sports partners close to you.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

To run the application, you will need these items :

- Xcode
- iOS Simulator
- Git
- Account GitLab (ask Bastien Dhiver for an access)

and if you copile on your mobile phone :

- Apple Cable Lightning
- iPhone/iPad/iPod (with iOS 10 minimum)

### Installing

First, you need to clone the repository with this command line :
```
cd; git clone git@gitlab.com:weepen/mobile/ios.git
```

After that, go to the main folder and launch the .xcworkspace :
```
cd ios/; open Weepen.xcworkspace
```

For build, select an iOS Simulator device or your iPhone and just tap on "Commande ⌘ + R".

## Deployment

For the deployment of our app, we use [iTunes Connect](https://itunesconnect.apple.com/) to put the application on the App Store and [TestFlight](https://developer.apple.com/testflight/) to distribute the app for beta testers.

## Built With

* [Xcode](https://developer.apple.com/xcode/) - Apple IDE
* [Cocoapods](https://cocoapods.org) - Dependency Management

## Authors

* **Louis Cheminant** - *Initial work* - [Olius](https://gitlab.com/Olius)

See also the list of [contributors](https://gitlab.com/weepen) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Apple & Steve Jobs 📱⌚️💻
* Pinterest ✏️
* Beers ❤️
